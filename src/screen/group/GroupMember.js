import React, {useEffect, useState} from 'react';
import {
  FlatList,
  View,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {Text, Thumbnail} from 'native-base';
import Header from '../../component/Header';
import {useNavigation, useRoute} from '@react-navigation/native';
import {ROUTES} from '../../navigation/routes.constant';
import Axios from 'axios';
import {SERVER_URI, LocalStorage, COLOR} from '../../common';
import {Capitalize} from '../../common/capatalize';
import Heading from '../../component/Heading';
import {
  responsiveFontSize,
  responsiveScreenHeight,
} from 'react-native-responsive-dimensions';
import TextAvatarComp from '../../component/textAvatarComponent';
import MemberCompo from '../../component/memberComponent';

const GroupMember = props => {
  const navigation = useNavigation();
  const route = useRoute();
  const {groupid, name} = route.params;

  const [groupmember, setGroupmember] = useState([]);
  const [loading, setLoading] = useState(true);
  const _groupfetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/subgroupsmember`,
      {
        user_id: await LocalStorage.getUserID(),
        group_id: groupid,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);

        if (res.data.status == 'fail') {
          setGroupmember([]);
        } else {
          setGroupmember(res.data.data);
        }
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    if (loading) {
      _groupfetch();
    }
  });

  return (
    <Heading
      text={name}
      lefticon={'ios-arrow-round-back'}
      Iconmap={
        [
          // {
          //   name: 'search',
          //   onPress: () => {
          //     navigation.navigate(ROUTES.GROUPSEARCH);
          //   },
          // },
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('bellpress');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        navigation.goBack();
      }}>
      {loading ? (
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
        </View>
      ) : (
        <FlatList
          data={groupmember}
          ListEmptyComponent={
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontFamily: 'product'}}>No members Found</Text>
            </View>
          }
          renderItem={({item}) => (
            <MemberCompo
              onPress={() => {
                navigation.navigate(ROUTES.MEMBERPROFILE, {
                  type: 'GROUP',
                  fullname: `${Capitalize(item.full_name)}`,
                  id: item.id,
                  address: item.address,
                  photo:
                    item.photo == 'http://app.sheepmug.com/public/uploads'
                      ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                      : item.photo,
                });
              }}
              name={Capitalize(item.full_name)}
              photo={
                item.photo == 'http://app.sheepmug.com/public/uploads'
                  ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                  : item.photo
              }
              address={item.address}
            />
            // <TouchableOpacity
            //   onPress={() => {
            //     navigation.navigate(ROUTES.MEMBERPROFILE, {
            //       type: 'GROUP',
            //       fullname: `${Capitalize(item.full_name)}`,
            //       id: item.id,
            //       address: item.address,
            //       photo:
            //         item.photo ==
            //         'http://app.sheepmug.com/public/uploads'
            //           ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
            //           : item.photo,
            //     });
            //   }}
            //   style={{
            //     height: responsiveScreenHeight(8),
            //     width: '100%',
            //     borderTopColor: '#00000017',
            //     borderTopWidth: 2,
            //     borderBottomColor: '#00000017',
            //     borderBottomWidth: 2,
            //     alignItems: 'center',
            //     flexDirection: 'row',
            //   }}>
            //   <View style={{marginLeft: 15}}>
            //     <Thumbnail
            //       style={{height: 40, width: 40}}
            //       source={{
            //         uri:
            //           item.photo ==
            //           'http://app.sheepmug.com/public/uploads'
            //             ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
            //             : item.photo,
            //       }}
            //     />
            //   </View>
            //   <View style={{marginLeft: 16}}>
            //     <Text
            //       style={{
            //         fontSize: responsiveFontSize(1.5),
            //         color: '#3A4041',
            //         fontFamily: 'product',
            //       }}>
            //       {Capitalize(item.full_name)}
            //     </Text>
            //     <Text
            //       style={{color: '#9CA8AA', fontSize: responsiveFontSize(1)}}>
            //       {item.address}
            //     </Text>
            //   </View>
            // </TouchableOpacity>
          )}
          keyExtractor={item => item.key}
        />
      )}
    </Heading>
  );
};
export default GroupMember;
{
  /* <FlatList
ListEmptyComponent={
  <View style={{justifyContent: 'center', alignItems: 'center'}}>
    <Text style={{fontFamily: 'sf'}}>No Result Found</Text>
  </View>
}
data={groupmember}
renderItem={({item}) => (
  <TouchableOpacity
    onPress={() => {
      navigation.navigate(ROUTES.MEMBERPROFILE, {
        type: 'GROUP',
        fullname: `${Capitalize(item.full_name)}`,
        id: item.id,
        address: item.address,
        photo:
          item.photo ==
          'http://app.sheepmug.com/public/uploads'
            ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
            : item.photo,
      });
    }}
    style={{
      height: 82,
      width: '100%',
      alignItems: 'center',
      flexDirection: 'row',
      backgroundColor: 'white',
      marginTop: 3,
    }}>
    <View style={{marginLeft: 32}}>
      <Thumbnail
        source={{
          uri:
            item.photo ==
            'http://app.sheepmug.com/public/uploads'
              ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
              : item.photo,
        }}
      />
    </View>
    <View style={{marginLeft: 25}}>
      <Text style={{fontSize: 18, color: '#000000',fontFamily: 'sf'}}>
        {Capitalize(item.full_name)}
      </Text>
    </View>
  </TouchableOpacity>
)}
keyExtractor={item => item.key}
/> */
}
