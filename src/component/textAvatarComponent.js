import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import {Text} from 'native-base';
import TextAvatar from 'react-native-text-thumbnail';
import {getRandomColor} from '../common';

const TextAvatarComp = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={{
        height: responsiveScreenHeight(10),
        width: '100%',
        marginTop: 5,
        backgroundColor: 'white', //'#1E7A9D',
        borderBottomColor: '#00000017',
        borderBottomWidth: 1,
        alignItems: 'center',
        flexDirection: 'row',
      }}>
      <View style={{marginLeft: 15}}>
        <TextAvatar
          backgroundColor={getRandomColor()}
          textColor={'#FFF'}
          size={40}
          type={'circle'}>
          {props.textavarname}
        </TextAvatar>
      </View>
      <View style={{marginLeft: 20}}>
        <Text
          style={{
            fontSize: responsiveFontSize(2),
            color: '#3A4041',
            fontFamily: 'product',
          }}>
          {props.name}
        </Text>
        {props.address && (
          <Text style={{color: '#9CA8AA', fontSize: responsiveFontSize(1.3)}}>
            {props.address}
          </Text>
        )}
      </View>
    </TouchableOpacity>
  );
};
export default TextAvatarComp;
