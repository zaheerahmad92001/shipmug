import React, {useState} from 'react';
import {Dimensions, ActivityIndicator} from 'react-native';
import {WebView} from 'react-native-webview';

import {useNavigation} from '@react-navigation/native';

import {COLOR} from '../../common';
import Heading from '../../component/Heading';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const BlogList = () => {
  const [visible, setVisible] = useState(true);
  const hideSpinner = () => {
    setVisible(false);
  };
  const navigation = useNavigation();
  return (
    <Heading
      text={'Blogs'}
      lefticon={'ios-menu'}
      Iconmap={
        [
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('bellpress');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        navigation.openDrawer();
      }}>
      <WebView
        onLoad={() => {
          hideSpinner();
        }}
        style={{flex: 1}}
        source={{uri: 'https://agapeinspired.com/'}}
      />
      {visible && (
        <ActivityIndicator
          style={{position: 'absolute', top: height / 2, left: width / 2}}
          size="large"
          color={COLOR.YELLOW}
        />
      )}
    </Heading>
  );
};

export default BlogList;
