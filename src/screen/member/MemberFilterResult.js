import React from 'react';
import {FlatList, View, TouchableOpacity} from 'react-native';
import {Text, ListItem, Left, Body, Thumbnail} from 'native-base';
import Header from '../../component/Header';
import {useNavigation, useRoute} from '@react-navigation/native';
import {ROUTES} from '../../navigation/routes.constant';
import {Capitalize} from '../../common/capatalize';
import Heading from '../../component/Heading';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import MemberCompo from '../../component/memberComponent';

const MemberFilterResult = props => {
  const navigation = useNavigation();
  const routes = useRoute();
  const {member} = routes.params;

  return (
    <Heading
      text={'Filter Members'}
      lefticon={'ios-arrow-round-back'}
      Iconmap={
        [
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('notification screen');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        navigation.goBack();
      }}>
      <FlatList
        data={member}
        ListEmptyComponent={
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontFamily: 'product'}}>...Loading</Text>
          </View>
        }
        renderItem={({item}) => (
          <MemberCompo
            onPress={() => {
              navigation.navigate(ROUTES.MEMBERPROFILE, {
                type: 'MEMBER_FILTER',
                id: item.id,
                address: item.address,
                fullname: `${Capitalize(item.first_name)} ${Capitalize(
                  item.last_name,
                )}`,
                photo:
                  item.photo == null
                    ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                    : `http://app.sheepmug.com/public/uploads/${item.photo}`,
              });
            }}
            name={`${Capitalize(item.first_name)} ${Capitalize(
              item.last_name,
            )}`}
            photo={
              item.photo == null
                ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                : `http://app.sheepmug.com/public/uploads/${item.photo}`
            }
            address={item.address}
          />
          // <TouchableOpacity
          //   onPress={() => {
          //     navigation.navigate(ROUTES.MEMBERPROFILE, {
          //       type: 'MEMBER_FILTER',
          //       id: item.id,
          //       address: item.address,
          //       fullname: `${Capitalize(item.first_name)} ${Capitalize(
          //         item.last_name,
          //       )}`,
          //       photo:
          //         item.photo == null
          //           ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
          //           : `http://app.sheepmug.com/public/uploads/${
          //               item.photo
          //             }`,
          //     });
          //   }}
          //   style={{
          //     height: responsiveScreenHeight(8),
          //     width: '100%',
          //     borderTopColor: '#00000017',
          //     borderTopWidth: 2,
          //     borderBottomColor: '#00000017',
          //     borderBottomWidth: 2,
          //     alignItems: 'center',
          //     flexDirection: 'row',
          //   }}>
          //   <View style={{marginLeft: 15}}>
          //     <Thumbnail
          //       style={{height: 40, width: 40}}
          //       source={{
          //         uri:
          //           item.photo == null
          //             ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
          //             : `http://app.sheepmug.com/public/uploads/${
          //                 item.photo
          //               }`,
          //       }}
          //     />
          //   </View>
          //   <View style={{marginLeft: 16}}>
          //     <Text
          //       style={{
          //         fontSize: responsiveFontSize(1.5),
          //         color: '#3A4041',
          //         fontFamily: 'product',
          //       }}>
          //       {Capitalize(item.first_name)} {Capitalize(item.last_name)}
          //     </Text>
          //     <Text style={{color: '#9CA8AA', fontSize: responsiveFontSize(1)}}>
          //       {item.address}
          //     </Text>
          //   </View>
          // </TouchableOpacity>
        )}
        keyExtractor={item => item.key}
      />
    </Heading>
  );
};
export default MemberFilterResult;
//     <TouchableOpacity
//       onPress={() => {
//         navigation.navigate(ROUTES.MEMBERPROFILE, {
//           type: 'MEMBER_FILTER',
//           id: item.id,
//           address: item.address,
//           fullname: `${Capitalize(item.first_name)} ${Capitalize(
//             item.last_name,
//           )}`,
//           photo:
//             item.photo == null
//               ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
//               : `http://app.sheepmug.com/public/uploads/${
//                   item.photo
//                 }`,
//         });
//       }}
//       style={{
//         height: 82,
//         width: '100%',
//         alignItems: 'center',
//         flexDirection: 'row',
//         backgroundColor: 'white',
//         marginTop: 3,
//       }}>
//       <View style={{marginLeft: 32}}>
//         <Thumbnail
//           source={{
//             uri:
//               item.photo == null
//                 ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
//                 : `http://app.sheepmug.com/public/uploads/${
//                     item.photo
//                   }`,
//           }}
//         />
//       </View>
//       <View style={{marginLeft: 25}}>
//         <Text style={{fontSize: 18, color: '#000000', fontFamily: 'sf'}}>
//           {Capitalize(item.first_name)} {Capitalize(item.last_name)}
//         </Text>
//       </View>
//     </TouchableOpacity>
//   )}
//   keyExtractor={item => item.first_name}
// />
