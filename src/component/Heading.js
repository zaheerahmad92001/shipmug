import React from 'react';
import { Container, Header } from 'native-base';
import { StatusBar, View, Text } from 'react-native';
import { COLOR } from '../common';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import Icon from 'react-native-vector-icons/EvilIcons';
import FeatheIcon from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Heading = (props) => {
  return (
    <Container style={{ backgroundColor: '#FFFFFF' }}>
      <Header
        style={{
          backgroundColor: 'white',
          height: responsiveScreenHeight(7),
          // marginTop: 24,
        }}>
        <View
          style={{
            justifyContent: 'space-between',
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            height: '100%',
            paddingHorizontal: 1,
          }}>
          <View style={{ flexDirection: 'row' }}>
            <Ionicons
              name={props.lefticon}
              onPress={props.onMenuPress}
              color={COLOR.SUBLABEL}
              size={30}
            />
            <Text
              style={{
                fontFamily: 'product',
                fontSize: responsiveFontSize(2.2),
                textAlign: 'left',
                marginLeft: 30,
              }}>
              {props.text}
            </Text>
          </View>

          {/* Right side  */}
          <View style={{flexDirection:'row',alignItems:"center"}}>
           {props.rightText?
          <TouchableOpacity 
           style={{marginRight:10}}
            onPress={props.rightAction}>
            <Text style={{color:COLOR.SUBLABEL}}>{props.rightText}</Text>
          </TouchableOpacity>:null

           }
      
          {props.iconType ?
            <FeatheIcon
              name={'more-vertical'}
              color={COLOR.SUBLABEL}
              onPress={props.rightIconPess}
              size={30}
            /> :
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              {props.Iconmap.map((i) => {
                return i.icon ? (
                  i.icon
                ) : (
                    <Icon
                      style={{ margin: 4 }}
                      key={i.name}
                      name={i.name}
                      onPress={i.onPress}
                      color={COLOR.SUBLABEL}
                      size={30}
                    />
                  );
              })}
            </View>
          }
        </View>
        </View>
      </Header>

      <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
      {props.children}
    </Container>
  );
};
export default Heading;
