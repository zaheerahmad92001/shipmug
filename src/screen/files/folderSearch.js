import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {FlatList, ActivityIndicator, View, StatusBar} from 'react-native';

import {Content, Text, Item, Header as Head, Input, Icon} from 'native-base';
import {COLOR, SERVER_URI, LocalStorage} from '../../common';
import Axios from 'axios';
import {ROUTES} from '../../navigation/routes.constant';
import {Capitalize} from '../../common/capatalize';
import TextAvatarComp from '../../component/textAvatarComponent';
import Heading from '../../component/Heading';
const FolderSearch = () => {
  const [search, setSearch] = useState('');
  const navigation = useNavigation();
  const [folderList, setfolderList] = useState([]);
  const [loading, setLoading] = useState(false);

  const _memberFetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/folder`,
      {
        user_id: await LocalStorage.getUserID(),
        key_word: search,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);
        setfolderList(res.data.data);
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };
  const _view = async (navigation, folderid, name) => {
    navigation.navigate(ROUTES.FILELIST, {folderid, name});
  };

  return (
    <Heading
      text={'Folder Search'}
      lefticon={'ios-arrow-round-back'}
      Iconmap={
        [
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('notification screen');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        navigation.goBack();
      }}>
      <Content
        contentContainerStyle={{
          width: '100%',
          backgroundColor: 'white',
        }}>
        <Head searchBar rounded style={{backgroundColor: 'white'}}>
          <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
          <Item
            regular
            style={{
              marginBottom: 6,
              padding: 5,
              borderRadius: 5,
              borderWidth: 1,
            }}>
            <Icon name="ios-people" />
            <Input
              placeholder="Search"
              style={{fontFamily: 'product', color: '#b0afb0'}}
              placeholderTextColor={'#b0afb0'}
              onChangeText={e => {
                setSearch(e);
                _memberFetch();
              }}
            />

            <Icon
              name="ios-search"
              onPress={() => {
                _memberFetch();
              }}
            />
          </Item>
        </Head>
        <FlatList
          data={folderList}
          ListFooterComponent={
            loading ? (
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
              </View>
            ) : null
          }
          ListEmptyComponent={
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontFamily: 'product'}}>Search Result empty</Text>
            </View>
          }
          renderItem={({item}) => (
            <TextAvatarComp
              onPress={() => {
                _view(navigation, item.id, item.name);
              }}
              name={Capitalize(item.name)}
              textavarname={item.name
                .split(' ')
                .slice(0, 2)
                .join('+')
                .toUpperCase()}
            />
          )}
          keyExtractor={item => item.key}
        />
      </Content>
    </Heading>
  );
};
export default FolderSearch;
