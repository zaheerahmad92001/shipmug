import React, {useEffect, useState} from 'react';
import {Image, View, Dimensions, RefreshControl, Text} from 'react-native';
import Header from '../../component/Header';
import {
  useNavigation,
  StackActions,
  useFocusEffect,
} from '@react-navigation/native';
import {COLOR, SERVER_URI, LocalStorage} from '../../common';
import {Content, Button} from 'native-base';
import Axios from 'axios';
import {ROUTES} from '../../navigation/routes.constant';
import {Capitalize} from '../../common/capatalize';
import Heading from '../../component/Heading';
const Profile = () => {
  const navigation = useNavigation();

  const [member, setMember] = useState();
  const [loading, setLoading] = useState(true);

  const [imgload, setImgload] = useState(true);
  const _memberFetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/getusernamedetail`,
      {
        user_id: await LocalStorage.getUserID(),
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);
        setMember(res.data.data);
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    if (loading) {
      _memberFetch();
    }
  });
  const reset = route => {
    return navigation.dispatch(StackActions.pop(2));
  };

  const _logout = async navigation => {
    await LocalStorage.clearCache();
    await LocalStorage.clearToken();
    await LocalStorage.clearUserID();
    await LocalStorage.clearPermission();
    Axios.post(
      `${SERVER_URI}/logoutuser`,
      {
        user_id: await LocalStorage.getUserID(),
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });

    await reset(ROUTES.AUTHSTACK);

    // navigation.dispatch(StackActions.push(ROUTES.AUTHSTACK));
    // await navigation.navigate(ROUTES.AUTHSTACK);
  };
  // useFocusEffect(
  //   React.useCallback(() => {
  //     _memberFetch();
  //   }),
  // );

  useFocusEffect(
    React.useCallback(() => {
      _memberFetch();
    }, []),
  );

  var str = member && member.profilepic;

  var res =
    member &&
    member.profilepic &&
    str.replace(
      'http://app.sheepmug.com/public/uploads/',
      'https://app.sheepmug.com/public/uploads/user/',
    );

  return (
    <Heading
      text={'Profile'}
      lefticon={'ios-arrow-round-back'}
      Iconmap={
        [
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('bellpress');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        navigation.goBack();
      }}>
      <Content
        refreshControl={
          <RefreshControl
            colors={['#9Bd35A', '#689F38']}
            refreshing={loading}
            onRefresh={() => setLoading(true)}
          />
        }
        contentContainerStyle={{
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white',
        }}>
        <View
          style={{
            backgroundColor: 'white',
            height: Dimensions.get('window').height - 150,
            width: '100%',
            alignItems: 'center',
          }}>
          <Image
            onLoadEnd={() => {
              setImgload(false);
            }}
            style={{
              marginBottom: 20,
              marginTop: 10,
              height: 200,
              width: 200,
              borderRadius: 100,
            }}
            source={{
              uri: imgload
                ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                : member &&
                  member.profilepic == 'http://app.sheepmug.com/public/uploads/'
                ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                : `${res}`,
            }}
          />
          <Text
            style={{
              color: '#000000',
              fontSize: 30,
              fontWeight: '100',
              fontFamily: 'product',
            }}>
            {member && Capitalize(member.username)}
          </Text>
          <View
            style={{
              width: '90%',
              backgroundColor: 'white',
              marginBottom: 10,
              marginTop: 10,
              height: 75,
              borderWidth: 1,
              borderColor: 'gray',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                color: '#000000',
                fontSize: 20,
                marginLeft: 15,
                fontFamily: 'product',
              }}>
              Members - {member && member.total_member}
            </Text>
          </View>
          <View
            style={{
              width: '90%',
              backgroundColor: 'white',
              borderWidth: 1,
              borderColor: 'gray',
              marginBottom: 10,
              height: 75,
              justifyContent: 'center',
            }}>
            <Text
              style={{
                color: '#000000',
                fontSize: 20,
                marginLeft: 15,
                fontFamily: 'product',
              }}>
              Groups - {member && member.total_group}
            </Text>
          </View>
          <Button
            style={{
              width: '90%',
              height: 56,
              backgroundColor: '#FEC169',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => {
              _logout(navigation);
            }}>
            <Text
              style={{
                color: 'white',
                alignSelf: 'center',
                fontSize: 20,
                fontFamily: 'sf',
              }}>
              LOG-OUT
            </Text>
          </Button>
        </View>
      </Content>
    </Heading>
  );
};
export default Profile;
