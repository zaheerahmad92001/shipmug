import React, {useEffect} from 'react';
import {View, Text, Image, StyleSheet, StatusBar} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import AppIntroSlider from 'react-native-app-intro-slider';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import {ROUTES} from '../../navigation/routes.constant';
import {LocalStorage} from '../../common';

const data = [
  {
    title: 'Church Management System',
    text: 'All the help at your finger tip!',
    description:
      'Manage all your church members in one place. Assign group leaders to their groups with the Mobile App',
    image: require('../../../assets/1.png'),
    bg: '#fff',
  },
  {
    title: 'MANAGE',
    text: 'Easily manage all your sheep in one place.',
    description:
      'Manage all your church members in one place with ease: events , groups attendance and reports online.',
    image: require('../../../assets/2.png'),
    bg: '#fff',
  },
  {
    title: 'LEARN',
    text: 'Learn about discipleship, get inspired in your walk with God',
    description:
      'Join the learning forum on how to disciple your members. Share your challenges and ideas',
    image: require('../../../assets/3.png'),
    bg: '#fff',
  },
];

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 320,
    height: 340,
    marginVertical: 32,
  },
  buttonCircle: {
    width: 44,
    height: 44,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 22,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
const Intro = () => {
  const navigation = useNavigation();
  // const _bootstrapAsync = async () => {
  //   const intro = await LocalStorage.getIntro();
  //   if (intro == 'true') {
  //     navigation.navigate(ROUTES.AUTHSTACK);
  //   }
  // };

  // useEffect(() => {
  //   _bootstrapAsync();
  // });
  // useFocusEffect(() => {
  //   _bootstrapAsync();
  // });
  const _renderItem = ({item}) => {
    return (
      <View
        style={[
          styles.slide,
          {
            backgroundColor: item.bg,
          },
        ]}>
        <Image source={item.image} style={styles.image} />

        <Text
          style={{
            fontFamily: 'product',
            color: '#707070',
            fontSize: 12,
          }}>
          {item.title}
        </Text>
        <Text
          style={{
            fontFamily: 'product',
            color: '#000000',
            marginTop: 21,
            fontSize: 18,
            textAlign: 'center',
            alignSelf: 'center',
            width: 150,
          }}>
          {item.text}
        </Text>
        <Text
          style={{
            fontFamily: 'product',
            color: '#5B6466',
            fontSize: 12,
            alignSelf: 'center',
            width: 210,
            textAlign: 'center',
            marginTop: 24,
          }}>
          {item.description}
        </Text>
      </View>
    );
  };

  const _keyExtractor = item => item.title;
  const _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon
          name="md-arrow-round-forward"
          color="rgba(255, 255, 255, .9)"
          size={24}
        />
      </View>
    );
  };
  const _onfinish = async navigation => {
    await LocalStorage.storeIntro('true');
    await navigation.navigate(ROUTES.AUTHSTACK);
  };

  const _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon
          onPress={() => {
            _onfinish(navigation);
          }}
          name="md-checkmark"
          color="rgba(255, 255, 255, .9)"
          size={24}
        />
      </View>
    );
  };

  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
      <AppIntroSlider
        keyExtractor={_keyExtractor}
        renderDoneButton={_renderDoneButton}
        renderNextButton={_renderNextButton}
        renderItem={_renderItem}
        data={data}
        activeDotStyle={{backgroundColor: '#0078F2'}}
        dotStyle={{backgroundColor: 'rgba(0, 120, 242, 0.2)'}}
      />
    </View>
  );
};
export default Intro;
