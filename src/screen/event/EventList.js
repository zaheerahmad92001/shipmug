import React, {useEffect, useState} from 'react';
import {View, FlatList, TouchableOpacity, RefreshControl} from 'react-native';
import {Text} from 'native-base';
import {SERVER_URI, LocalStorage, getRandomColor} from '../../common';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import {ROUTES} from '../../navigation/routes.constant';

import Axios from 'axios';

import {Capitalize} from '../../common/capatalize';
import {_logout} from '../../component/logout';
import Heading from '../../component/Heading';
import TextAvatar from 'react-native-text-thumbnail';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import TextAvatarComp from '../../component/textAvatarComponent';
import moment from 'moment';

const EventList = () => {
  const navigation = useNavigation();
  const [eventList, setEventList] = useState();
  const [loading, setLoading] = useState(true);
  const _fetchevent = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/eventlist`,
      {
        user_id: await LocalStorage.getUserID(),
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        if (res.data.message == 'Invalid token or userid') {
          _logout(navigation);
        } else {
          setLoading(false);
          setEventList(res.data.data);
        }
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    if (loading) {
      _fetchevent();
    }
  });

  useFocusEffect(
    React.useCallback(() => {
      _fetchevent();
    }, []),
  );

  return (
    <Heading
      text={'Events'}
      lefticon={'ios-menu'}
      Iconmap={[
        {
          name: 'search',
          onPress: () => {
            navigation.navigate(ROUTES.EVENTSEARCH);
          },
        },
        // {
        //   name: 'bell',
        //   onPress: () => {
        //     alert('bellpress');
        //   },
        // },
      ]}
      rightText={'Create Event'}
      rightAction={() =>navigation.navigate('CREATEEVENT')}
      onMenuPress={() => {
        navigation.openDrawer();
      }}>
      <FlatList
        refreshControl={
          <RefreshControl
            colors={['#9Bd35A', '#689F38']}
            refreshing={loading}
            onRefresh={() => setLoading(true)}
          />
        }
        data={eventList}
        ListEmptyComponent={
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
              width: '100%',
            }}>
            <Text style={{fontFamily: 'product'}}>No Result Found</Text>
          </View>
        }
        renderItem={({item}) => (
          <TextAvatarComp
            onPress={() => {
              navigation.navigate(ROUTES.EVENTPROFILE, {
                id: item.id,
                name: item.name,
                item
              });
            }}
            name={Capitalize(item.name)}
            textavarname={item.name
              .split(' ')
              .slice(0, 2)
              .join('+')
              .toUpperCase()}
            address={moment(item.start_date).format('dddd, Do MMMM, YY')}
          />
        )}
        keyExtractor={item => item.key}
      />
    </Heading>
  );
};
export default EventList;
{
  /* <Content contentContainerStyle={{backgroundColor: COLOR.LIGHTBLUE}}>
<View
  style={{
    height: 82,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
    marginTop: 3,
    paddingHorizontal: 30,
  }}>
  <View style={{flexDirection: 'row', width: '100%'}}>
    <TouchableOpacity
      onPress={() => {
        navigation.navigate(ROUTES.EVENTPROFILE, {id: item.id});
      }}
      style={{width: '55%'}}>
      <Text
        numberOfLines={1}
        style={{fontSize: 20, fontFamily: 'sf'}}>
        {Capitalize(item.name)}
      </Text>
      <Text
        numberOfLines={1}
        style={{color: '#b8b6bf', fontFamily: 'sf'}}>
        {moment(item.start_date).format('dddd, Do MMMM, YY')}
      </Text>
    </TouchableOpacity>
    <View
      style={{
        width: '45%',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        flexDirection: 'row',
      }}>
      <View style={{marginLeft: 10}}>
        <Text
          style={{
            color: COLOR.DARKBLUE,
            alignSelf: 'center',
            fontSize: 20,
            fontFamily: 'sf',
          }}>
          {item.present}
        </Text>
        <Text style={{fontSize: 10, fontFamily: 'sf'}}>
          PRESENT
        </Text>
      </View>

      <View style={{marginLeft: 10}}>
        <Text
          style={{
            color: COLOR.YELLOW,
            alignSelf: 'center',
            fontSize: 20,
            fontFamily: 'sf',
          }}>
          {item.absent}
        </Text>
        <Text style={{fontSize: 10, fontFamily: 'sf'}}>ABSENT</Text>
      </View>
    </View>
  </View>
</View>
</Content> */
}
