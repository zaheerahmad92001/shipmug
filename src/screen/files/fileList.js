import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  FlatList,
  Linking,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import {SERVER_URI, LocalStorage, COLOR} from '../../common';
import {useRoute, useNavigation} from '@react-navigation/native';
import Axios from 'axios';

import Doc from '../../icon/Doc';
import Excel from '../../icon/Excel';
import Jpg from '../../icon/Jpg';
import MP from '../../icon/Mp';
import Pdf from '../../icon/pdf';
import PowerPoint from '../../icon/powerpoint';
import {Capitalize} from '../../common/capatalize';
import Heading from '../../component/Heading';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import {ROUTES} from '../../navigation/routes.constant';
const FileList = () => {
  const [fileList, setFileList] = useState(null);
  const navigation = useNavigation();
  const route = useRoute();
  const {folderid, name} = route.params;
  const [loading, setLoading] = useState(true);
  const [loadings, setLoadings] = useState(false);

  const fetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/files`,
      {
        user_id: await LocalStorage.getUserID(),
        folder_id: folderid,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setFileList(res.data.data);
        setLoading(false);
      })
      .catch(err => {
        setLoading(false);
        setFileList([]);
        console.log(err);
      });
  };
  useEffect(() => {
    if (loading) {
      fetch();
    }
  });

  const _refresh = () => {
    setTimeout(() => {
      setLoadings(false);
    }, 5000);
  };

  const getfileicon = type => {
    let Component = Doc;
    if (type == 'pdf') {
      Component = Pdf;
    } else if (type == 'docx' || type == 'doc') {
      Component = Doc;
    } else if (type == 'jpg' || type == 'png' || type == 'jpeg') {
      Component = Jpg;
    } else if (
      type == 'xls' ||
      type == 'xlsb' ||
      type == 'xlsm' ||
      type == 'xlsx'
    ) {
      Component = Excel;
    } else if (
      type == 'mp3' ||
      type == 'Mp3' ||
      type == 'ogg' ||
      type == 'mp4'
    ) {
      Component = MP;
    } else if (type == 'pptx' || type == 'pptm' || type == 'ppt') {
      Component = PowerPoint;
    }

    return <Component height={40} width={40} />;
  };

  return fileList == null ? (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        backgroundColor: COLOR.LIGHTBLUE,
      }}>
      <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
    </View>
  ) : (
    <Heading
      text={name}
      lefticon={'ios-arrow-round-back'}
      Iconmap={[
        {
          name: 'search',
          onPress: () => {
            navigation.navigate(ROUTES.FILESEARCH, {
              folderid: folderid,
            });
          },
        },
        // {
        //   name: 'bell',
        //   onPress: () => {
        //     alert('bellpress');
        //   },
        // },
      ]}
      onMenuPress={() => {
        navigation.goBack();
      }}>
      <FlatList
        refreshControl={
          <RefreshControl
            colors={['#9Bd35A', '#689F38']}
            refreshing={loadings}
            onRefresh={() => _refresh()}
          />
        }
        ListEmptyComponent={
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: '100%',
              height: '100%',
              backgroundColor: COLOR.LIGHTBLUE,
            }}>
            <Text style={{fontFamily: 'product'}}>No file Found</Text>
          </View>
        }
        data={fileList}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(
                `http://www.app.sheepmug.com/public/uploads/folderFiles/${
                  item.name
                }`,
              );
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 15}}>
              {getfileicon(item.name.slice(item.name.lastIndexOf('.') + 1))}
            </View>
            <View style={{marginLeft: 20, width: '90%'}}>
              <Text
                numberOfLines={1}
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                  width: '90%',
                }}>
                {Capitalize(item.name)}
              </Text>
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={item => item.id}
      />
    </Heading>
  );
};
export default FileList;
