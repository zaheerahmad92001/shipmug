import * as React from 'react';
import Svg, {G, Circle, Path} from 'react-native-svg';

function Call(props) {
  return (
    <Svg width={30} height={30} viewBox="0 0 24 24" {...props}>
      <G transform="translate(-.189)">
        <Circle
          cx={12}
          cy={12}
          r={12}
          transform="translate(.189)"
          fill="#2196f3"
        />
        <Path
          d="M17.501 14.262a8.128 8.128 0 01-3.6-.823.737.737 0 00-.986.342l-.53 1.1a10.367 10.367 0 01-3.824-3.824l1.1-.53a.737.737 0 00.342-.986 8.1 8.1 0 01-.825-3.6.736.736 0 00-.736-.736H5.724a.736.736 0 00-.736.736 12.529 12.529 0 0012.515 12.515.736.736 0 00.736-.736v-2.719a.736.736 0 00-.738-.739z"
          fill="#fafafa"
        />
      </G>
    </Svg>
  );
}

export default Call;
