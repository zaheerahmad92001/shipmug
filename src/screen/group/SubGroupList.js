import React, { useState, useEffect } from 'react';
import {
  FlatList,
  View,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import { Text, ListItem, Left, Body, Thumbnail } from 'native-base';
import TextAvatar from 'react-native-text-thumbnail';
import Header from '../../component/Header';
import {
  useNavigation,
  useRoute,
  useFocusEffect,
} from '@react-navigation/native';
import { ROUTES } from '../../navigation/routes.constant';
import Axios from 'axios';
import { SERVER_URI, LocalStorage, COLOR, getRandomColor } from '../../common';
import { Capitalize } from '../../common/capatalize';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import Heading from '../../component/Heading';
import TextAvatarComp from '../../component/textAvatarComponent';

const SubGroupList = props => {
  const navigation = useNavigation();
  const route = useRoute();
  const { groupid } = route.params;

  const [grouplist, setGrouplist] = useState([]);
  const [loading, setLoading] = useState(true);
  const [isInit, setInit] = useState(true);
  const [pastSubGroup, setPastSubGroup] = useState([]);

  const _groupfetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/subgroups`,
      {
        user_id: await LocalStorage.getUserID(),
        group_id: groupid,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);
        setGrouplist(res.data.data);
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    if (loading && isInit) {
      _groupfetch();
    }
  });
  const _backGroups = async () => {
    if (pastSubGroup.length < 1) {
      navigation.goBack();

    } else {
      const lastIndex = pastSubGroup.length - 1;
      setGrouplist([pastSubGroup[lastIndex]]);
      pastSubGroup.pop()
    }
  }
  const _view = async (navigation, groupid, name, notes, userID) => {
    setInit(false)
    setPastSubGroup(pastSubGroup => [...pastSubGroup, { id: groupid, name: name, notes: notes, user_id: userID }])
    setLoading(true);

    Axios.post(
      `${SERVER_URI}/subgroups`,
      {
        user_id: await LocalStorage.getUserID(),
        group_id: groupid,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);
        if (res.data.data.length > 0) {
          setGrouplist(res.data.data);
        } else {
          navigation.navigate(ROUTES.GROUPPROFILE, { groupid, name });
        }
      })
      .catch(err => {
        console.log(err);
      });

  };

  // useFocusEffect(
  //   React.useCallback(() => {
  //     _groupfetch();
  //   }),
  // );

  return (
    <Heading
      text={'Sub Group'}
      lefticon={'ios-arrow-round-back'}
      Iconmap={
        [
          // {
          //   name: 'search',
          //   onPress: () => {
          //     navigation.navigate(ROUTES.GROUPSEARCH);
          //   },
          // },
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('bellpress');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        _backGroups();
      }}>
      {loading ? (
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
        </View>
      ) : (
          <FlatList
            data={grouplist}
            ListEmptyComponent={
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontFamily: 'product' }}>No Result Found</Text>
              </View>
            }
            renderItem={({ item }) => (
              <TextAvatarComp
                onPress={() => {
                  _view(navigation, item.id, item.name, item.notes, item.user_id);
                }}
                name={Capitalize(item.name)}
                textavarname={item.name
                  .split(' ')
                  .slice(0, 2)
                  .join('+')
                  .toUpperCase()}
              />
              // <TouchableOpacity
              //   onPress={() => {
              //     // navigation.navigate(ROUTES.GROUPMEMBER, {groupid: item.id});
              //     navigation.navigate(ROUTES.GROUPPROFILE, {
              //       groupid: item.id,
              //       name: item.name,
              //     });
              //   }}
              //   style={{
              //     height: responsiveScreenHeight(8),
              //     width: '100%',
              //     borderTopColor: '#00000017',
              //     borderTopWidth: 2,
              //     borderBottomColor: '#00000017',
              //     borderBottomWidth: 2,
              //     alignItems: 'center',
              //     flexDirection: 'row',
              //   }}>
              //   <View style={{marginLeft: 15}}>
              //     <TextAvatar
              //       backgroundColor={getRandomColor()}
              //       textColor={'#FFF'}
              //       size={40}
              //       type={'circle'}>
              //       {item.name.split(' ').slice(0, 2).join('+').toUpperCase()}
              //     </TextAvatar>
              //   </View>
              //   <View style={{marginLeft: 16}}>
              //     <Text
              //       style={{
              //         fontSize: responsiveFontSize(1.5),
              //         color: '#3A4041',
              //         fontFamily: 'product',
              //       }}>
              //       {Capitalize(item.name)}
              //     </Text>
              //     <Text
              //       style={{color: '#9CA8AA', fontSize: responsiveFontSize(1)}}>
              //       147 Kotei Road Kotei
              //     </Text>
              //   </View>
              // </TouchableOpacity>
            )}
            keyExtractor={item => item.id}
          />
        )}
    </Heading>
    // <Header
    //   leftIcon={'arrow-back'}
    //   leftClick={() => {
    //     navigation.goBack();
    //   }}
    //   title={`Sub Groups (${grouplist.length})`}
    //   rightSecond={'search'}
    //   rightSecondclick={() => {
    //     navigation.navigate(ROUTES.GROUPSEARCH);
    //   }}>
    //   {loading ? (
    //     <View style={{justifyContent: 'center', alignItems: 'center'}}>
    //       <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
    //     </View>
    //   ) : (
    //     <FlatList
    //       data={grouplist}
    //       ListEmptyComponent={
    //         <View style={{justifyContent: 'center', alignItems: 'center'}}>
    //           <Text style={{fontFamily: 'sf'}}>No Result Found</Text>
    //         </View>
    //       }
    //       renderItem={({item}) => (
    //         <TouchableOpacity
    //           onPress={() => {
    //             navigation.navigate(ROUTES.GROUPMEMBER, {groupid: item.id});
    //           }}
    //           style={{
    //             height: 82,
    //             width: '100%',
    //             alignItems: 'center',
    //             flexDirection: 'row',
    //             backgroundColor: 'white',
    //             marginTop: 3,
    //           }}>
    //           <View style={{marginLeft: 32}}>
    //             <TextAvatar
    //               backgroundColor={'#F1F1F1'}
    //               textColor={COLOR.DARKBLUE}
    //               size={60}
    //               type={'circle'}>
    //               {Capitalize(item.name)}
    //             </TextAvatar>
    //           </View>
    //           <View style={{marginLeft: 25}}>
    //             <Text style={{fontSize: 18, color: '#000000',fontFamily: 'sf'}}>
    //               {Capitalize(item.name)}
    //             </Text>
    //           </View>
    //         </TouchableOpacity>
    //       )}
    //       keyExtractor={item => item.id}
    //     />
    //   )}
    // </Header>
  );
};
export default SubGroupList;
