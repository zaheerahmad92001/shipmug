import React, {useState, useEffect, useRef} from 'react';
import {
  FlatList,
  View,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Content, CardItem, Card, Text, Button} from 'native-base';
import Modal from 'react-native-modal';
import {COLOR, SERVER_URI, LocalStorage} from '../../common';
import Axios from 'axios';
import moment from 'moment';
import {Capitalize} from '../../common/capatalize';

import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import TextAvatar from 'react-native-text-thumbnail';
import RBSheet from 'react-native-raw-bottom-sheet';
const MemberEvent = ({id}) => {
  const [loading, setLoading] = useState(true);
  const [event, setEvent] = useState(null);
  const [show, setShow] = useState(false);
  const [eventid, setEventid] = useState(null);
  const attendenceRef = useRef();
  const _eventfetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/memberattendance`,
      {
        user_id: await LocalStorage.getUserID(),
        member_id: id,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async (res) => {
        setLoading(false);
        setEvent(res.data.data);
      })
      .catch((err) => {
        setLoading(false);
      });
  };

  const _takeattendence = async (present, eventid) => {
    Axios.post(
      `${SERVER_URI}/attendanceaction`,
      {
        user_id: await LocalStorage.getUserID(),
        member_id: id,
        event_id: eventid,
        is_present: present,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async (res) => {
        _eventfetch();
      })
      .catch((err) => {
        _eventfetch();
      });
  };

  useEffect(() => {
    if (loading) {
      _eventfetch();
    }
  });
  return event == null ? (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
      <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
    </View>
  ) : (
    <>
      {/* <Modal isVisible={show}>
        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            width: Dimensions.get('window').width,
          }}
          onPress={() => {
            setShow(!show);
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              width: Dimensions.get('window').width,
            }}>
            <View
              style={{
                height: 180,
                width: Dimensions.get('window').width - 100,
                backgroundColor: 'white',
              }}>
              <Text
                style={{
                  alignSelf: 'center',
                  fontSize: 25,
                  padding: 20,
                  fontFamily: 'sf',
                }}>
                Attendence
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  position: 'absolute',
                  bottom: 0,
                }}>
                <Button
                  onPress={() => {
                    _takeattendence(1, eventid);
                    setShow(!show);
                  }}
                  style={{
                    height: 60,
                    backgroundColor: COLOR.DARKBLUE,
                    width: '50%',
                    justifyContent: 'center',
                  }}>
                  <Text style={{alignSelf: 'center', fontFamily: 'sf'}}>
                    Present
                  </Text>
                </Button>
                <Button
                  onPress={() => {
                    _takeattendence(0, eventid);
                    setShow(!show);
                  }}
                  style={{
                    height: 60,
                    backgroundColor: COLOR.YELLOW,
                    width: '50%',
                    justifyContent: 'center',
                  }}>
                  <Text style={{alignSelf: 'center', fontFamily: 'sf'}}>
                    Absent
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </Modal> */}

      <View style={{backgroundColor: 'white', height: '100%'}}>
        <FlatList
          data={event}
          ListEmptyComponent={
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontFamily: 'product'}}>No Result Found</Text>
            </View>
          }
          renderItem={({item}) => (
            <View
              style={{
                height: responsiveScreenHeight(10),
                width: '100%',
                marginTop: 1,
                backgroundColor: 'white', //'#1E7A9D',
                borderBottomColor: '#00000017',
                borderBottomWidth: 1,
                alignItems: 'center',
                flexDirection: 'row',

                paddingHorizontal: 20,
              }}>
              <View style={{flexDirection: 'row', width: '100%'}}>
                <View style={{width: '55%'}}>
                  <Text
                    numberOfLines={1}
                    style={{
                      fontSize: responsiveFontSize(2),
                      fontFamily: 'product',
                    }}>
                    {Capitalize(item.name)}
                  </Text>
                  <Text
                    numberOfLines={1}
                    style={{
                      color: '#b8b6bf',
                      fontFamily: 'product',
                      fontSize: responsiveFontSize(1.3),
                    }}>
                    {moment(item.start_date).format('dddd, Do MMMM, YY')}
                  </Text>
                </View>
                <View
                  style={{
                    width: '45%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 40,
                  }}>
                  {item.is_present == '' && (
                    <TouchableOpacity
                      style={{justifyContent: 'center', alignItems: 'center'}}
                      onPress={() => {
                        setEventid(item.id);
                        attendenceRef.current.open();
                      }}>
                      <TextAvatar
                        backgroundColor={'#0078F2'}
                        textColor={'#FFF'}
                        size={35}
                        type={'circle'}>
                        M
                      </TextAvatar>
                      <Text
                        style={{
                          fontSize: responsiveFontSize(1.3),
                          fontFamily: 'product',
                        }}>
                        Mark
                      </Text>
                    </TouchableOpacity>
                  )}
                  {item.is_present == '1' && (
                    <TouchableOpacity
                      style={{justifyContent: 'center', alignItems: 'center'}}
                      onPress={() => {
                        setEventid(item.id);
                        attendenceRef.current.open();
                      }}>
                      <TextAvatar
                        backgroundColor={'green'}
                        textColor={'#FFF'}
                        size={35}
                        type={'circle'}>
                        P
                      </TextAvatar>
                      <Text
                        style={{
                          fontSize: responsiveFontSize(1.3),
                          fontFamily: 'product',
                        }}>
                        Present
                      </Text>
                    </TouchableOpacity>
                  )}
                  {item.is_present == '0' && (
                    <TouchableOpacity
                      style={{justifyContent: 'center', alignItems: 'center'}}
                      onPress={() => {
                        setEventid(item.id);
                        attendenceRef.current.open();
                      }}>
                      <TextAvatar
                        backgroundColor={'red'}
                        textColor={'#FFF'}
                        size={35}
                        type={'circle'}>
                        A
                      </TextAvatar>
                      <Text
                        style={{
                          fontSize: responsiveFontSize(1.3),
                          fontFamily: 'product',
                        }}>
                        Absent
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </View>
          )}
          keyExtractor={(item) => item.id}
        />

        <RBSheet
          ref={attendenceRef}
          closeOnDragDown={true}
          closeOnPressMask={false}
          height={230}
          openDuration={250}
          customStyles={{
            wrapper: {
              backgroundColor: 'transparent',
            },
            container: {
              borderTopColor: 'rgba(0, 0, 0, 0.2)',
              borderTopWidth: 3,
              borderRadius: 10,
            },
            draggableIcon: {
              backgroundColor: '#CDCDCD',
            },
          }}>
          <View>
            <TouchableOpacity
              onPress={() => {
                attendenceRef.current.close();
                _takeattendence(1, eventid);
              }}
              style={{
                height: responsiveScreenHeight(10),
                width: '100%',
                marginTop: 5,
                backgroundColor: 'white', //'#1E7A9D',
                borderBottomColor: '#00000017',
                borderBottomWidth: 1,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 15}}>
                <TextAvatar
                  backgroundColor={'green'}
                  textColor={'#FFF'}
                  size={40}
                  type={'circle'}>
                  PM
                </TextAvatar>
              </View>
              <View style={{marginLeft: 20}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontFamily: 'product',
                  }}>
                  Present Member
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                attendenceRef.current.close();
                _takeattendence(0, eventid);
              }}
              style={{
                height: responsiveScreenHeight(10),
                width: '100%',
                marginTop: 5,
                backgroundColor: 'white', //'#1E7A9D',
                borderBottomColor: '#00000017',
                borderBottomWidth: 1,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 15}}>
                <TextAvatar
                  backgroundColor={'red'}
                  textColor={'#FFF'}
                  size={40}
                  type={'circle'}>
                  A M
                </TextAvatar>
              </View>
              <View style={{marginLeft: 20}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontFamily: 'product',
                  }}>
                  Absent Member
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </RBSheet>
      </View>
    </>
  );
};

export default MemberEvent;
