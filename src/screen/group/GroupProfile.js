import React, {useEffect, useState} from 'react';
import {View, ImageBackground, TouchableOpacity, Linking} from 'react-native';
import {Text} from 'native-base';

import {useNavigation, useRoute} from '@react-navigation/native';
import {ROUTES} from '../../navigation/routes.constant';

import {_logout} from '../../component/logout';
import Heading from '../../component/Heading';
import {
  responsiveScreenHeight,
  responsiveFontSize,
  responsiveScreenWidth,
} from 'react-native-responsive-dimensions';
import * as data from '../../common/data';

import Members from '../../icon/member';
import Sms from '../../icon/sms';
import Mail from '../../icon/mail';
import Axios from 'axios';
import {LocalStorage, SERVER_URI} from '../../common';
function shuffle(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
const GroupProfile = props => {
  const navigation = useNavigation();
  const route = useRoute();
  const {name, groupid} = route.params;

  const [groupmember, setGroupmember] = useState([]);
  const [loading, setLoading] = useState(true);
  const _groupfetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/subgroupsmember`,
      {
        user_id: await LocalStorage.getUserID(),
        group_id: groupid,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);

        if (res.data.status == 'fail') {
          setGroupmember([]);
        } else {
          setGroupmember(res.data.data);
        }
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    if (loading) {
      _groupfetch();
    }
  });

  const emails = groupmember && groupmember.map(i => i.email);
  const phones = groupmember && groupmember.map(i => i.mobile_phone);

  const quotes = shuffle(data.data);
  return (
    <Heading
      text={name}
      lefticon={'ios-arrow-round-back'}
      Iconmap={
        [
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('bellpress');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        navigation.goBack();
      }}>
      <ImageBackground
        source={require('../../../assets/biblebg.png')}
        style={{height: responsiveScreenHeight(25), width: '100%'}}>
        <Text
          style={{
            color: '#FFFFFF',
            fontSize: responsiveFontSize(1.3),
            marginLeft: 15,
            marginTop: 10,
            fontFamily: 'product',
          }}>
          Bible Verse for Today
        </Text>
        <View
          style={{
            width: responsiveScreenWidth(12),
            backgroundColor: 'white',
            height: responsiveScreenHeight(0.2),
            marginLeft: 15,
            marginTop: 10,
          }}
        />
        <Text
          style={{
            color: 'white',
            fontSize: responsiveFontSize(1.8),
            width: responsiveScreenWidth(98),
            fontFamily: 'product',
            marginLeft: 15,
            marginTop: 10,
          }}>
          {quotes[0].key}
        </Text>
      </ImageBackground>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate(ROUTES.GROUPMEMBER, {groupid, name});
        }}
        style={{
          height: responsiveScreenHeight(8),
          width: '100%',
          borderTopColor: '#00000017',
          borderTopWidth: 2,
          borderBottomColor: '#00000017',
          borderBottomWidth: 2,
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{marginLeft: 30}}>
          <Members />
        </View>
        <View style={{marginLeft: 30}}>
          <Text
            style={{
              fontSize: responsiveFontSize(2),
              color: '#3A4041',
              fontWeight: '800',
              fontFamily: 'product',
            }}>
            Members
          </Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={async () => {
          loading
            ? alert('wait some time to get member details')
            : Linking.openURL(`sms:${phones}`);
        }}
        style={{
          height: responsiveScreenHeight(8),
          width: '100%',
          borderTopColor: '#00000017',
          borderTopWidth: 2,
          borderBottomColor: '#00000017',
          borderBottomWidth: 2,
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{marginLeft: 30}}>
          <Sms />
        </View>
        <View style={{marginLeft: 30}}>
          <Text
            style={{
              fontSize: responsiveFontSize(2),
              color: '#3A4041',
              fontWeight: '800',
              fontFamily: 'product',
            }}>
            SMS Members
          </Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => {
          loading
            ? alert('wait some time to get member details')
            : Linking.openURL(`mailto:${emails}`);
        }}
        style={{
          height: responsiveScreenHeight(8),
          width: '100%',
          borderTopColor: '#00000017',
          borderTopWidth: 2,
          borderBottomColor: '#00000017',
          borderBottomWidth: 2,
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{marginLeft: 30}}>
          <Mail />
        </View>
        <View style={{marginLeft: 30}}>
          <Text
            style={{
              fontSize: responsiveFontSize(2),
              color: '#3A4041',
              fontWeight: '800',
              fontFamily: 'product',
            }}>
            Email Members
          </Text>
        </View>
      </TouchableOpacity>
    </Heading>
  );
};
export default GroupProfile;
