import * as React from 'react';
import Svg, {G, Ellipse} from 'react-native-svg';

const Bg = props => {
  return (
    <Svg
      width={props.width}
      height={props.height}
      viewBox="0 0 845.169 843.145"
      {...props}>
      <G
        transform="rotate(45.97 229.475 540.959)"
        fill="#394c68"
        stroke="#707070">
        <Ellipse cx={278} cy={319} rx={278} ry={319} stroke="none" />
        {/* <Ellipse cx={278} cy={319} rx={277.5} ry={318.5} fill="none" /> */}
      </G>
    </Svg>
  );
};

export default Bg;
