import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  Dimensions,
  RefreshControl,
} from 'react-native';
import {
  useNavigation,
  useRoute,
  useFocusEffect,
} from '@react-navigation/native';
import {
  Content,
  Footer,
  FooterTab,
  Button,
  Text,
  Right,
  Thumbnail,
  CheckBox,
} from 'native-base';
import Modal from 'react-native-modal';
import Axios from 'axios';
import {Filter} from '../../icon';
import {COLOR, LocalStorage, SERVER_URI} from '../../common';
import Heading from '../../component/Heading';
import {Capitalize} from '../../common/capatalize';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import TextAvatar from 'react-native-text-thumbnail';
import RBSheet from 'react-native-raw-bottom-sheet';
import MaterialICon from 'react-native-vector-icons/MaterialIcons';
const EventAttendence = () => {
  const navigation = useNavigation();
  const [checked, setChecked] = useState();
  const refRBSheet = useRef();
  const attendenceRef = useRef();
  const checkbocRef = useRef();
  const route = useRoute();
  const {id} = route.params;

  const [memId, setmemId] = useState();
  const [eventList, setEventList] = useState();
  const [loading, setLoading] = useState(true);
  const [filterdata, setFilterdata] = useState();
  const _fetchevent = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/eventmember`,
      {
        user_id: await LocalStorage.getUserID(),
        event_id: id,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);
        setEventList(res.data.data);
        setFilterdata(res.data.data);
      })
      .catch(err => {
        setLoading(false);
      });
  };
  const _oncheck = id => {
    const changedCheckbox =
      eventList && eventList.member.find(cb => cb.id === id);
    changedCheckbox.checked = !changedCheckbox.checked;
    const checkboxes = Object.assign({}, checked, changedCheckbox);
    setChecked({checkboxes});
  };
  useEffect(() => {
    if (loading) {
      _fetchevent();
    }
  });

  const _takeattendence = async presents => {
    const userid = await LocalStorage.getUserID();
    const token = await LocalStorage.getToken();
    const data = await eventList.member.find(cb => {
      const present = cb.checked ? cb.id : null;

      Axios.post(
        `${SERVER_URI}/attendanceaction`,
        {
          user_id: userid,
          member_id: present,
          event_id: eventList.event_id,
          is_present: presents,
        },
        {
          headers: {
            'X-API-TOKEN': token,
          },
        },
      )
        .then(async res => {
          console.log(res.data);
          _fetchevent();
        })
        .catch(err => {
          _fetchevent();
          console.log(err);
        });
    });

    const changedCheckbox =
      eventList && eventList.member.find(cb => cb.id === id);
    if (changedCheckbox && changedCheckbox.checked) {
      alert('Attendence Added Successfully');
    }
  };

  const _takeattendencemanually = async present => {
    Axios.post(
      `${SERVER_URI}/attendanceaction`,
      {
        user_id: await LocalStorage.getUserID(),
        member_id: memId,
        event_id: id,
        is_present: parseInt(present),
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        console.log(res.data);
        _fetchevent();
      })
      .catch(async err => {
        console.log(err);
        _fetchevent();
      });
  };
  useFocusEffect(
    React.useCallback(() => {
      _fetchevent();
    }, []),
  );

  //FILTER
  const presentPhones =
    filterdata && filterdata.member.map(i => i.is_check == 'true' && i);
  let presentPhone = presentPhones && presentPhones.filter(a => a !== false);

  const absentPhones =
    filterdata && filterdata.member.map(i => i.is_check == 'false' && i);
  let absentPhone = absentPhones && absentPhones.filter(a => a !== false);

  const unknownPhones =
    filterdata && filterdata.member.map(i => i.is_check == '' && i);
  let unknownPhone = unknownPhones && unknownPhones.filter(a => a !== false);

  const checkedlist =
    eventList && eventList.member.map(cb => cb.checked == true && cb);
  let checkedliststatus = checkedlist && checkedlist.filter(a => a !== false);

  return (
    <Heading
      text={'Event Attendence'}
      lefticon={'ios-arrow-round-back'}
      Iconmap={[
        {
          icon: (
            <Filter
              onPress={() => {
                refRBSheet.current.open();
              }}
              style={{margin: 10}}
              fill={COLOR.SUBLABEL}
              height={20}
              width={20}
            />
          ),
        },
        // {
        //   name: 'bell',
        //   onPress: () => {
        //     alert('notification screen');
        //   },
        // },
        {
          icon: (
            <MaterialICon
              onPress={() => {
                checkedliststatus.length === 0
                  ? alert('select member to take attendence')
                  : checkbocRef.current.open();
              }}
              style={{margin: 10}}
              name={'more-vert'}
              color={COLOR.SUBLABEL}
              size={20}
            />
          ),
        },
      ]}
      onMenuPress={() => {
        navigation.goBack();
      }}>
      <FlatList
        refreshControl={
          <RefreshControl
            colors={['#9Bd35A', '#689F38']}
            refreshing={loading}
            onRefresh={() => setLoading(true)}
          />
        }
        ListEmptyComponent={
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontFamily: 'product'}}>No Result found</Text>
          </View>
        }
        data={eventList && eventList.member}
        renderItem={({item, index}) => (
          <Content contentContainerStyle={{backgroundColor: COLOR.LIGHTBLUE}}>
            {console.log(item)}
            <View
              style={{
                height: responsiveScreenHeight(10),
                width: '100%',
                marginTop: 1,
                backgroundColor: 'white', //'#1E7A9D',
                borderBottomColor: '#00000017',
                borderBottomWidth: 1,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 15, width: '15%'}}>
                <Thumbnail
                  source={{
                    uri:
                      item.photo == 'http://app.sheepmug.com/public/uploads'
                        ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                        : item.photo,
                  }}
                />
              </View>

              <View style={{marginLeft: 15,  width: '60%'}}>
                <Text
                  numberOfLines={1}
                  style={{fontSize: 17, width: '100%', fontFamily: 'product'}}>
                  {Capitalize(item.full_name)}
                </Text>
              </View>

              <Right style={{marginRight: 15,  width: '20%'}}>
                {item.is_check == '' && (
                  <CheckBox
                    onPress={() => {
                      _oncheck(item.id);
                    }}
                    color={COLOR.DARKBLUE}
                    style={{marginRight: 20}}
                    checked={item.checked}
                  />
                )}
                {item.is_check == 'true' && (
                  <TouchableOpacity
                    style={{justifyContent: 'center', alignItems: 'center'}}
                    onPress={async () => {
                      await setmemId(item.id);
                      await attendenceRef.current.open();
                    }}>
                    <TextAvatar
                      backgroundColor={'green'}
                      textColor={'#FFF'}
                      size={35}
                      type={'circle'}>
                      P
                    </TextAvatar>
                    <Text
                      style={{
                        fontSize: responsiveFontSize(1.3),
                        fontFamily: 'product',
                      }}>
                      Present
                    </Text>
                  </TouchableOpacity>
                )}
                {item.is_check == 'false' && (
                  <TouchableOpacity
                    onPress={async () => {
                      await setmemId(item.id);
                      await attendenceRef.current.open();
                    }}>
                    <TextAvatar
                      backgroundColor={'red'}
                      textColor={'#FFF'}
                      size={35}
                      type={'circle'}>
                      A
                    </TextAvatar>
                    <Text
                      style={{
                        fontSize: responsiveFontSize(1.3),
                        fontFamily: 'product',
                      }}>
                      Absent
                    </Text>
                  </TouchableOpacity>
                )}
              </Right>
            </View>
          </Content>
        )}
        keyExtractor={item => item.key}
      />

      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        height={300}
        openDuration={250}
        customStyles={{
          wrapper: {
            backgroundColor: 'transparent',
          },
          container: {
            borderTopColor: 'rgba(0, 0, 0, 0.2)',
            borderTopWidth: 3,
            borderRadius: 10,
          },
          draggableIcon: {
            backgroundColor: '#CDCDCD',
          },
        }}>
        <View>
          <TouchableOpacity
            onPress={() => {
              setEventList({member: presentPhone});
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 15}}>
              <TextAvatar
                backgroundColor={'green'}
                textColor={'#FFF'}
                size={40}
                type={'circle'}>
                P M
              </TextAvatar>
            </View>
            <View style={{marginLeft: 20}}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                }}>
                Present Member List
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              setEventList({member: absentPhone});
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 15}}>
              <TextAvatar
                backgroundColor={'red'}
                textColor={'#FFF'}
                size={40}
                type={'circle'}>
                A M
              </TextAvatar>
            </View>
            <View style={{marginLeft: 20}}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                }}>
                Absent Member List
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              setEventList({member: unknownPhone});
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 15}}>
              <TextAvatar
                backgroundColor={'#1E7A9D'}
                textColor={'#FFF'}
                size={40}
                type={'circle'}>
                U M
              </TextAvatar>
            </View>
            <View style={{marginLeft: 20}}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                }}>
                Unknown Member List
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </RBSheet>

      <RBSheet
        ref={attendenceRef}
        closeOnDragDown={true}
        closeOnPressMask={false}
        height={230}
        openDuration={250}
        customStyles={{
          wrapper: {
            backgroundColor: 'transparent',
          },
          container: {
            borderTopColor: 'rgba(0, 0, 0, 0.2)',
            borderTopWidth: 3,
            borderRadius: 10,
          },
          draggableIcon: {
            backgroundColor: '#CDCDCD',
          },
        }}>
        <View>
          <TouchableOpacity
            onPress={() => {
              _takeattendencemanually(1);
              attendenceRef.current.close();
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 15}}>
              <TextAvatar
                backgroundColor={'green'}
                textColor={'#FFF'}
                size={40}
                type={'circle'}>
                PM
              </TextAvatar>
            </View>
            <View style={{marginLeft: 20}}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                }}>
                Present Member
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              _takeattendencemanually(0);
              attendenceRef.current.close();
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 15}}>
              <TextAvatar
                backgroundColor={'red'}
                textColor={'#FFF'}
                size={40}
                type={'circle'}>
                A M
              </TextAvatar>
            </View>
            <View style={{marginLeft: 20}}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                }}>
                Absent Member
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </RBSheet>
      <RBSheet
        ref={checkbocRef}
        closeOnDragDown={true}
        closeOnPressMask={false}
        height={230}
        openDuration={250}
        customStyles={{
          wrapper: {
            backgroundColor: 'transparent',
          },
          container: {
            borderTopColor: 'rgba(0, 0, 0, 0.2)',
            borderTopWidth: 3,
            borderRadius: 10,
          },
          draggableIcon: {
            backgroundColor: '#CDCDCD',
          },
        }}>
        <View>
          <TouchableOpacity
            onPress={() => {
              _takeattendence(1);
              checkbocRef.current.close();
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 15}}>
              <TextAvatar
                backgroundColor={'green'}
                textColor={'#FFF'}
                size={40}
                type={'circle'}>
                PM
              </TextAvatar>
            </View>
            <View style={{marginLeft: 20}}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                }}>
                Present Member
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              _takeattendence(0);
              checkbocRef.current.close();
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 15}}>
              <TextAvatar
                backgroundColor={'red'}
                textColor={'#FFF'}
                size={40}
                type={'circle'}>
                A M
              </TextAvatar>
            </View>
            <View style={{marginLeft: 20}}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                }}>
                Absent Member
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </RBSheet>
    </Heading>
  );
};
export default EventAttendence;
{
  /* <Footer>
<FooterTab>
  <Button
    onPress={() => {
      _takeattendence(0);
    }}
    style={{backgroundColor: '#F0402A'}}>
    <Text style={{color: 'white', fontSize: 15, fontFamily: 'product'}}>
      ABSENT
    </Text>
  </Button>
  <Button
    onPress={() => {
      _takeattendence(1);
    }}
    style={{backgroundColor: '#53D575'}}>
    <Text style={{color: 'white', fontSize: 15, fontFamily: 'product'}}>
      PRESENT
    </Text>
  </Button>
</FooterTab>
</Footer> */
}

{
  /* <Modal isVisible={show}>
  <TouchableOpacity
    style={{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      width: Dimensions.get('window').width,
    }}
    onPress={() => {
      setShow(!show);
    }}>
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get('window').width,
      }}>
      <View
        style={{
          height: 180,
          width: Dimensions.get('window').width - 100,
          backgroundColor: 'white',
        }}>
        <Text
          style={{
            alignSelf: 'center',
            fontSize: 25,
            padding: 20,
            fontFamily: 'sf',
          }}>
          Attendence
        </Text>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            position: 'absolute',
            bottom: 0,
          }}>
          <Button
            onPress={() => {
              _takeattendencemanually(1);
              setShow(!show);
            }}
            style={{
              height: 60,
              backgroundColor: COLOR.DARKBLUE,
              width: '50%',
              justifyContent: 'center',
            }}>
            <Text style={{alignSelf: 'center', fontFamily: 'product'}}>
              Present
            </Text>
          </Button>
          <Button
            onPress={() => {
              _takeattendencemanually(0);
              setShow(!show);
            }}
            style={{
              height: 60,
              backgroundColor: COLOR.YELLOW,
              width: '50%',
              justifyContent: 'center',
            }}>
            <Text style={{alignSelf: 'center', fontFamily: 'product'}}>
              Absent
            </Text>
          </Button>
        </View>
      </View>
    </View>
  </TouchableOpacity>
</Modal> */
}
