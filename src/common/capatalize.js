export const Capitalize = (str) => {
  return str == null
    ? str
    : str.replace(/\w\S*/g, function (txt) {
      let text = txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      text = text.replace(/-/g, ' ');
        return text;
      });
};
