import * as React from 'react';
import Svg, {Defs, Path} from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: style */

function Mail(props) {
  return (
    <Svg width={30} height={30} viewBox="0 0 23.96 23.96" {...props}>
      <Defs />
      <Path fill={'#00479d'} d="M16.7 7.739H7.26l4.72 4.046zm0 0" />
      <Path
        fill={'#00479d'}
        d="M11.98 12.824a.417.417 0 01-.272-.1l-5.16-4.423v7.866h10.864V8.301l-5.16 4.423a.417.417 0 01-.272.1zm0 0"
      />
      <Path
        fill={'#00479d'}
        d="M11.98 0a11.98 11.98 0 1011.98 11.98A11.98 11.98 0 0011.98 0zm6.42 16.686a.428.428 0 01-.428.428H5.99a.428.428 0 01-.428-.428V7.273a.428.428 0 01.428-.428h11.98a.428.428 0 01.428.428zm0 0"
      />
    </Svg>
  );
}

export default Mail;
