import React, {useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  StatusBar,
} from 'react-native';
import {ROUTES} from '../navigation/routes.constant';
import {LocalStorage} from '../common';
import {useFocusEffect} from '@react-navigation/native';
import {Container} from 'native-base';

const width = Dimensions.get('screen').width;
const height = Dimensions.get('screen').height;

const SplashScreen = (props) => {
  const _bootstrapAsync = async () => {
    const userToken = await LocalStorage.getToken();

    const intro = await LocalStorage.getIntro();
    if (intro == 'true') {
      props.navigation.navigate(
        userToken ? ROUTES.DRAWERSTACK : ROUTES.AUTHSTACK,
      );
    } else {
      props.navigation.navigate(ROUTES.INTRO);
    }
  };

  useEffect(() => {
    _bootstrapAsync();
  });
  useFocusEffect(() => {
    _bootstrapAsync();
  });
  return (
    <Container style={{backgroundColor: '#FFFFFF'}}>
      <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
      <View style={styles.container}>
        {/* <Image
          source={require('../../assets/playstore.png')}
          resizeMode={'contain'}
          style={styles.image}
        /> */}
        <Text style={{color: 'white', fontSize: 20}}>Loading</Text>
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    width: width,
    height: height,
  },
  image: {
    width: 150,
  },
});

export default SplashScreen;
