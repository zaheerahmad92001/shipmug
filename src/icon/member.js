import * as React from "react"
import Svg, { Defs, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: style */

function Members(props) {
  return (
    <Svg width={30} height={30} viewBox="0 0 15.999 16.189" {...props}>
      <Defs></Defs>
      <Path
      fill={"#e48e66"}
        d="M12.758 1.876h1.556a1.684 1.684 0 011.684 1.684v10.944a1.684 1.684 0 01-1.684 1.684H1.684A1.684 1.684 0 010 14.504V3.56a1.684 1.684 0 011.684-1.684zm0 0"
      />
      <Path
         fill={"#e48e66"}
        d="M8 1.876H1.684A1.684 1.684 0 000 3.56v10.944a1.684 1.684 0 001.684 1.684H8zm0 0"
      />
      <Path
        fill={"#e2d574"}
        d="M3.241 4.183a.421.421 0 01-.421-.421V.421a.421.421 0 01.842 0v3.34a.421.421 0 01-.421.422zm0 0M12.757 4.183a.421.421 0 01-.421-.421V.421a.421.421 0 11.842 0v3.34a.421.421 0 01-.421.422zm0 0M12.525 10.292H3.473a.421.421 0 110-.842h9.05a.421.421 0 110 .842zm0 0M12.525 13.309H3.473a.421.421 0 010-.842h9.05a.421.421 0 010 .842zm0 0M8.373 7.275h-4.9a.421.421 0 110-.842h4.9a.421.421 0 010 .842zm0 0"
      />
    </Svg>
  )
}

export default Members
