import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  ActivityIndicator,
  ImageBackground,
  TouchableOpacity,
  Linking,
  StyleSheet,
  ToastAndroid
} from 'react-native';
import {
  useNavigation,
  useRoute,
  useFocusEffect,
} from '@react-navigation/native';
import moment from 'moment';
import Axios from 'axios';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import {Button, Text, Badge, Content} from 'native-base';
import RBSheet from 'react-native-raw-bottom-sheet';
import {COLOR, SERVER_URI, LocalStorage} from '../../common';
import {ROUTES} from '../../navigation/routes.constant';
import {Capitalize} from '../../common/capatalize';
import Heading from '../../component/Heading';
import EventMember from '../../icon/EventMember';
import Sms from '../../icon/sms';
import Mail from '../../icon/mail';
import Attendence from '../../icon/Attendence';
import TextAvatar from 'react-native-text-thumbnail';
import {Icon}from 'native-base';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import DropdownAlert from 'react-native-dropdownalert';

const EventProfile = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const {id, name, item} = route.params;
  // console.log('i receive props',item)

  const [eventList, setEventList] = useState([]);
  const [eventdata, setEventData] = useState();
  const [loading, setLoading] = useState(true);
  const [permission , setPermission] = useState();
  

  const [expand, setExpand] = useState({
    details: false,
    sms: false,
    email: false,
    note: false,
  });
  const  refRBSheet = useRef()
  const  emailefRBSheet = useRef()
  let  menuRef = useRef()
  let dropDownAlertRef = useRef()




  const _fetchevent = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/eventdetail`,
      {
        user_id: await LocalStorage.getUserID(),
        event_id: id,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        // console.log('here is response',res.data.data.selected_group)
        setLoading(false);
        setEventList(res.data.data);
      })
      .catch(err => {
        setLoading(false);
      });
  };

  const showMenu = async()=>{
    if(menuRef.current){
      menuRef.current.show()
    }
     
  }
  const hideMenu= async()=>{
    if(menuRef.current){
     await menuRef.current.hide()
    }
  }

const editEvent=()=>{
  hideMenu()
  navigation.navigate('EDITEVENT',{
    item,
    eventList,
    eventdata,
  })

}
const deleteEvent= async ()=>{
  hideMenu()
  // let permission = await LocalStorage.getPermission()
  if(permission.eventsdelete){
  let eventId = item.id
  let uid = await LocalStorage.getUserID()
  let token = await LocalStorage.getToken()
  Axios.post(
    `${SERVER_URI}/eventdelete/${eventId}`,
    {
      user_id: uid,
    },
    {
      headers: {
        'X-API-TOKEN':token,
      },
    },
  )
    .then(async res => {
      // console.log('response success ', res)
      ToastAndroid.show('Event deleted',ToastAndroid.SHORT)
      navigation.pop()
    })
    .catch(err => {
      console.log('res error',err)
    });

  }else{
    dropDownAlertRef.current.alertWithType(
      'error',
      'Error',
      'You are not allowed to Delete Event pleae contact with Adminstration ',
    );
  }

}

  const _fetchcurrenteventdata = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/eventmember`,
      {
        user_id: await LocalStorage.getUserID(),
        event_id: id,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);

        setEventData(res.data.data);
      })
      .catch(err => {
        setLoading(false);
      });
  };

  const getPermissions= async ()=>{
    let token = await LocalStorage.getToken()
    let uid = await LocalStorage.getUserID()
    const scope = this
  
    Axios({
       method: 'get',
       url: `${SERVER_URI}/get_user_permission_list/${uid}`,
       headers: {
           'X-API-TOKEN': token,
       },
   })
       .then(function (response) {
          // console.log('success response fun:', response.data.data.eventsdelete);
           setPermission(response.data.data)
          //  console.log('here is dele',permission.eventsdelete)
       })
       .catch(function (response) {
           //handle error
           console.log('error response :', response);
       });

   }


  useEffect(() => {
    if (loading) {
      _fetchevent();
      _fetchcurrenteventdata();
      getPermissions()
    }
    // console.log('state data', permission.eventsdelete)
    // console.log('hhhhhhh',menuRef)
  });

  useFocusEffect(
    React.useCallback(() => {
      _fetchevent();
      _fetchcurrenteventdata();
    }, []),
  );

  const _onopencollapse = type => {
    if (type == 'details') {
      setExpand({
        details: true,
        sms: false,
        email: false,
        note: false,
      });
    } else if (type == 'sms') {
      setExpand({
        details: false,
        sms: true,
        email: false,
        note: false,
      });
    } else if (type == 'email') {
      setExpand({
        details: false,
        sms: false,
        email: true,
        note: false,
      });
    } else if (type == 'note') {
      setExpand({
        details: false,
        sms: false,
        email: false,
        note: true,
      });
    } else {
      setExpand({
        details: false,
        sms: false,
        email: false,
        note: false,
      });
    }
  };
  //PHONE
  const presentPhones =
    eventdata &&
    eventdata.member.map(i => i.is_check == 'true' && i.mobile_phone);
  let presentPhone = presentPhones && presentPhones.filter(a => a !== false);
  const absentPhones =
    eventdata &&
    eventdata.member.map(i => i.is_check == 'false' && i.mobile_phone);
  let absentPhone = absentPhones && absentPhones.filter(a => a !== false);
  const unknownPhones =
    eventdata && eventdata.member.map(i => i.is_check == '' && i.mobile_phone);
  let unknownPhone = unknownPhones && unknownPhones.filter(a => a !== false);
  //EMAIL
  const presentEmails =
    eventdata && eventdata.member.map(i => i.is_check == 'true' && i.email);
  let presentEmail = presentEmails && presentEmails.filter(a => a !== false);
  const absentEmails =
    eventdata && eventdata.member.map(i => i.is_check == 'false' && i.email);
  let absentEmail = absentEmails && absentEmails.filter(a => a !== false);
  const unknownEmails =
    eventdata && eventdata.member.map(i => i.is_check == '' && i.email);
  let unknownEmail = unknownEmails && unknownEmails.filter(a => a !== false);

  return eventList == '' ? (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLOR.LIGHTBLUE,
        height: '100%',
        width: '100%',
      }}>
      <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
    </View>
  ) : (
    <>
      <Heading
        text={name}
        lefticon={'ios-arrow-round-back'}
        Iconmap={
          [
            // {
            //   name: 'bell',
            //   onPress: () => {
            //     // alert('bellpress');
            //   },
            // },
          ]
        }
        iconType={true}
        rightIconPess={showMenu}
        onMenuPress={() => {
          navigation.goBack();
        }}>
        {loading && eventList && eventList[0] ? (
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
          </View>
        ) : (
          <Content>
            <ImageBackground
              source={{
                uri:
                  eventList[0] &&
                  eventList[0].featured_image ==
                    'http://app.sheepmug.com/public/uploads'
                    ? 'https://via.placeholder.com/300.png'
                    : eventList[0].featured_image,
              }}
              style={{height: responsiveScreenHeight(30), width: '100%'}}
            />

            <TouchableOpacity
              onPress={() => {
                expand.details
                  ? _onopencollapse(null)
                  : _onopencollapse('details');
              }}
              style={{
                height: responsiveScreenHeight(8),
                width: '100%',
                borderTopColor: '#00000017',
                borderTopWidth: 2,
                borderBottomColor: '#00000017',
                borderBottomWidth: 2,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 30}}>
                <EventMember width={30} height={30} />
              </View>
              <View style={{marginLeft: 30}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontWeight: '800',
                    fontFamily: 'product',
                  }}>
                  Event Details
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-end',

                  alignItems: 'center',
                  flex: 1,
                }}>
                <Badge
                  primary
                  style={{
                    marginLeft: 10,
                    width: 50,
                    height: 30,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={{fontFamily: 'product'}}>
                    {eventList[0].total_member}
                  </Text>
                </Badge>
                <Badge
                  success
                  style={{
                    marginLeft: 10,
                    width: 50,
                    height: 30,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={{fontFamily: 'product'}}>
                    {eventList[0].present}
                  </Text>
                </Badge>
                <Badge
                  style={{
                    marginLeft: 10,
                    width: 50,
                    height: 30,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginRight: 10,
                  }}>
                  <Text style={{fontFamily: 'product'}}>
                    {eventList[0].absent}
                  </Text>
                </Badge>
              </View>
            </TouchableOpacity>

            {expand.details && (
              <View
                style={{
                  height: 250,
                  width: '100%',
                  padding: 15,
                  position: 'relative',
                }}>
                <Text
                  style={{
                    color: COLOR.SUBLABEL,
                    fontFamily: 'product',
                    fontSize: responsiveFontSize(2),
                  }}>
                  Title
                </Text>
                <Text
                  style={{
                    color: '#3A4041',
                    fontFamily: 'product',
                    fontSize: responsiveFontSize(2),
                    marginTop: 10,
                  }}>
                  {Capitalize(eventList[0].name)}
                </Text>
                <Text
                  style={{
                    marginTop: 10,
                    color: COLOR.SUBLABEL,
                    fontFamily: 'product',
                    fontSize: responsiveFontSize(2),
                  }}>
                  Date
                </Text>
                <Text
                  style={{
                    color: '#3A4041',
                    fontFamily: 'product',
                    fontSize: responsiveFontSize(2),
                    marginTop: 10,
                  }}>
                  {moment(eventList[0].start_date).format('dddd, Do MMMM, YY')}
                </Text>
                <Text
                  style={{
                    marginTop: 10,
                    color: COLOR.SUBLABEL,
                    fontFamily: 'product',
                    fontSize: responsiveFontSize(2),
                  }}>
                  Groups
                </Text>
                <Text
                  style={{
                    color: '#3A4041',
                    fontFamily: 'product',
                    fontSize: responsiveFontSize(2),
                    marginTop: 10,
                  }}>
                  {Capitalize(eventList[0].groups)}
                </Text>
              </View>
            )}
            <TouchableOpacity
              onPress={() => {
                navigation.navigate(ROUTES.EVENTEATTENDENCE, {id: id});
              }}
              style={{
                height: responsiveScreenHeight(8),
                width: '100%',
                borderTopColor: '#00000017',
                borderTopWidth: 2,
                borderBottomColor: '#00000017',
                borderBottomWidth: 2,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 30}}>
                <Attendence width={30} height={30} />
              </View>
              <View style={{marginLeft: 30}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontWeight: '800',
                    fontFamily: 'product',
                  }}>
                  Attendance
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                // expand.sms ? _onopencollapse(null) : _onopencollapse('sms');
                refRBSheet.current.open();
              }}
              style={{
                height: responsiveScreenHeight(8),
                width: '100%',
                borderTopColor: '#00000017',
                borderTopWidth: 2,
                borderBottomColor: '#00000017',
                borderBottomWidth: 2,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 30}}>
                <Sms />
              </View>
              <View style={{marginLeft: 30}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontWeight: '800',
                    fontFamily: 'product',
                  }}>
                  SMS Members
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                emailefRBSheet.current.open();
              }}
              style={{
                height: responsiveScreenHeight(8),
                width: '100%',
                borderTopColor: '#00000017',
                borderTopWidth: 2,
                borderBottomColor: '#00000017',
                borderBottomWidth: 2,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 30}}>
                <Mail />
              </View>
              <View style={{marginLeft: 30}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontWeight: '800',
                    fontFamily: 'product',
                  }}>
                  Email Members
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                expand.note ? _onopencollapse(null) : _onopencollapse('note');
              }}
              style={{
                height: responsiveScreenHeight(8),
                width: '100%',
                borderTopColor: '#00000017',
                borderTopWidth: 2,
                borderBottomColor: '#00000017',
                borderBottomWidth: 2,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 30}}>
                <Mail />
              </View>
              <View style={{marginLeft: 30}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontWeight: '800',
                    fontFamily: 'product',
                  }}>
                  Program Outlines
                </Text>
              </View>
            </TouchableOpacity>
            {expand.note && (
              <View
                style={{
                  height: 250,
                  width: '100%',
                  padding: 15,
                  position: 'relative',
                }}>
                <Text
                  style={{
                    color: COLOR.SUBLABEL,
                    fontFamily: 'product',
                    fontSize: responsiveFontSize(2),
                  }}>
                  Notes
                </Text>

                <Text
                  style={{
                    color: '#3A4041',
                    fontFamily: 'product',
                    fontSize: responsiveFontSize(2),
                    marginTop: 10,
                  }}>
                  {eventList[0].notes == ''
                    ? 'No notes found'
                    : eventList[0].notes}
                </Text>
              </View>
            )}
          </Content>
        )}

        <RBSheet
          ref={refRBSheet}
          closeOnDragDown={true}
          closeOnPressMask={false}
          height={300}
          openDuration={250}
          customStyles={{
            wrapper: {
              backgroundColor: 'transparent',
            },
            container: {
              borderTopColor: 'rgba(0, 0, 0, 0.2)',
              borderTopWidth: 3,
              borderRadius: 10,
            },
            draggableIcon: {
              backgroundColor: '#CDCDCD',
            },
          }}>
          <View>
            <TouchableOpacity
              onPress={() => {
                loading
                  ? alert('wait some time to get member details')
                  : Linking.openURL(`sms:${presentPhone}`);
              }}
              style={{
                height: responsiveScreenHeight(10),
                width: '100%',
                marginTop: 5,
                backgroundColor: 'white', //'#1E7A9D',
                borderBottomColor: '#00000017',
                borderBottomWidth: 1,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 15}}>
                <TextAvatar
                  backgroundColor={'green'}
                  textColor={'#FFF'}
                  size={40}
                  type={'circle'}>
                  PM
                </TextAvatar>
              </View>
              <View style={{marginLeft: 20}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontFamily: 'product',
                  }}>
                  Present Members SMS
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                loading
                  ? alert('wait some time to get member details')
                  : Linking.openURL(`sms:${absentPhone}`);
              }}
              style={{
                height: responsiveScreenHeight(10),
                width: '100%',
                marginTop: 5,
                backgroundColor: 'white', //'#1E7A9D',
                borderBottomColor: '#00000017',
                borderBottomWidth: 1,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 15}}>
                <TextAvatar
                  backgroundColor={'red'}
                  textColor={'#FFF'}
                  size={40}
                  type={'circle'}>
                  AM
                </TextAvatar>
              </View>
              <View style={{marginLeft: 20}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontFamily: 'product',
                  }}>
                  Absent Members SMS
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                loading
                  ? alert('wait some time to get member details')
                  : Linking.openURL(`sms:${unknownPhone}`);
              }}
              style={{
                height: responsiveScreenHeight(10),
                width: '100%',
                marginTop: 5,
                backgroundColor: 'white', //'#1E7A9D',
                borderBottomColor: '#00000017',
                borderBottomWidth: 1,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 15}}>
                <TextAvatar
                  backgroundColor={'#1E7A9D'}
                  textColor={'#FFF'}
                  size={40}
                  type={'circle'}>
                  UM
                </TextAvatar>
              </View>
              <View style={{marginLeft: 20}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontFamily: 'product',
                  }}>
                  Unknown Members SMS
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </RBSheet>

        <RBSheet
          ref={emailefRBSheet}
          closeOnDragDown={true}
          closeOnPressMask={false}
          height={300}
          openDuration={250}
          customStyles={{
            wrapper: {
              backgroundColor: 'transparent',
            },
            container: {
              borderTopColor: 'rgba(0, 0, 0, 0.2)',
              borderTopWidth: 3,
              borderRadius: 10,
            },
            draggableIcon: {
              backgroundColor: '#CDCDCD',
            },
          }}>
          <View>
            <TouchableOpacity
              onPress={() => {
                loading
                  ? alert('wait some time to get member details')
                  : Linking.openURL(`mailto:${presentEmail}`);
              }}
              style={{
                height: responsiveScreenHeight(10),
                width: '100%',
                marginTop: 5,
                backgroundColor: 'white', //'#1E7A9D',
                borderBottomColor: '#00000017',
                borderBottomWidth: 1,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 15}}>
                <TextAvatar
                  backgroundColor={'green'}
                  textColor={'#FFF'}
                  size={40}
                  type={'circle'}>
                  PM
                </TextAvatar>
              </View>
              <View style={{marginLeft: 20}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontFamily: 'product',
                  }}>
                  Present Members EMAIL
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                loading
                  ? alert('wait some time to get member details')
                  : Linking.openURL(`mailto:${absentEmail}`);
              }}
              style={{
                height: responsiveScreenHeight(10),
                width: '100%',
                marginTop: 5,
                backgroundColor: 'white', //'#1E7A9D',
                borderBottomColor: '#00000017',
                borderBottomWidth: 1,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 15}}>
                <TextAvatar
                  backgroundColor={'red'}
                  textColor={'#FFF'}
                  size={40}
                  type={'circle'}>
                  AM
                </TextAvatar>
              </View>
              <View style={{marginLeft: 20}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontFamily: 'product',
                  }}>
                  Absent Members EMAIL
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                loading
                  ? alert('wait some time to get member details')
                  : Linking.openURL(`mailto:${unknownEmail}`);
              }}
              style={{
                height: responsiveScreenHeight(10),
                width: '100%',
                marginTop: 5,
                backgroundColor: 'white', //'#1E7A9D',
                borderBottomColor: '#00000017',
                borderBottomWidth: 1,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 15}}>
                <TextAvatar
                  backgroundColor={'#1E7A9D'}
                  textColor={'#FFF'}
                  size={40}
                  type={'circle'}>
                  UM
                </TextAvatar>
              </View>
              <View style={{marginLeft: 20}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontFamily: 'product',
                  }}>
                  Unknown Members EMAIL
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </RBSheet>

      <View style={styles.menuView}>
          <Menu 
            ref={menuRef}>
              <MenuItem
              onPress={editEvent}
              style={styles.menuText}>
                Edit Event
              </MenuItem>
         
            <MenuDivider/>
              <MenuItem
                 onPress={deleteEvent}
                style={styles.menuText}
               >
                Delete Event
              </MenuItem>
            <MenuDivider />
          </Menu>
        </View>

        <DropdownAlert ref={dropDownAlertRef} />
      </Heading>
    </>
  );
};
export default EventProfile;

const styles= StyleSheet.create({
  menuView: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
    top: 20,
    right:30,
    position: 'absolute',
    // width:responsiveFontSize(12),
  },
  menuItemStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal:10,
   
    
  },
})
