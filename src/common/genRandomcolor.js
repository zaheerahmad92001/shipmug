function random(mn, mx) {
  return Math.random() * (mx - mn) + mn;
}

function get_random(arr) {
  return arr[Math.floor(random(1, 5)) - 1];
}
const getRandomColor = () => {
  var arr = ['#1E7A9D', '#FFAF02', '#53D575', '#F0402A'];
  return get_random(arr);
  // var letters = '0123456789ABCDEF';
  // var color = '#';
  // for (var i = 0; i < 6; i++) {
  //   color += letters[Math.floor(Math.random() * 16)];
  // }
  // return color;
};
export default getRandomColor;
