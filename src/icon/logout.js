import * as React from 'react';
import Svg, {Defs, Path} from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: style */

function Logout(props) {
  return (
    <Svg viewBox="0 0 13.483 17.773" {...props}>
      <Defs></Defs>
      <Path fill={'#5eb3d1'} d="M1.839 0h11.644v15.934H1.839zm0 0" />
      <Path d="M9.499 2.145h-.613v13.789h3.371V4.903zm0 0" fill="#4fa1b7" />
      <Path d="M11.644 17.774H0V1.839h8.579l3.064 3.064zm0 0" fill="#87ced9" />
      <Path d="M11.644 4.903H8.58V1.839zm0 0" fill="#b9e6ed" />
      <Path
        fill={'#5eb3d1'}
        d="M2.145 5.822h7.354v.613H2.145zm0 0M2.145 3.983h2.451v.613H2.145zm0 0M2.145 7.661h4.9v.613h-4.9zm0 0M7.661 7.661H9.5v.613H7.661zm0 0M2.145 9.499h7.354v.613H2.145zm0 0M2.145 13.176h7.354v.613H2.145zm0 0M4.903 11.338h4.6v.613h-4.6zm0 0M2.145 11.338H4.29v.613H2.145zm0 0M2.145 15.015h1.226v.613H2.145zm0 0M3.984 15.015H9.5v.613H3.984zm0 0"
      />
    </Svg>
  );
}

export default Logout;
