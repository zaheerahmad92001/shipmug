import React, { Component } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
const {width:screenWidth, height:screenHeight} = Dimensions.get('window')
export default StyleSheet.create({
    contentView: {
        marginHorizontal: 25,
        marginTop: 20,
    },
    heading: {
        // color:'#D3D3D3',
        color: 'grey',
        fontSize: 12,
        fontWeight: 'normal',

    },
    inputField: {
        paddingTop: 5,
        paddingBottom: 15,
        paddingHorizontal: 0,
        fontSize: 16,
    },
    assignGroupView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    inputContainerStyle: {
        height: 40,
        borderBottomColor: 'transparent',
        paddingTop: 5,
        paddingBottom: 0,
    },
    timeView: {
        paddingVertical: 10,
        marginBottom: 5
    },
    dummyTime: {
        color: 'black'
    },
    imgUpload: {
        marginBottom: 15,
    },
    notesOutline: {
        marginTop: 10,
        color: 'grey'
    },
    buttonView: {
        backgroundColor: '#3b5998',
        position: 'absolute',
        bottom: 0, width: '100%',
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center'

    },
    createEvent: {
        color: 'white'
    },
    imgPreview: {
        alignSelf: 'flex-start',
        marginLeft: 5,
        // width:50,height:50,
        // borderRadius:25
    },
    selectGroup: {
        marginVertical: 13
    },

    content: {
        paddingBottom: 10,
        backgroundColor: 'white',
        marginHorizontal: 20,
        flexDirection: 'row',
    },
    itemName: {
        marginLeft: 15,
    },
    cancelBtn: {
        paddingVertical: 15,
        width: '40%',
        marginRight: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'red'
    },
    doneBtn:{
        paddingVertical:15,
        width:'40%',
        marginLeft:10,
        borderRadius:10,
        borderWidth:1,
        borderColor:'#3b5998',
    },
    OverlayStyle: {
        paddingVertical: 0,
        paddingHorizontal: 0,
        width: screenWidth*0.8 ,
        // height: RFValue(250),
        // paddingBottom:RFValue(50),
        // top: RFValue(60),
        // backgroundColor: White
    },
    overlayView: {
        width: screenWidth * 0.80,
    },

})