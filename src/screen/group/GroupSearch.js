import React, {useState} from 'react';
import {
  FlatList,
  View,
  ActivityIndicator,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Header from '../../component/Header';
import TextAvatar from 'react-native-text-thumbnail';
import {
  Content,
  Text,
  Item,
  Button,
  Header as Head,
  Input,
  Icon,
  ListItem,
  Left,
  Thumbnail,
  Body,
} from 'native-base';
import {COLOR, SERVER_URI, LocalStorage, getRandomColor} from '../../common';
import {ROUTES} from '../../navigation/routes.constant';
import Axios from 'axios';
import {Capitalize} from '../../common/capatalize';
import Heading from '../../component/Heading';
import {
  responsiveFontSize,
  responsiveScreenHeight,
} from 'react-native-responsive-dimensions';
import TextAvatarComp from '../../component/textAvatarComponent';
const GroupSearch = () => {
  const [search, setSearch] = useState('');
  const navigation = useNavigation();
  const [member, setMember] = useState();
  const [loading, setLoading] = useState(false);

  const _memberFetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/groups`,
      {
        user_id: await LocalStorage.getUserID(),
        key_word: search,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);
        setMember(res.data.data);
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };
  return (
    // <>
    //   <Header
    //     leftIcon={'arrow-back'}
    //     leftClick={() => {
    //       navigation.goBack();
    //     }}
    //     title={'Group Search'}>
    //     <Content
    //       contentContainerStyle={{
    //         width: '100%',
    //         backgroundColor: COLOR.LIGHTBLUE,
    //       }}>
    //       <Head searchBar rounded style={{backgroundColor: COLOR.DARKBLUE}}>
    //         <Item>
    //           <Icon name="ios-people" />
    //           <Input
    //             placeholder="Search"
    //             onChangeText={e => {
    //               setSearch(e);
    //               _memberFetch();
    //             }}
    //           />
    //           <Icon
    //             name="ios-search"
    //             onPress={() => {
    //               _memberFetch();
    //             }}
    //           />
    //         </Item>
    //       </Head>
    //       <FlatList
    //         data={member}
    //         ListFooterComponent={
    //           loading ? (
    //             <View style={{justifyContent: 'center', alignItems: 'center'}}>
    //               <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
    //             </View>
    //           ) : null
    //         }
    //         ListEmptyComponent={
    //           <View style={{justifyContent: 'center', alignItems: 'center'}}>
    //             <Text style={{fontFamily: 'sf'}}>Search Result empty</Text>
    //           </View>
    //         }
    //         renderItem={({item}) => (
    //           <TouchableOpacity
    //             onPress={() => {
    //               navigation.navigate(ROUTES.SUBGROUPLIST, {groupid: item.id});
    //             }}
    //             style={{
    //               height: 82,
    //               width: '100%',
    //               alignItems: 'center',
    //               flexDirection: 'row',
    //               backgroundColor: 'white',
    //               marginTop: 3,
    //             }}>
    //             <View style={{marginLeft: 32}}>
    //               <TextAvatar
    //                 backgroundColor={'#F1F1F1'}
    //                 textColor={COLOR.DARKBLUE}
    //                 size={60}
    //                 type={'circle'}>
    //                 {Capitalize(item.name)}
    //               </TextAvatar>
    //             </View>
    //             <View style={{marginLeft: 25}}>
    //               <Text style={{fontSize: 18, color: '#000000',fontFamily: 'sf'}}>
    //                 {Capitalize(item.name)}
    //               </Text>
    //             </View>
    //           </TouchableOpacity>
    //         )}
    //         keyExtractor={item => item.id}
    //       />
    //     </Content>
    //   </Header>
    // </>
    <Heading
      text={'Group Search'}
      lefticon={'ios-arrow-round-back'}
      Iconmap={
        [
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('notification screen');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        navigation.goBack();
      }}>
      <Content
        contentContainerStyle={{
          width: '100%',
          backgroundColor: 'white',
        }}>
        <Head searchBar rounded style={{backgroundColor: 'white'}}>
          <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
          <Item
            regular
            style={{
              marginBottom: 6,
              padding: 5,
              borderRadius: 5,
              borderWidth: 1,
            }}>
            <Icon name="ios-people" />
            <Input
              placeholder="Search"
              style={{fontFamily: 'product', color: '#b0afb0'}}
              placeholderTextColor={'#b0afb0'}
              onChangeText={e => {
                setSearch(e);
                _memberFetch();
              }}
            />

            <Icon
              name="ios-search"
              onPress={() => {
                _memberFetch();
              }}
            />
          </Item>
        </Head>
        <FlatList
          data={member}
          ListFooterComponent={
            loading ? (
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
              </View>
            ) : null
          }
          ListEmptyComponent={
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontFamily: 'product'}}>Search Result empty</Text>
            </View>
          }
          renderItem={({item}) => (
            <TextAvatarComp
              onPress={() => {
                navigation.navigate(ROUTES.SUBGROUPLIST, {groupid: item.id});
              }}
              name={Capitalize(item.name)}
              textavarname={item.name
                .split(' ')
                .slice(0, 2)
                .join('+')
                .toUpperCase()}
            />
            // <TouchableOpacity
            //   onPress={() => {
            //     navigation.navigate(ROUTES.SUBGROUPLIST, {groupid: item.id});
            //   }}
            //   style={{
            //     height: responsiveScreenHeight(8),
            //     width: '100%',
            //     borderTopColor: '#00000017',
            //     borderTopWidth: 2,
            //     borderBottomColor: '#00000017',
            //     borderBottomWidth: 2,
            //     alignItems: 'center',
            //     flexDirection: 'row',
            //   }}>
            //   <View style={{marginLeft: 15}}>
            //     <TextAvatar
            //       backgroundColor={getRandomColor()}
            //       textColor={'#FFF'}
            //       size={40}
            //       type={'circle'}>
            //       {item.name.split(' ').slice(0, 2).join('+').toUpperCase()}
            //     </TextAvatar>
            //   </View>
            //   <View style={{marginLeft: 16}}>
            //     <Text
            //       style={{
            //         fontSize: responsiveFontSize(1.5),
            //         color: '#3A4041',
            //         fontFamily: 'product',
            //       }}>
            //       {Capitalize(item.name)}
            //     </Text>
            //     <Text
            //       style={{
            //         color: '#9CA8AA',
            //         fontFamily: 'product',
            //         fontSize: responsiveFontSize(1),
            //       }}>
            //       147 Kotei Road Kotei
            //     </Text>
            //   </View>
            // </TouchableOpacity>
          )}
          keyExtractor={item => item.key}
        />
      </Content>
    </Heading>
  );
};
export default GroupSearch;
