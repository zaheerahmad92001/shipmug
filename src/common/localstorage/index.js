import AsyncStorage from '@react-native-community/async-storage';

export const LOCAL_STORAGE_LABLES = {TOKEN: 'TOKEN'};

export const LOCAL_USER_ID = {USER_ID: 'USER_ID'};

export const FOLLOW_UPS = {FOLLOW_UPS: 'FOLLOW_UPS'};

export const BOOKMARK = {BOOKMARK: 'BOOKMARK'};

export const INTRO = {INTRO: 'INTRO'};

export const CREATE_EVENT ='CREATE_EVENT';
export const EDIT_EVENT ='EDIT_EVENT';
export const DELETE_EVENT = 'DELETE_EVENT';

class LocalStorage {
  static storeToken = async (token) => {
    await AsyncStorage.setItem(LOCAL_STORAGE_LABLES.TOKEN, token);
  };
  static getToken = async () => {
    return await AsyncStorage.getItem(LOCAL_STORAGE_LABLES.TOKEN);
  };
  static clearToken = async () => {
    AsyncStorage.removeItem(LOCAL_STORAGE_LABLES.TOKEN);
  };
  static clearCache = async () => {
    AsyncStorage.clear().then(() => console.log('Cleared'));
  };

  //USER ID
  static storeUserId = async (token) => {
    await AsyncStorage.setItem(LOCAL_USER_ID.USER_ID, token);
  };
  static getUserID = async () => {
    return await AsyncStorage.getItem(LOCAL_USER_ID.USER_ID);
  };
  static clearUserID = async () => {
    AsyncStorage.removeItem(LOCAL_USER_ID.USER_ID);
  };

  //FOLLOW_UPS

  static storePermission = async (token) => {
    await AsyncStorage.setItem(FOLLOW_UPS.FOLLOW_UPS, token);
  };
  static getPermission = async () => {
    return await AsyncStorage.getItem(FOLLOW_UPS.FOLLOW_UPS);
  };
  static clearPermission = async () => {
    AsyncStorage.removeItem(FOLLOW_UPS.FOLLOW_UPS);
  };


// event create delete and update

static Store_Create_Event_Permission = async (permission) => {
  await AsyncStorage.setItem(CREATE_EVENT, permission);
};
static get_Create_Event_Permission = async () => {
  await AsyncStorage.getItem(CREATE_EVENT);
};

static Store_Edit_Event_Permission = async (edit) => {
  await AsyncStorage.setItem(EDIT_EVENT, edit);
};
static get_Edit_Event_Permission = async () => {
  await AsyncStorage.getItem(EDIT_EVENT);
};

static Store_Delete_Event_Permission = async (del) => {
  await AsyncStorage.setItem(DELETE_EVENT, del);
};

static get_Delete_Event_Permission = async () => {
  await AsyncStorage.getItem(DELETE_EVENT);
};






  // store Bookmark
  static storeBookmark = async (token) => {
    await AsyncStorage.setItem(BOOKMARK.BOOKMARK, JSON.stringify(token));
  };
  static getBookmark = async () => {
    return await AsyncStorage.getItem(BOOKMARK.BOOKMARK);
  };
  static clearBookmark = async () => {
    AsyncStorage.removeItem(BOOKMARK.BOOKMARK);
  };
  //INTRO
  static storeIntro = async (token) => {
    await AsyncStorage.setItem(INTRO.INTRO, token);
  };
  static getIntro = async () => {
    return await AsyncStorage.getItem(INTRO.INTRO);
  };
  static clearIntro = async () => {
    AsyncStorage.removeItem(INTRO.INTRO);
  };
}

export {LocalStorage};
