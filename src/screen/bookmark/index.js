import React, {useEffect, useState, useRef} from 'react';
import {
  FlatList,
  RefreshControl,
  View,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {Text, Thumbnail} from 'native-base';

import {useNavigation, useFocusEffect} from '@react-navigation/native';
import {ROUTES} from '../../navigation/routes.constant';
import Axios from 'axios';

import MaterialICon from 'react-native-vector-icons/MaterialIcons';
import {LocalStorage, SERVER_URI, COLOR} from '../../common';
import {Capitalize} from '../../common/capatalize';
import {_logout} from '../../component/logout';
import {Filter} from '../../icon';
import Heading from '../../component/Heading';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import RBSheet from 'react-native-raw-bottom-sheet';
import TextAvatar from 'react-native-text-thumbnail';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
const BookMarks = props => {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(true);
  const [memeber, setMember] = useState();

  const _memberFetch = async () => {
    try {
      const data = await LocalStorage.getBookmark();

      if (data !== null) {
        setLoading(false);
        setMember(JSON.parse(data));
      }
    } catch (error) {
      setLoading(false);
      setMember([]);
      setLoading(false);
      // Error retrieving data
    }
  };
  useEffect(() => {
    if (loading) {
      _memberFetch();
    }
  });
  useFocusEffect(
    React.useCallback(() => {
      _memberFetch();
    }, []),
  );
  return (
    <Heading
      text={'Bookmarks'}
      lefticon={'ios-menu'}
      Iconmap={
        [
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('notification screen');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        navigation.openDrawer();
      }}>
      <FlatList
        refreshControl={
          <RefreshControl
            colors={['#9Bd35A', '#689F38']}
            refreshing={loading}
            onRefresh={() => setLoading(true)}
          />
        }
        data={memeber && memeber.member}
        ListEmptyComponent={
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontFamily: 'product'}}>No Bookmark Found</Text>
          </View>
        }
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate(ROUTES.MEMBERPROFILE, {
                type: 'BOOKMARK',
                id: item.id,
                address: item.address,
                fullname: `${Capitalize(item.first_name)} ${Capitalize(
                  item.last_name,
                )}`,
                photo:
                  item.photo == 'http://app.sheepmug.com/public/uploads'
                    ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                    : `${item.photo}`,
              });
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 15}}>
              <Thumbnail
                style={{height: 50, width: 50}}
                source={{
                  uri:
                    item.photo == 'http://app.sheepmug.com/public/uploads'
                      ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                      : `${item.photo}`,
                }}
              />
            </View>
            <View style={{marginLeft: 20}}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                }}>
                {Capitalize(item.first_name)} {Capitalize(item.last_name)}
              </Text>
              <Text
                style={{color: '#9CA8AA', fontSize: responsiveFontSize(1.3)}}>
                {item.address}
              </Text>
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={item => item.key}
      />
    </Heading>
  );
};
export default BookMarks;
