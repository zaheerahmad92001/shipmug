import React, {useRef} from 'react';

import {NavigationContainer} from '@react-navigation/native';
import Appnavigator from './src/navigation/Appnavigator';
import {setCustomText} from 'react-native-global-props';
import DropdownAlert from 'react-native-dropdownalert';

import {View, Text} from 'native-base';
import {useNetInfo} from '@react-native-community/netinfo';

const App = () => {
  const customTextProps = {
    style: {
      fontFamily: 'product',
    },
  };
  setCustomText(customTextProps);
  const noInternetref = useRef();
  const netInfo = useNetInfo();

  netInfo.isConnected.toString() === 'false' &&
    noInternetref &&
    noInternetref.current &&
    noInternetref.current.alertWithType(
      'error',
      'Internet',
      'No internet connection found,kindly connect to internet',
    );

  return (
    <NavigationContainer>
      <DropdownAlert
        inactiveStatusBarBackgroundColor={'white'}
        inactiveStatusBarStyle="dark-content"
        ref={noInternetref}
        onTap={() => {
          noInternetref.current.closeAction();
        }}
        zIndex={9999999999}
      />

      <Appnavigator />
    </NavigationContainer>
  );
};
export default App;
