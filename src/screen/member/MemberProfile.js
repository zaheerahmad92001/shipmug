import React, {useState, useRef} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import {View, TouchableOpacity, RefreshControl, Linking} from 'react-native';
import {
  Text,
  Tab,
  Tabs,
  Thumbnail,
  Content,
  TabHeading,
  Row,
  Col,
} from 'native-base';
import ImageView from 'react-native-image-view';
import Heading from '../../component/Heading';
import ViewPager from '@react-native-community/viewpager';

import {COLOR, LocalStorage} from '../../common';
import MemberInformation from './MemberInformation';
import MemberEvent from './MemberEvent';
import {ROUTES} from '../../navigation/routes.constant';
import FeatherIcon from 'react-native-vector-icons/FontAwesome';
import {
  responsiveScreenHeight,
  responsiveFontSize,
  responsiveHeight,
} from 'react-native-responsive-dimensions';
import Call from '../../icon/call';
import Sms from '../../icon/sms';
import Whatsapp from '../../icon/whatsapp';
import Mail from '../../icon/mail';
const MemberProfile = props => {
  const navigation = useNavigation();
  const route = useRoute();
  const viewpagerRef = useRef();
  const [activetab, setActiveTab] = useState(0);
  const [showImage, setshowImage] = useState(false);
  const [loading, setLoading] = useState(false);
  const _refresh = () => {
    setTimeout(() => {
      setLoading(false);
    }, 5000);
  };
  const {
    id,
    fullname,
    address,
    photo,
    type,
    phone,
    email,
    last_name,
    first_name,
  } = route.params;
  const _goback = () => {
    if (type == 'GROUP') {
      navigation.navigate(ROUTES.GROUPMEMBER);
    } else if (type == 'SEARCH_MEMBER') {
      navigation.navigate(ROUTES.MEMBERSEARCH);
    } else if (type == 'MEMBER_FILTER') {
      navigation.navigate(ROUTES.MEMBERFILTER);
    } else if (type == 'SEARCH_MEMBER_GROUP') {
      navigation.navigate(ROUTES.GROUPSEARCH);
    } else if (type == 'BOOKMARK') {
      navigation.navigate(ROUTES.BOOKMARK);
    } else {
      navigation.navigate(ROUTES.MEMBERLIST);
    }
  };
  const images = [
    {
      source: {
        uri: photo,
      },
      title: fullname,
      width: 806,
      height: 720,
    },
  ];
  const _addbookmark = async () => {
    // await LocalStorage.clearBookmark();
    const old = await LocalStorage.getBookmark();
    const available = {
      id,
      address,
      last_name,
      first_name,
      photo,
      phone,
      email,
    };
    await LocalStorage.storeBookmark({...old, member: [available]});

    await alert('Members added to bookmark');
  };
  return (
    <Heading
      text={'Member Profile'}
      lefticon={'ios-arrow-round-back'}
      Iconmap={[
        {
          icon: (
            <FeatherIcon
              onPress={() => {
                _addbookmark();
              }}
              style={{margin: 10}}
              name={'bookmark-o'}
              color={COLOR.SUBLABEL}
              size={20}
            />
          ),
        },
        // {
        //   name: 'bell',
        //   onPress: () => {
        //     alert('notification screen');
        //   },
        // },
      ]}
      onMenuPress={() => {
        _goback();
      }}>
      <ImageView
        onClose={() => setshowImage(false)}
        images={images}
        imageIndex={0}
        isVisible={showImage}
        renderFooter={currentImage => (
          <View>
            <Text style={{color: 'white', fontFamily: 'product'}}>
              {fullname}
            </Text>
          </View>
        )}
      />
      <View style={{alignItems: 'center'}}>
        <TouchableOpacity onPress={() => setshowImage(true)}>
          <Thumbnail
            style={{
              marginBottom: responsiveScreenHeight(1),
              marginTop: responsiveScreenHeight(1),
            }}
            large
            source={{
              uri: photo,
            }}
          />
        </TouchableOpacity>
        <Text
          style={{
            color: '#5B6466',
            fontSize: responsiveFontSize(1.5),
            fontFamily: 'product',
          }}>
          {fullname}
        </Text>
        <Text
          style={{
            color: '#9CA8AA',
            fontSize: responsiveFontSize(1),
            fontFamily: 'product',
          }}>
          {address}
        </Text>
        <View
          style={{
            marginTop: responsiveScreenHeight(2),
            flexDirection: 'row',
            justifyContent: 'center',
            width: '100%',
          }}>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`tel:${phone}`);
            }}>
            <Call />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`sms:${phone}`);
            }}>
            <Sms style={{marginLeft: 10}} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`whatsapp://send?text=hello&phone=${phone}`);
            }}>
            <Whatsapp style={{marginLeft: 10}} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`mailto:${email}`);
            }}>
            <Mail style={{marginLeft: 10}} />
          </TouchableOpacity>
        </View>
      </View>
      <Row
        style={{
          position: 'relative',
          marginBottom: responsiveScreenHeight(6),
        }}>
        <Col style={{height: responsiveScreenHeight(10)}}>
          <TouchableOpacity
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            hitSlop={{top: 8, left: 8, right: 8, bottom: 8}}
            onPress={() => {
              viewpagerRef &&
                viewpagerRef.current &&
                viewpagerRef.current.setPage(0);
            }}>
            <Text
              style={
                activetab == 0
                  ? {
                      height: responsiveScreenHeight(5),
                      alignSelf: 'center',

                      fontFamily: 'product',
                      color: '#3A4041',
                    }
                  : {
                      height: responsiveScreenHeight(5),
                      alignSelf: 'center',

                      fontFamily: 'product',
                      color: '#9CA8AA',
                    }
              }>
              Data Information
            </Text>
          </TouchableOpacity>
        </Col>
        <Col style={{height: responsiveScreenHeight(10)}}>
          <TouchableOpacity
            hitSlop={{top: 8, left: 8, right: 8, bottom: 8}}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => {
              viewpagerRef &&
                viewpagerRef.current &&
                viewpagerRef.current.setPage(1);
            }}>
            <Text
              style={
                activetab == 1
                  ? {
                      height: responsiveScreenHeight(5),
                      alignSelf: 'center',

                      fontFamily: 'product',
                      color: '#3A4041',
                    }
                  : {
                      height: responsiveScreenHeight(5),
                      alignSelf: 'center',

                      fontFamily: 'product',
                      color: '#9CA8AA',
                    }
              }>
              Attendence
            </Text>
          </TouchableOpacity>
        </Col>
      </Row>
      <ViewPager
        ref={viewpagerRef}
        onPageSelected={e => {
          setActiveTab(e.nativeEvent.position);
        }}
        style={{height: responsiveScreenHeight(58), width: '100%'}}
        initialPage={0}>
        <View key="1">
          <MemberInformation id={id} />
        </View>

        <View key="2">
          <MemberEvent id={id} />
        </View>
      </ViewPager>
      {/* <Tabs
        tabBarUnderlineStyle={{backgroundColor: 'transprent', height: 0}}
        tabContainerStyle={{height: 40, marginTop: 10}}>
        <Tab
          activeTabStyle={{backgroundColor: COLOR.DARKBLUE, height: 40}}
          tabStyle={{backgroundColor: COLOR.DARKBLUE, height: 40}}
          heading={'Information'}>
          <MemberInformation id={id} />
        </Tab>
        <Tab
          activeTabStyle={{backgroundColor: COLOR.DARKBLUE, height: 40}}
          tabStyle={{backgroundColor: COLOR.DARKBLUE, height: 40}}
          heading={'Attendence'}>
          <MemberEvent id={id} />
        </Tab>
      </Tabs> */}
    </Heading>
  );
};
export default MemberProfile;
{
  /* <Header
      leftIcon={'arrow-back'}
      leftClick={() => {
        _goback();
      }}
      title={'Member Profile'}
      refreshControl={
        <RefreshControl
          colors={['#9Bd35A', '#689F38']}
          refreshing={loading}
          onRefresh={() => _refresh()}
        />
      }> */
}

{
  /* </Header> */
}
{
  /* <ImageView
onClose={() => setshowImage(false)}
images={images}
imageIndex={0}
isVisible={showImage}
renderFooter={currentImage => (
  <View>
    <Text style={{color: 'white', fontFamily: 'sf'}}>{fullname}</Text>
  </View>
)}
/>

<Content
contentContainerStyle={{
  justifyContent: 'center',
  alignItems: 'center',
}}>
<TouchableOpacity onPress={() => setshowImage(true)}>
  <Thumbnail
    style={{marginBottom: 20, marginTop: 10}}
    large
    source={{
      uri: photo,
    }}
  />
</TouchableOpacity>
<Text style={{color: '#4E4A5E', fontSize: 20, fontFamily: 'sf'}}>
  {fullname}
</Text>
<Text style={{color: '#949494', marginBottom: 20, fontFamily: 'sf'}}>
  {address}
</Text>
</Content>
<Tabs
tabBarUnderlineStyle={{backgroundColor: 'transprent', height: 0}}
tabContainerStyle={{height: 40}}>
<Tab
  activeTabStyle={{backgroundColor: COLOR.DARKBLUE, height: 40}}
  tabStyle={{backgroundColor: COLOR.DARKBLUE, height: 40}}
  heading={'Information'}>
  <MemberInformation id={id} />
</Tab>
<Tab
  activeTabStyle={{backgroundColor: COLOR.DARKBLUE, height: 40}}
  tabStyle={{backgroundColor: COLOR.DARKBLUE, height: 40}}
  heading={'Attendence'}>
  <MemberEvent id={id} />
</Tab>
</Tabs> */
}
