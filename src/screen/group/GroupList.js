import React, {useEffect, useState} from 'react';
import {FlatList, TouchableOpacity, View, RefreshControl} from 'react-native';
import {Text, ListItem, Left, Body, Thumbnail} from 'native-base';
import Header from '../../component/Header';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import {ROUTES} from '../../navigation/routes.constant';
import Axios from 'axios';
import {SERVER_URI, LocalStorage, COLOR, getRandomColor} from '../../common';
import TextAvatar from 'react-native-text-thumbnail';
import {Capitalize} from '../../common/capatalize';
import {_logout} from '../../component/logout';
import Heading from '../../component/Heading';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import TextAvatarComp from '../../component/textAvatarComponent';

const GroupList = props => {
  const navigation = useNavigation();
  const [grouplist, setGrouplist] = useState();
  const [loading, setLoading] = useState(true);
  const [grpPermission, setgrpPermission] = useState(false);
  const _groupfetch = async () => {
    const perm = await LocalStorage.getPermission();
    setgrpPermission(perm);
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/groups`,
      {
        user_id: await LocalStorage.getUserID(),
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        if (res.data.message == 'Invalid token or userid') {
          _logout(navigation);
        } else {
          setLoading(false);
          setGrouplist(res.data.data);
        }
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    if (loading) {
      _groupfetch();
    }
  });

  const _view = async (navigation, groupid, name) => {
    Axios.post(
      `${SERVER_URI}/subgroups`,
      {
        user_id: await LocalStorage.getUserID(),
        group_id: groupid,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        if (res.data.data.length > 0) {
          navigation.navigate(ROUTES.SUBGROUPLIST, {groupid});
        } else {
          navigation.navigate(ROUTES.GROUPPROFILE, {groupid, name});
        }
      })
      .catch(err => {
        console.log(err);
      });
      
  };

  useFocusEffect(
    React.useCallback(() => {
      _groupfetch();
    }, []),
  );

  return (
    <Heading
      text={'Groups'}
      lefticon={'ios-menu'}
      Iconmap={[
        {
          name: 'search',
          onPress: () => {
            navigation.navigate(ROUTES.GROUPSEARCH);
          },
        },
        // {
        //   name: 'bell',
        //   onPress: () => {
        //     alert('bellpress');
        //   },
        // },
      ]}
      onMenuPress={() => {
        navigation.openDrawer();
      }}>
      <FlatList
        refreshControl={
          <RefreshControl
            colors={['#9Bd35A', '#689F38']}
            refreshing={loading}
            onRefresh={() => setLoading(true)}
          />
        }
        data={grouplist}
        ListEmptyComponent={
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontFamily: 'product'}}>No Result Found</Text>
          </View>
        }
        renderItem={({item}) => (
          <TextAvatarComp
            onPress={() => {
              _view(navigation, item.id, item.name);
            }}
            name={Capitalize(item.name)}
            textavarname={item.name
              .split(' ')
              .slice(0, 2)
              .join('+')
              .toUpperCase()}
          />
        )}
        keyExtractor={item => item.id}
      />
    </Heading>
  );
};
export default GroupList;

// grpPermission == "false" ? (
//   <Header
//     leftIcon={'menu'}
//     leftClick={() => {
//       navigation.openDrawer();
//     }}
//     // title={`Groups (${grouplist.length})`}
//     title={'Groups List'}
//     refreshControl={
//       <RefreshControl
//         colors={['#9Bd35A', '#689F38']}
//         refreshing={loading}
//         onRefresh={() => setLoading(true)}
//       />
//     }>
//     <View style={{justifyContent: 'center', alignItems: 'center'}}>
//       <Text>User dont have access of group</Text>
//     </View>
//   </Header>
// ) :
{
  /* <Header
leftIcon={'menu'}
leftClick={() => {
  navigation.openDrawer();
}}
// title={`Groups (${grouplist.length})`}
title={'Groups'}
rightSecond={'search'}
rightSecondclick={() => {
  navigation.navigate(ROUTES.GROUPSEARCH);
}}
refreshControl={
  <RefreshControl
    colors={['#9Bd35A', '#689F38']}
    refreshing={loading}
    onRefresh={() => setLoading(true)}
  />
}> */
}

{
  /* <TouchableOpacity
onPress={() => {
  _view(navigation, item.id);
}}
style={{
  height: 82,
  width: '100%',
  alignItems: 'center',
  flexDirection: 'row',
  backgroundColor: 'white',
  marginTop: 3,
}}>
<View style={{marginLeft: 32}}>
  <TextAvatar
    backgroundColor={'#F1F1F1'}
    textColor={COLOR.DARKBLUE}
    size={60}
    type={'circle'}>
    {Capitalize(item.name)}
  </TextAvatar>
</View>
<View style={{marginLeft: 25}}>
  <Text style={{fontSize: 18, color: '#000000', fontFamily: 'sf'}}>
    {Capitalize(item.name)}
  </Text>
</View>
</TouchableOpacity> */
}
