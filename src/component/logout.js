import {LocalStorage, SERVER_URI} from '../common';
import {StackActions, CommonActions} from '@react-navigation/native';
import {ROUTES} from '../navigation/routes.constant';

const reset = (navigation) => {
  // return navigation.dispatch(StackActions.pop(2));
  return navigation.dispatch(
    CommonActions.navigate({
      name: ROUTES.AUTHSTACK,
    }),
  );
};

export const _logout = async (navigation) => {
  await LocalStorage.clearCache();
  await LocalStorage.clearToken();
  await LocalStorage.clearUserID();
  await LocalStorage.clearPermission();

  await reset(navigation);
};
