import React from 'react';
import {Item, Input, Button} from 'native-base';
import {
  View,
  Text,
  ScrollView,
  Animated,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import AntIcon from 'react-native-vector-icons/AntDesign';
import FonAwsmIcon from 'react-native-vector-icons/FontAwesome';
import {SERVER_URI, LocalStorage} from '../../common';
import {ROUTES} from '../../navigation/routes.constant';
import Axios from 'axios';
import styles, {IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL} from './style';
import DropdownAlert from 'react-native-dropdownalert';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showpass: true,
      // email: 'adminthree@demo.com',
      // password: 'admin123',
      email: '',
      password: '',
      loading: false,
    };
    this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
  }

  componentWillMount() {
    if (Platform.OS == 'ios') {
      this.keyboardWillShowSub = Keyboard.addListener(
        'keyboardWillShow',
        this.keyboardWillShow,
      );
      this.keyboardWillHideSub = Keyboard.addListener(
        'keyboardWillHide',
        this.keyboardWillHide,
      );
    } else {
      this.keyboardWillShowSub = Keyboard.addListener(
        'keyboardDidShow',
        this.keyboardDidShow,
      );
      this.keyboardWillHideSub = Keyboard.addListener(
        'keyboardDidHide',
        this.keyboardDidHide,
      );
    }
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  keyboardWillShow = event => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: IMAGE_HEIGHT_SMALL,
    }).start();
  };

  keyboardWillHide = event => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: IMAGE_HEIGHT,
    }).start();
  };

  keyboardDidShow = event => {
    Animated.timing(this.imageHeight, {
      toValue: IMAGE_HEIGHT_SMALL,
    }).start();
  };

  keyboardDidHide = event => {
    Animated.timing(this.imageHeight, {
      toValue: IMAGE_HEIGHT,
    }).start();
  };

  _login = navigation => {
    const {email, password} = this.state;

    this.setState({
      loading: true,
    });
    Axios.post(`${SERVER_URI}/login`, {
      email: email,
      password: password,
    })
      .then(async response => {
        if (response.data.status == 'fail') {
          this.setState({
            loading: false,
          });
          this.dropDownAlertRef.alertWithType(
            'error',
            'Error',
            'Email or password is wrong OR your account is disabled by admin ',
          );
        } else {
          this.setState({
            loading: false,
          });
          // let res = response.data.data.newpermissions
          // console.log('login data',res ,'typeof ',typeof(res.events.create) )
          const permission =
            (await response.data.data.newpermissions.follow_ups) == undefined
              ? false
              : true;

          await LocalStorage.storeToken(response.data.data.token);
          await LocalStorage.storeUserId(response.data.data.id.toString());
          await LocalStorage.storePermission(permission.toString());
          // await LocalStorage.Store_Create_Event_Permission(res.events.create)
           console.log('permissions',response.data.data)

          await navigation.navigate(ROUTES.DRAWERSTACK);
          // console.log('login response',response.data.data)
        }
      })
      .catch(error => {
        this.setState({
          loading: false,
        });
        console.log(error);
      });
  };
  render() {
    const {showpass, loading, email, password} = this.state;
    return (
      <View
        style={{
          height: responsiveHeight(100),
          width: '100%',
          backgroundColor: 'white',
          alignItems: 'center',
        }}>
        <DropdownAlert ref={ref => (this.dropDownAlertRef = ref)} />
        <View style={{marginTop: responsiveHeight(10)}}>
          <Animated.Image
            source={require('../../../assets/1.png')}
            style={[styles.logo, {height: this.imageHeight}]}
          />
        </View>

        <ScrollView style={{marginTop: responsiveHeight(10)}}>
          <KeyboardAvoidingView behavior="padding">
            <View style={{width: responsiveWidth(90), alignItems: 'center'}}>
              <Item
                regular
                style={{
                  marginBottom: 6,
                  padding: 5,
                  borderRadius: 5,
                  borderWidth: 1,
                }}>
                
                <Input
                  value={email}
                  onChangeText={text => {
                    this.setState({
                      email: text,
                    });
                  }}
                  style={{fontFamily: 'product', color: '#b0afb0'}}
                  placeholder="Email Address"
                  placeholderTextColor={'#b0afb0'}
                />
              </Item>
              <Item
                regular
                style={{
                  marginBottom: 6,
                  padding: 5,
                  borderRadius: 5,
                  borderWidth: 1,
                }}>
                
                <Input
                  style={{fontFamily: 'product', color: '#b0afb0'}}
                  value={password}
                  onChangeText={text => {
                    this.setState({
                      password: text,
                    });
                  }}
                  secureTextEntry={showpass}
                  placeholder="Password"
                  placeholderTextColor={'#b0afb0'}
                />
                {!showpass ? (
                  <FonAwsmIcon
                    style={{marginRight: 10}}
                    onPress={() => {
                      this.setState({
                        showpass: !showpass,
                      });
                    }}
                    active
                    name="eye-slash"
                    size={20}
                    color="#b0afb0"
                  />
                ) : (
                  <FonAwsmIcon
                    style={{marginRight: 10}}
                    onPress={() => {
                      this.setState({
                        showpass: !showpass,
                      });
                    }}
                    active
                    name="eye"
                    size={20}
                    color="#b0afb0"
                  />
                )}
              </Item>

              <Button
                style={{
                  width: 136,
                  marginTop: responsiveHeight(5),
                  borderRadius: 8,
                  height: 50,
                  backgroundColor: '#0178f2',
                  justifyContent: 'center',
                }}
                disabled={loading ? true : false}
                onPress={() => {
                  this._login(this.props.navigation);
                }}>
                {loading ? (
                  <ActivityIndicator size="large" color={'white'} />
                ) : (
                  <Text
                    uppercase={false}
                    style={{
                      fontFamily: 'product',
                      color: 'white',
                      alignSelf: 'center',
                      fontSize: responsiveFontSize(1.5),
                    }}>
                    Login
                  </Text>
                )}
              </Button>

              <View
                style={{
                  flexDirection: 'row',
                  marginTop: responsiveHeight(5),
                }}>
                <Text
                  style={{
                    fontFamily: 'product',
                    color: 'black',
                    alignSelf: 'center',
                    fontSize: responsiveFontSize(1.5),
                  }}>
                  .
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    Linking.openURL('https://www.sheepmug.com/registeryourchurch');
                  }}>
                  <Text
                    style={{
                      fontFamily: 'product',
                      color: '#00479D',
                      alignSelf: 'center',
                      fontSize: responsiveFontSize(1.5),
                      marginLeft: 10,
                    }}>
                    Register Your Church
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  }
}
export default Login;
