import React, {useState, useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import {View, TouchableOpacity, Dimensions} from 'react-native';
import {ROUTES} from './routes.constant';
import Login from '../screen/auth/Login';
import {
  MemberFilter,
  MemberList,
  MemberProfile,
  MemberSearch,
} from '../screen/member';
import {
  GroupList,
  GroupMember,
  GroupSearch,
  SubGroupList,
} from '../screen/group';
import {EventAttendence, EventList, EventProfile} from '../screen/event';
import {COLOR, SERVER_URI, LocalStorage} from '../common';
import {Thumbnail, Text, Content, Button, Footer, FooterTab} from 'native-base';
import SplashScreen from '../screen/SplashScreen';
import BlogList from '../screen/blog/BlogList';
import Notification from '../screen/notification/index';
import Settings from '../screen/settings';
import Profile from '../screen/profile';
import FolderList from '../screen/files/fileFolder';
import FileList from '../screen/files/fileList';
import EventSearch from '../screen/event/EventSearch';
import MemberFilterResult from '../screen/member/MemberFilterResult';
import Icon from 'react-native-vector-icons/FontAwesome';
import OctiIcon from 'react-native-vector-icons/Octicons';
import FeaterIcon from 'react-native-vector-icons/Feather';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import CreateEvent from '../screen/CreateEvent';
import EditEvent from '../screen/EditEvent';


import {
  useFocusEffect,
  useNavigation,
  StackActions,
} from '@react-navigation/native';
import Axios from 'axios';
import Intro from '../screen/intro';
import Dashboard from '../screen/dashboard';
import GroupProfile from '../screen/group/GroupProfile';
import BookMarks from '../screen/bookmark';
import FolderSearch from '../screen/files/folderSearch';
import FileSearch from '../screen/files/fileSearch';
const CustomDrawerContent = props => {
  const navigation = useNavigation();
  const [member, setMember] = useState();
  const [loading, setLoading] = useState(true);

  const [imgload, setImgload] = useState(true);
  const _memberFetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/getusernamedetail`,
      {
        user_id: await LocalStorage.getUserID(),
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);
        setMember(res.data.data);
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    if (loading) {
      _memberFetch();
    }
  });

  const reset = route => {
    return navigation.dispatch(StackActions.pop(2));
  };

  const _logout = async navigation => {
    await LocalStorage.clearCache();
    await LocalStorage.clearToken();
    await LocalStorage.clearUserID();
    await LocalStorage.clearPermission();
    Axios.post(
      `${SERVER_URI}/logoutuser`,
      {
        user_id: await LocalStorage.getUserID(),
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });

    // await navigation.dispatch(StackActions.push(ROUTES.AUTHSTACK));
    await reset(ROUTES.AUTHSTACK);
    // navigation.dispatch(StackActions.push(ROUTES.AUTHSTACK));
    // await navigation.navigate(ROUTES.AUTHSTACK);
  };
  useFocusEffect(
    React.useCallback(() => {
      _memberFetch();
    }, []),
  );

  var str = member && member.profilepic;

  var res =
    member &&
    member.profilepic &&
    str.replace(
      'http://app.sheepmug.com/public/uploads/',
      'https://www.sheepmug.com/superadmin/public/uploads/user/',
    );
  const height = Dimensions.get('window').height;

  return (
    <>
      <View style={{alignItems: 'center', width: '100%'}}>
        <TouchableOpacity
          onPress={() => {
            // navigation.navigate(ROUTES.PROFILE);
          }}>
          <Thumbnail
            onLoadEnd={() => {
              setImgload(false);
            }}
            style={{marginBottom: 5, marginTop: height / 5}}
            large
            source={{
              uri: imgload
                ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                : member &&
                  member.profilepic == 'http://app.sheepmug.com/public/uploads/'
                ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                : `${res}`,
            }}
          />
        </TouchableOpacity>
      </View>
      <DrawerContentScrollView {...props}>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
      {/* <View style={{alignItems: 'center', width: '100%'}}>
        <View
          style={{
            margin: 30,
            borderRadius: 30,
            backgroundColor: COLOR.YELLOW,
            height: 50,
            width: 50,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <TouchableOpacity
            onPress={() => {
              _logout(navigation);
            }}>
            <Icon name="sign-out" color={'white'} size={30} />
          </TouchableOpacity>
        </View>
      </View> */}
    </>
  );
};

const AuthStack = createStackNavigator();
const AuthNavigator = () => {
  return (
    <AuthStack.Navigator
      initialRouteName={ROUTES.LOGIN}
      screenOptions={{
        headerShown: false,
      }}>
      <AuthStack.Screen name={ROUTES.LOGIN} component={Login} />
    </AuthStack.Navigator>
  );
};

const CreateEventStack = createStackNavigator();
// const CreateEventNavigator = () => {
//   return (
//     <CreateEventStack.Navigator
//       initialRouteName={ROUTES.CREATEEVENT}
//       screenOptions={{
//         headerShown: false,
//       }}>
//       <CreateEventStack.Screen name={ROUTES.CREATEEVENT} component={CreateEvent} />
//     </CreateEventStack.Navigator>
//   );
// };

const DashboardStack = createStackNavigator();
const DashboardNavigator = () => {
  return (
    <DashboardStack.Navigator
      initialRouteName={ROUTES.DASHBOARD}
      screenOptions={{
        headerShown: false,
      }}>
      <DashboardStack.Screen name={ROUTES.DASHBOARD} component={Dashboard} />
      <DashboardStack.Screen
        name={ROUTES.MEMBERPROFILE}
        component={MemberProfile}
      />
      {/* <DashboardStack.Screen name={ROUTES.PROFILE} component={Profile} /> */}
    </DashboardStack.Navigator>
  );
};
const MemberStack = createStackNavigator();
const MemberNavigator = () => {
  return (
    <MemberStack.Navigator
      initialRouteName={ROUTES.MEMBERLIST}
      screenOptions={{
        headerShown: false,
      }}>
      <MemberStack.Screen name={ROUTES.MEMBERLIST} component={MemberList} />
      <MemberStack.Screen
        name={ROUTES.MEMBERPROFILE}
        component={MemberProfile}
      />
      <MemberStack.Screen name={ROUTES.MEMBERFILTER} component={MemberFilter} />
      <MemberStack.Screen name={ROUTES.MEMBERSEARCH} component={MemberSearch} />
      <MemberStack.Screen
        name={ROUTES.MEMBERFILTERRESULT}
        component={MemberFilterResult}
      />
      {/* <MemberStack.Screen name={ROUTES.PROFILE} component={Profile} /> */}
    </MemberStack.Navigator>
  );
};

const GroupStack = createStackNavigator();
const GroupNavigator = () => {
  return (
    <GroupStack.Navigator
      initialRouteName={ROUTES.GROUPLIST}
      screenOptions={{
        headerShown: false,
      }}>
      <GroupStack.Screen name={ROUTES.GROUPLIST} component={GroupList} />
      <GroupStack.Screen name={ROUTES.SUBGROUPLIST} component={SubGroupList} />
      <GroupStack.Screen name={ROUTES.GROUPMEMBER} component={GroupMember} />
      <GroupStack.Screen name={ROUTES.GROUPSEARCH} component={GroupSearch} />
      <GroupStack.Screen name={ROUTES.GROUPPROFILE} component={GroupProfile} />
    </GroupStack.Navigator>
  );
};

const EventStack = createStackNavigator();
const EventNavigator = () => {
  return (
    <EventStack.Navigator
      initialRouteName={ROUTES.EVENTLIST}
      screenOptions={{
        headerShown: false,
      }}>
      <EventStack.Screen name={ROUTES.EVENTLIST} component={EventList} />
      <EventStack.Screen name={ROUTES.EVENTPROFILE} component={EventProfile} />
      <EventStack.Screen name={ROUTES.EDITEVENT} component={EditEvent} />
      <EventStack.Screen name={ROUTES.CREATEEVENT} component={CreateEvent} />
      <EventStack.Screen
        name={ROUTES.EVENTEATTENDENCE}
        component={EventAttendence}
      />
      <EventStack.Screen name={ROUTES.EVENTSEARCH} component={EventSearch} />
    </EventStack.Navigator>
  );
};

const FileStack = createStackNavigator();
const FileNavigator = () => {
  return (
    <FileStack.Navigator
      initialRouteName={ROUTES.FOLDERLIST}
      screenOptions={{
        headerShown: false,
      }}>
      <FileStack.Screen name={ROUTES.FOLDERLIST} component={FolderList} />
      <FileStack.Screen name={ROUTES.FILELIST} component={FileList} />
      <FileStack.Screen name={ROUTES.FOLDERSEARCH} component={FolderSearch} />
      <FileStack.Screen name={ROUTES.FILESEARCH} component={FileSearch} />
    </FileStack.Navigator>
  );
};

const BlogStack = createStackNavigator();
const BlogNavigator = () => {
  return (
    <BlogStack.Navigator
      initialRouteName={ROUTES.BLOGLIST}
      screenOptions={{
        headerShown: false,
      }}>
      <BlogStack.Screen name={ROUTES.BLOGLIST} component={BlogList} />
    </BlogStack.Navigator>
  );
};

const NotificationStack = createStackNavigator();
const NotificationNavigator = () => {
  return (
    <NotificationStack.Navigator
      initialRouteName={ROUTES.NOTIFICATION}
      screenOptions={{
        headerShown: false,
      }}>
      <NotificationStack.Screen
        name={ROUTES.NOTIFICATION}
        component={Notification}
      />
    </NotificationStack.Navigator>
  );
};

const SettingStack = createStackNavigator();
const SettingNavigator = () => {
  return (
    <SettingStack.Navigator
      initialRouteName={ROUTES.SETTING}
      screenOptions={{
        headerShown: false,
      }}>
      <SettingStack.Screen name={ROUTES.SETTING} component={Settings} />
    </SettingStack.Navigator>
  );
};

const BookMarkStack = createStackNavigator();
const BookMarkNavigator = () => {
  return (
    <BookMarkStack.Navigator
      initialRouteName={ROUTES.BOOKMARK}
      screenOptions={{
        headerShown: false,
      }}>
      <BookMarkStack.Screen name={ROUTES.BOOKMARK} component={BookMarks} />
    </BookMarkStack.Navigator>
  );
};

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      // drawerContent={props => {
      //   let copyprops = Object.assign({}, props);

      //   copyprops &&
      //     copyprops.state &&
      //     copyprops.state.routes.filter(item => item.name !== 'Profile');
      //   // console.log(copyprops, 'dacfjp');
      //   return <CustomDrawerContent {...copyprops} />;
      // }}
      drawerContent={props => <CustomDrawerContent {...props} />}
      initialRouteName={ROUTES.MEMBERSTACK}
      drawerContentOptions={{
        activeTintColor: '#0078F2',
        activeBackgroundColor: 'white',
        inactiveTintColor: '#3A4041',
        labelStyle: {fontSize: 15, fontFamily: 'product'},
        itemStyle: {
          width: '100%',
          marginBottom: -10,
          marginLeft: '26%',
          // marginTop: 5,
        },
      }}
      drawerStyle={{backgroundColor: 'white'}}>
      <Drawer.Screen
        name={'Dashboard'}
        component={DashboardNavigator}
        options={{
          drawerIcon: config => (
            <OctiIcon
              name="dashboard"
              color={config.color}
              style={{marginRight: -15}}
              size={20}
            />
          ),
        }}
      />
      <Drawer.Screen
        name={'Members'}
        component={MemberNavigator}
        options={{
          drawerIcon: config => (
            <FeaterIcon
              name="user"
              style={{marginRight: -15}}
              color={config.color}
              size={20}
            />
          ),
        }}
      />
      <Drawer.Screen
        name={'Bookmarks'}
        component={BookMarkNavigator}
        options={{
          drawerIcon: config => (
            <FontAwesomeIcon
              name={'bookmark-o'}
              style={{marginRight: -15}}
              color={config.color}
              size={20}
            />
          ),
        }}
      />
      <Drawer.Screen
        name={'Groups'}
        component={GroupNavigator}
        options={{
          drawerIcon: config => (
            <FeaterIcon
              name="users"
              style={{marginRight: -15}}
              color={config.color}
              size={20}
            />
          ),
        }}
      />
      <Drawer.Screen
        name={'Events'}
        component={EventNavigator}
        options={{
          drawerIcon: config => (
            <SimpleLineIcons
              name="event"
              style={{marginRight: -15}}
              color={config.color}
              size={20}
            />
          ),
        }}
      />
       {/* <Drawer.Screen
        name={'CreateEvent'}
        component={CreateEventNavigator}
        options={{
          drawerIcon: config => (
            <SimpleLineIcons
              name="event"
              style={{marginRight: -15}}
              color={config.color}
              size={20}
            />
          ),
        }}
      /> */}
      <Drawer.Screen
        name={'Blog'}
        component={BlogNavigator}
        options={{
          drawerIcon: config => (
            <FontAwesomeIcon
              name="comment-o"
              style={{marginRight: -15}}
              color={config.color}
              size={20}
            />
          ),
        }}
      />
      <Drawer.Screen
        name={'Reports'}
        component={FileNavigator}
        options={{
          drawerIcon: config => (
            <FeaterIcon
              name="file"
              style={{marginRight: -15}}
              color={config.color}
              size={20}
            />
          ),
        }}
      />
      <Drawer.Screen
        name={'Notification'}
        component={NotificationNavigator}
        options={{
          drawerIcon: config => (
            <FontAwesomeIcon
              name="bell-o"
              style={{marginRight: -15}}
              color={config.color}
              size={20}
            />
          ),
        }}
      />
      <Drawer.Screen
        name={'Settings'}
        component={SettingNavigator}
        options={{
          drawerIcon: config => (
            <FeaterIcon
              name="settings"
              style={{marginRight: -15}}
              color={config.color}
              size={20}
            />
          ),
        }}
      />
    </Drawer.Navigator>
  );
};

const AppStack = createStackNavigator();
const Appnavigator = () => {
  return (
    <AppStack.Navigator initialRouteName={ROUTES.SPLASH}>
      <AppStack.Screen
        name={ROUTES.SPLASH}
        component={SplashScreen}
        options={{headerShown: false}}
      />
      <AppStack.Screen
        name={ROUTES.INTRO}
        component={Intro}
        options={{headerShown: false}}
      />

      <AppStack.Screen
        name={ROUTES.AUTHSTACK}
        component={AuthNavigator}
        options={{headerShown: false}}
      />
      <AppStack.Screen
        name={ROUTES.DRAWERSTACK}
        component={DrawerNavigator}
        options={{headerShown: false}}
      />

      <AppStack.Screen
        name={ROUTES.DASHEVENTPROFILE}
        component={EventProfile}
        options={{headerShown: false}}
      />
      <AppStack.Screen
        name={ROUTES.DASHEVENT}
        component={EventList}
        options={{headerShown: false}}
      />
      <AppStack.Screen
        name={ROUTES.DASHMEMBERPROFILE}
        component={MemberProfile}
        options={{headerShown: false}}
      />
      <AppStack.Screen
        name={ROUTES.DASHMEMBER}
        component={MemberList}
        options={{headerShown: false}}
      />
    </AppStack.Navigator>
  );
};

export default Appnavigator;
