import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native';
import styles from './styles'
import Heading from '../../component/Heading'
import { Divider } from 'react-native-elements';
import { Dropdown } from 'react-native-material-dropdown';
import DateTimePicker from 'react-native-modal-datetime-picker';
import ImagePicker from 'react-native-image-picker';
import { PermissionsAndroid } from 'react-native'
import { SERVER_URI, LocalStorage } from '../../common';
import Axios from 'axios';
import moment from 'moment';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import Modal from 'react-native-modal';
import { Icon, Thumbnail } from 'native-base';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { Button, Overlay } from 'react-native-elements';
import DropdownAlert from 'react-native-dropdownalert';
let groupListCopy = []


class EditEvent extends Component {
    constructor(props) {
        super(props)
        //  console.log('props ', this.props.route.params.item.event_calendar_id)
        this.props.route.params.eventList.selected = true
        //  console.log('props are here ', this.props.route.params.eventList.selected_group)
        this.state = {
            eventName: this.props.route.params.item.name,
            groupName: '',
            index: '',
            isTimPickeVisible: false,
            isDatePickeVisible: false,
            eventTime: '',
            eventDate: '',
            e_date: '',
            avatarSource: '',
            notes: this.props.route.params.item.notes,
            eventType: '',
            eventId: '',
            tag: '',
            loading: false,
            eventTypeList: [],
            groupList: [],
            _selectedgroups: [],
            // groupSelected: [],
            source: '',
            isModalVisible: false,
             uploaded: '',
             permission:'',

        }
      
    }
    async componentDidMount() {


        let token = await LocalStorage.getToken()
        let uid = await LocalStorage.getUserID()
           
        this.getEventList(token, uid)
        this.getGroups(token, uid)
        this.getPermissions(token, uid)


        let exDate = this.props.route.params.item.start_date
        let event_list = this.props.route.params.eventList
        let _eventData = this.props.route.params.eventdata
        // console.log('event data',event_list[0].start_time)

        // pre selected image
        let imgUri = event_list[0].featured_image ==
        'http://app.sheepmug.com/public/uploads'
        ? 'https://via.placeholder.com/300.png'
        : event_list[0].featured_image

        // for pre selected date
        let d = moment(exDate).format("MMM Do YYYY")
        let [m, dd, y] = d.split(' ');
        let ddd = `${dd} ${m} ${y}`

        // for pre selected time 
        let ps_time =event_list[0].start_time 
        let time = this.tConvert(ps_time)
        var timeString = time;
        var H = +timeString.substr(0, 2);
        var h = H % 12 || 12;
        var ampm = (H < 12 || H === 24) ? "AM" : "PM";
        timeString = h + ' ' + timeString.substr(2, 3) + ' ' + ampm;

        this.setState({
            eventDate: ddd,
            e_date: exDate,
            uploaded:imgUri,
            eventTime: timeString
        })

     


        // console.log('token is ',token, 'user id', uid)
        // var today = new Date();
        // var dd = String(today.getDate()).padStart(2, '0');
        // var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        // var yyyy = today.getFullYear();

        // today = dd + '/' + mm + '/' + yyyy;
        // console.warn(today)
    }

    getPermissions=(token,uid)=>{
        const scope = this
        let user_id = uid
        
        Axios({
           method: 'get',
           url: `${SERVER_URI}/get_user_permission_list/${user_id}`,
           headers: {
               'X-API-TOKEN': token,
           },
       })
           .then(function (response) {
              console.log('success response :', response.data.data.eventsupdate);
               scope.setState({permission:response.data.data})
           })
           .catch(function (response) {
               //handle error
               console.log('error response :', response);
           });
   
       }

    getEventList = (token, uid) => {
        const scope = this
        Axios({
            method: 'post',
            url: `${SERVER_URI}/eventcreatepage`,
            data: {
                user_id: 74
            },
            headers: {
                'X-API-TOKEN': token,
            },
        })
            .then(function (response) {
                //   console.log('success response :', response.data.data, 'uid', uid);
                let list = []
                response.data.data.calendars.map((res) => {
                    list.push({ ...res, value: res.name, id: res.id })
                })
                scope.setState({
                    eventTypeList: list
                },()=>scope.preselectedEvent(scope.state.eventTypeList))

            })
            .catch(function (response) {
                //handle error
                console.log('error response :', response);
            });
    }
   



    getGroups = async (token, uid) => {
        const scope = this
        Axios.post(
            `${SERVER_URI}/groups`,
            {
                user_id: uid,
            },
            {
                headers: {
                    'X-API-TOKEN': token,
                },
            },
        )
            .then(async res => {
                if (res.data.status == 'success') {
                    //    console.log('response success',res.data.data)
                    let groups = []
                    res.data.data.map((res) => {
                        groups.push({ ...res, value: res.name })
                    })
                    groupListCopy = JSON.parse(JSON.stringify(groups));
                    scope.setState({ groupList: groups },()=>this.preSelctedGroup(this.state.groupList))
                } else {
                    console.log('something went wrong', res)
                }
            })
            .catch(err => {
                setLoading(false);
                console.log('response error', err);
            });
    };

    openModal = () => {
        this.setState({
            isModalVisible: true
        })
    }
    _SelectGroup = (item, index) => {
        let selectedItem = this.state.groupList.slice();
        selectedItem.map((res) => {
            if (res.id === item.id && item.selected) {
                selectedItem[index].selected = false
                // console.log('first', 'res', res.id, 'item', item.id)
            }
            else if (res.id === item.id && !item.selected) {
                selectedItem[index].selected = true
                // console.log('second', 'res', res.id, 'item', item.id)

            }

        })
        this.setState({ groupList: selectedItem });
    }
    // this.props.route.params.eventList.selected_group
preSelctedGroup =(groups)=>{
  let eventGroup= this.props.route.params.eventList.selected_group
  let mygroup = []
  eventGroup.map((res)=>{
      groups.map((result,index)=>{
          if(res.id===result.id){
           groups[index].selected = true
           mygroup.push(res)
          }
      })
  })
  this.setState({groupList:groups,_selectedgroups:mygroup})
//   console.log('presele',mygroup)
}
preselectedEvent=(events)=>{
   let event_Id =  this.props.route.params.item.event_calendar_id
   events.map((res)=>{
       if(res.id===event_Id){
           console.log('res',res)
        this.setState({
            eventType: res.value,
            event_Calendar_Id: res.id})
       }
   })
}

    _renderGroupsContent = ({ item, index }) => {
        const { selected } = this.state
        return (
            <View style={{
                backgroundColor: 'white',

            }} >
                <TouchableOpacity
                    onPress={() => this._SelectGroup(item, index)}
                >
                    <View style={styles.content}>
                        <View style={{ flex: 1, flexDirection: 'row', }}>
                            {item.selected ?
                                <Icon
                                    name={'check-square'}
                                    type={'Feather'}
                                    style={{ fontSize: 20 }}
                                /> :
                                <Icon
                                    name={'square'}
                                    type={'Feather'}
                                    style={{ fontSize: 20 }}
                                />}
                            <Text style={styles.itemName}>{item.name}</Text>
                        </View>

                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    EventType = (value, index, data) => {
          console.log('value', value, 'index', index, 'id',data[index].id)
        this.setState({
            eventType: value,
            event_Calendar_Id: data[index].id,
        })
    }

    showDatePicker = () => {
        this.setState({
            isDatePickeVisible: true
        })
    }
    hideDatePicker = () => {
        this.setState({
            isDatePickeVisible: false
        })
    }
    handleDatePicked = (date) => {
        //  console.log('zaheer',date,'aaa',moment(date).format("MMM Do YYYY"))
        //  let d = moment(date).format("MMM Do YYYY")
        let d = moment(date.dateString).format("MMM Do YYYY")
        let [m, dd, y] = d.split(' ');
        let ddd = `${dd} ${m} ${y}`
        this.setState({
            eventDate: ddd,
            e_date: date.dateString,
        })
        this.hideDatePicker()
    }


    showTimePicker = () => {
        this.setState({
            isTimPickeVisible: true
        })
    };

    tConvert(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }

    hideTimePicker = () => {
        this.setState({
            isTimPickeVisible: false
        })
    };

    handleTimePicked = (date) => {
        // console.log('time',date,'type of ',typeof(date.toString()) )
        let d = date.toString()
        let [dd, PKT] = d.split('G')
        let ddd = dd.slice(-9)
         let e_time = ddd
        let time = this.tConvert(ddd)
        var timeString = time;
        var H = +timeString.substr(0, 2);
        var h = H % 12 || 12;
        var ampm = (H < 12 || H === 24) ? "AM" : "PM";
        timeString = h + ' ' + timeString.substr(2, 3) + ' ' + ampm;

        console.log('time',ddd,'type of',typeof(ddd))
        this.setState({
            eventTime: timeString,
            myEvent_time : e_time
        })
        // console.warn("A date has been picked: ", timeString);

        this.hideTimePicker();
    };
    selectImg = async () => {
        const options = {
            title: 'Upload Event Banner',
            // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            // console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log(
                    'User tapped custom button: ',
                    response.customButton
                );
                alert(response.customButton);
            } else {
                let source = { uri: response.uri };
                this.setState({
                    avatarSource: response.uri,
                    avatar: response,
                    source: source
                })

            }
        });
    }
    createEvent = async () => {
        if(this.state.permission.eventsupdate){

        let token = await LocalStorage.getToken()
        let uid = await LocalStorage.getUserID()
        let eventId = this.props.route.params.item.id
        
        const scope = this
        const { eventName, eventDate, e_date, notes, avatar,uploaded, tag, myEvent_time, eventTime, event_Calendar_Id, _selectedgroups } = this.state

        if (eventName.trim().length > 0) {
            if (_selectedgroups.length > 0) {
                if (myEvent_time.trim().length > 0) {
                    if (event_Calendar_Id != null) {
                        if (avatar|| uploaded) {
                            if (notes.trim().length > 0) {
                                this.setState({ loading: true })
                                let body = new FormData()
                                body.append('user_id', 84)
                                body.append('event_calendar_id', event_Calendar_Id)
                                body.append('name', eventName)
                                body.append('start_date', e_date)
                                body.append('notes', notes)
                                body.append('all_day', 0)
                                {
                                avatar ?
                                body.append('featured_image', { uri: avatar.uri , type: avatar.type, name:  avatar.fileName , data:avatar.data })
                                :null
                               }
                                body.append('tags', Object.keys(tag).map(function (k) { return tag[k] }).join(","))
                                body.append('start_time',myEvent_time)
                                body.append('end_time',myEvent_time)


                                Axios({
                                    method: 'post',
                                    url: `${SERVER_URI}/eventupdate/${eventId}`,
                                    data: body,
                                    headers: {
                                        'X-API-TOKEN': token,
                                    },

                                })
                                    .then(function (response) {
                                        console.log('success response :', response.data.message);
                                        scope.setState({
                                            loading: false,
                                            notes: '',
                                            eventTime: "",
                                            tag: '',
                                            eventName: '',
                                            event_Calendar_Id: '',
                                        },()=>scope.props.navigation.pop())
                                    })
                                    .catch(function (response) {
                                        //handle error
                                        console.log('error response :', response);
                                    });
                            } else {
                                alert('plase type event notes and outline')
                            }
                        } else {
                            alert('please upload event Banner')
                        }
                    } else {
                        alert('please choose event type')
                    }
                } else {
                    alert('please select time ')
                }
            } else {
                alert('please select event Group')
            }
        } else {
            alert('please type event name')
        }
    } else{
        this.dropDownAlertRef.alertWithType(
            'error',
            'Error',
            'You are not allowed to edit Event pleae contact with Adminstration ',
          );
    }
}

    CancelSelectedGroup = () => {
        let newArray = JSON.parse(JSON.stringify(groupListCopy))
        this.setState({ isModalVisible: false, groupList: newArray },()=>this.preSelctedGroup(this.state.groupList))
    }
    doneSelectedGroup = () => {
        alert('calll')
        const { groupList, _selectedgroups } = this.state
        let newArray = JSON.parse(JSON.stringify(groupListCopy))
        let groupTag = {}
        const scope = this
        let mygroup = []
        groupList.map((res, i) => {
            if (res.selected) {
                mygroup.push(res)
                groupTag[i] = res.id
            }
        })
        this.setState({ _selectedgroups: mygroup, isModalVisible: false, tag: groupTag })
    }

    renderGroup = ({ item }) => {
        // console.log('item', item.name)
        return (
            <View style={{ marginRight: 5 }}>
                <Text>{`${item.name} `}</Text>
            </View>
        )
    }



    render() {
        const { eventName, isTimPickeVisible, eventTime, avatarSource, eventDate, isDatePickeVisible, loading, _selectedgroups, eventTypeList ,eventType } = this.state
        //  console.log('in reder ',typeof(eventTime))
        return (
            <Heading
                text={'Edit Event'}
                lefticon={'ios-arrow-round-back'}
                Iconmap={
                    [
                        // {
                        //   name: 'bell',
                        //   onPress: () => {
                        //     alert('bellpress');
                        //   },
                        // },
                    ]
                }
                onMenuPress={() => {
                    // this.props.navigation.navigate('EVENTPROFILE')
                    this.props.navigation.pop()
                    this.props.navigation.pop()
                    // this.props.navigation.pop()
                    // this.props.navigation.openDrawer();
                }}>
                <ScrollView>
                    <View style={styles.contentView}>
                        <Text style={styles.heading}>Event Name</Text>
                        <TextInput
                            style={styles.inputField}
                            // placeholder={'Midweek service'}
                            placeholder={'Type meeting Name'}
                            placeholderTextColor={'black'}
                            value={eventName}
                            onChangeText={(value) => this.setState({ eventName: value })}
                        />
                    </View>
                    <Divider style={{ backgroundColor: 'grey' }} />
                    <View style={[styles.contentView, { marginTop: 15 }]}>
                        <Text style={styles.heading}>Assign Group</Text>

                        <TouchableOpacity
                            style={styles.selectGroup}
                            onPress={() => this.openModal()}>
                            {_selectedgroups.length > 0 ?

                                <FlatList
                                    data={_selectedgroups}
                                    horizontal={true}
                                    keyExtractor={(item) => { item.id }}
                                    renderItem={this.renderGroup}
                                />

                                :

                                <Text>Select Group</Text>
                            }
                        </TouchableOpacity>

                        <Modal
                            testID={'modal'}
                            isVisible={this.state.isModalVisible}
                            animationIn="zoomInDown"
                            animationOut="zoomOutUp"
                            animationInTiming={600}
                            animationOutTiming={600}
                            backdropTransitionInTiming={600}
                            backdropTransitionOutTiming={600}>
                            <View style={{ backgroundColor: 'white', maxHeight: 300, paddingTop: 20 }}>
                                <FlatList
                                    style={{ marginBottom: 50 }}
                                    data={this.state.groupList}
                                    keyExtractor={(item) => { item.id }}
                                    renderItem={({ item, index }) => this._renderGroupsContent({ item, index })}
                                />
                                <View style={{
                                    flexDirection: 'row',
                                    position: 'absolute',
                                    bottom: 3,
                                    alignSelf: 'center'
                                }}>

                                    <TouchableOpacity
                                        style={styles.cancelBtn}
                                        onPress={() => this.CancelSelectedGroup()}>
                                        <Text style={{ textAlign: 'center', color: 'red' }}>Cancel</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        style={styles.doneBtn}
                                        onPress={() => this.doneSelectedGroup()}
                                    >
                                        <Text style={{ textAlign: 'center', }}>Done</Text>
                                    </TouchableOpacity>

                                </View>
                            </View>

                        </Modal>


                        {/* <Dropdown
                            // label='Favorite Fruit'
                            data={this.state.groupList}
                            value={'Group One'}
                            inputContainerStyle={styles.inputContainerStyle}
                            onChangeText={(value, index, data) => this.assignGroup(value, index + 1, data)}
                        /> */}
                    </View>
                    <Divider style={{ backgroundColor: 'grey', }} />

                    <View style={[styles.contentView, { marginTop: 15 }]}>
                        <Text style={styles.heading}>Date</Text>

                        <TouchableOpacity
                            style={{ marginVertical: 10 }}
                            onPress={() => this.showDatePicker()}
                        >
                            {eventDate ?
                                <Text>{eventDate}</Text> :
                                <Text style={styles.dummyTime}>{'Select Date'}</Text>
                            }
                        </TouchableOpacity>
                        {/* <Modal
                            isVisible={isDatePickeVisible}
                            animationIn="zoomInDown"
                            animationOut="zoomOutUp"
                            animationInTiming={600}
                            animationOutTiming={600}
                            backdropTransitionInTiming={600}
                            backdropTransitionOutTiming={600}> */}
                        <Overlay isVisible={isDatePickeVisible}
                            onBackdropPress={this.hideDatePicker}
                            overlayStyle={styles.OverlayStyle}
                        >
                            <Calendar
                                onDayPress={this.handleDatePicked}
                                style={{
                                    elevation: 4,

                                }}
                                theme={{
                                    calendarBackground: '#3b5998',
                                    textSectionTitleColor: '#FFFFFF',
                                    selectedDayBackgroundColor: '#F2B705',
                                    selectedDayTextColor: '#141259',
                                    todayTextColor: '#00adf5',
                                    dayTextColor: '#FFFFFF',
                                    textDisabledColor: '#d9e1e8',
                                    dotColor: '#FFFFFF',
                                    selectedDotColor: '#FFFFFF',
                                    arrowColor: '#FFFFFF',
                                    monthTextColor: '#FFFFFF',
                                    indicatorColor: '#FFFFFF',
                                    textDayFontWeight: '500',
                                    textMonthFontWeight: 'bold',
                                    textDayHeaderFontWeight: '500',
                                    textDayFontSize: 18,
                                    textMonthFontSize: 18,
                                    textDayHeaderFontSize: 16,
                                }}
                            />
                        </Overlay>

                        {/* <TouchableOpacity
                            style={styles.timeView}
                            onPress={() => this.showDatePicker()}
                        >
                            {eventDate ?
                                <Text>{eventDate}</Text> :
                                <Text style={styles.dummyTime}>{'1st jan 2020'}</Text>
                            }
                        </TouchableOpacity>
                        <DateTimePicker
                            isVisible={isDatePickeVisible}
                            onConfirm={this.handleDatePicked}
                            onCancel={this.hideDatePicker}
                            is24Hour={false}
                            mode="date"
                            datePickerModeAndroid="spinner"
                            timePickerModeAndroid="spinner"
                            date={new Date()}
                        /> */}
                    </View>
                    <Divider style={{ backgroundColor: 'grey', }} />
                    <View style={[styles.contentView, { marginTop: 15 }]}>
                        <Text style={styles.heading}>Time</Text>
                        <TouchableOpacity
                            style={styles.timeView}
                            onPress={() => this.showTimePicker()}
                        >
                            {eventTime ?
                                <Text>{eventTime}</Text> :
                                <Text style={styles.dummyTime}>{'Select Time'}</Text>
                            }
                        </TouchableOpacity>
                        <DateTimePicker
                            isVisible={isTimPickeVisible}
                            onConfirm={this.handleTimePicked}
                            onCancel={this.hideTimePicker}
                            is24Hour={false}
                            mode="time"
                            datePickerModeAndroid="spinner"
                            timePickerModeAndroid="spinner"
                            date={new Date()}
                        //    minimumDate={minimumDate}
                        />
                    </View>
                    <Divider style={{ backgroundColor: 'grey', }} />
                    <View style={[styles.contentView, { marginTop: 15 }]}>
                        <Text style={styles.heading}>Event Type</Text>
                        <Dropdown
                            // label='Favorite Fruit'
                            data={this.state.eventTypeList}
                            value={eventType? eventType:'Choose event type'}
                            inputContainerStyle={styles.inputContainerStyle}
                            onChangeText={(value, index, data) => this.EventType(value, index, data)}
                        />
                    </View>

                    <Divider style={{ backgroundColor: 'grey', }} />

                    <View style={[styles.contentView, { marginTop: 15 }]}>
                        <Text style={styles.heading}>Banner</Text>
                        <TouchableOpacity
                            style={styles.imgUpload}
                            onPress={() => this.selectImg()}
                        >
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1 }}>
                                    {avatarSource ?
                                        <Text>{avatarSource}</Text> :
                                    <Text>{this.state.uploaded}</Text>
                                    }
                                </View>
                                {avatarSource ?
                                    <Thumbnail
                                        style={styles.imgPreview}
                                        source={this.state.source}
                                    /> : 
                                    <Thumbnail
                                    style={styles.imgPreview}
                                    source={{uri:this.state.uploaded}}
                                    
                                />
                                    }



                            </View>
                        </TouchableOpacity>

                        {/* <TouchableOpacity
                            style={styles.imgUpload}
                            onPress={() => this.selectImg()}
                        >
                            {avatarSource ?
                                <Text>{avatarSource}</Text> :
                                <Text>Upload image</Text>
                            }
                        </TouchableOpacity> */}
                    </View>
                    <Divider style={{ backgroundColor: 'grey', }} />

                    <View style={[styles.contentView, { marginTop: 15, marginBottom: 50 }]}>
                        <Text style={styles.heading}>Notes , Program, Outline </Text>
                        <TextInput
                            placeholder={'type notes programs and outline here'}
                            placeholderTextColor={'grey'}
                            multiline={true}
                            onChangeText={(value) => this.setState({ notes: value })}
                            value={this.state.notes}
                        />
                    </View>
                </ScrollView>
                <TouchableOpacity
                    style={styles.buttonView}
                    onPress={() => this.createEvent()}
                >
                    {loading ?
                        <ActivityIndicator
                            size="small" color="#0000ff"
                        /> :
                        <Text style={styles.createEvent}>Update Event</Text>
                    }
                </TouchableOpacity>
                <DropdownAlert ref={ref => (this.dropDownAlertRef = ref)} />
            </Heading>


        )
    }
}
export default EditEvent