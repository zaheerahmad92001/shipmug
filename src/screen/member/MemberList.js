import React, { useEffect, useState, useRef } from 'react';
import {
  FlatList,
  RefreshControl,
  View,
  TouchableOpacity,
  Linking,
} from 'react-native';
import { Text, Thumbnail } from 'native-base';

import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { ROUTES } from '../../navigation/routes.constant';
import Axios from 'axios';

import MaterialICon from 'react-native-vector-icons/MaterialIcons';
import { LocalStorage, SERVER_URI, COLOR } from '../../common';
import { Capitalize } from '../../common/capatalize';
import { _logout } from '../../component/logout';
import { Filter } from '../../icon';
import Heading from '../../component/Heading';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import RBSheet from 'react-native-raw-bottom-sheet';
import TextAvatar from 'react-native-text-thumbnail';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
const MemberList = props => {
  const navigation = useNavigation();
  const [checked, setChecked] = useState();
  const refRBSheet = useRef();

  const [multiselect, setMultiselect] = useState(false);
  const [member, setMember] = useState([]);
  const [email, setEmail] = useState([]);
  const [loading, setLoading] = useState(true);
  const [dataSource, setdataSource] = useState([]);
  const _memberFetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/memberlist`,
      {
        user_id: await LocalStorage.getUserID(),
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        if (res.data.message == 'Invalid token or userid') {
          _logout(navigation);
        } else {
          setLoading(false);
          // await setMember(res.data.data);
          const data = await res.data.data.map(item => {
            item.isSelect = false;
            item.selectedClass = 'white';
            return item;
          });
          await setdataSource(data);
        }
      })
      .catch(err => {
        setLoading(false);
      });
  };

  useEffect(() => {
    if (loading) {
      _memberFetch();
    }
  });
  useFocusEffect(
    React.useCallback(() => {
      _memberFetch();
    }, []),
  );

  const _oncheck = async id => {
    const changedCheckbox =
      (await dataSource) && dataSource.find(cb => cb.id == id);

    changedCheckbox.isSelect = !changedCheckbox.isSelect;

    const checkboxes = Object.assign({}, checked, changedCheckbox);
    setChecked({ checkboxes });
    const available =
      (await dataSource) && dataSource.find(cb => cb.isSelect == true);
    if (available === undefined) {
      setMultiselect(false);
    } else {
      setMultiselect(true);
    }
    const mobile =
      changedCheckbox.isSelect == true && changedCheckbox.mobile_phone;
    const emails = changedCheckbox.isSelect == true && changedCheckbox.email;
    setMember([...member, mobile]);
    setEmail([...email, emails]);
  };

  const _addbookmark = async () => {
    const available =
      (await dataSource) && dataSource.filter(cb => cb.isSelect == true);
    const old = await LocalStorage.getBookmark();
    await LocalStorage.storeBookmark({ ...old, member: available });
    await _memberFetch();
    await alert('Members added to bookmark');
    await refRBSheet.current.close();
  };

  return (
    <Heading
      text={'Members'}
      lefticon={'ios-menu'}
      Iconmap={[
        {
          icon: (
            <Filter
              onPress={() => {
                navigation.navigate(ROUTES.MEMBERFILTER);
              }}
              style={{ margin: 10 }}
              fill={COLOR.SUBLABEL}
              height={20}
              width={20}
            />
          ),
        },

        {
          name: 'search',
          onPress: () => {
            navigation.navigate(ROUTES.MEMBERSEARCH);
          },
        },
        // {
        //   name: 'bell',
        //   onPress: () => {
        //     alert('notification screen');
        //   },
        // },
        {
          icon: (
            <MaterialICon
              onPress={() => {
                multiselect
                  ? refRBSheet.current.open()
                  : alert('Please select member');
              }}
              style={{ margin: 10 }}
              name={'more-vert'}
              color={COLOR.SUBLABEL}
              size={20}
            />
          ),
        },
      ]}
      onMenuPress={() => {
        navigation.openDrawer();
      }}>
      <FlatList
        data={dataSource}
        refreshControl={
          <RefreshControl
            colors={['#9Bd35A', '#689F38']}
            refreshing={loading}
            onRefresh={() => setLoading(true)}
          />
        }
        ListEmptyComponent={
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontFamily: 'product' }}>No members Found</Text>
          </View>
        }
        renderItem={({ item }) => (
          <TouchableOpacity
            onLongPress={() => {
              _oncheck(item.id);
            }}
            onPress={() => {
              multiselect
                ? _oncheck(item.id)
                : navigation.navigate(ROUTES.MEMBERPROFILE, {
                  type: 'MEMBER',
                  id: item.id,
                  address: item.address,
                  fullname: `${Capitalize(item.first_name)} ${Capitalize(
                    item.last_name,
                  )}`,
                  first_name: item.first_name,
                  last_name: item.last_name,
                  photo:
                    item.photo == 'http://app.sheepmug.com/public/uploads'
                      ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                      : `${item.photo}`,
                  phone: item.mobile_phone,
                  email: item.email,
                });
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: item.isSelect ? '#1e7a9d2b' : 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{ marginLeft: 15, width: '15%' }}>
              {item.isSelect ? (
                <SimpleLineIcons name={'check'} size={50} color={'#1e7a9d'} />
              ) : (
                  <Thumbnail
                    style={{ height: 50, width: 50 }}
                    source={{
                      uri:
                        item.photo == 'http://app.sheepmug.com/public/uploads'
                          ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                          : `${item.photo}`,
                    }}
                  />
                )}
            </View>
            <View style={{ marginLeft: 15, width: '75%' }}>
              <Text
                numberOfLines={1}
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                  width: '100%',
                }}>
                {Capitalize(item.first_name)} {Capitalize(item.last_name)}
              </Text>
              <Text
                style={{ color: '#9CA8AA', fontSize: responsiveFontSize(1.3) }}>
                {item.address}
              </Text>
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={item => item.key}
      />

      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        height={300}
        openDuration={250}
        customStyles={{
          wrapper: {
            backgroundColor: 'transparent',
          },
          container: {
            borderTopColor: 'rgba(0, 0, 0, 0.2)',
            borderTopWidth: 3,
            borderRadius: 10,
          },
          draggableIcon: {
            backgroundColor: '#CDCDCD',
          },
        }}>
        <View>
          <TouchableOpacity
            onPress={async () => {
              Linking.openURL(`sms:${member}`);
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{ marginLeft: 15 }}>
              <TextAvatar
                backgroundColor={'#1E7A9D'}
                textColor={'#FFF'}
                size={40}
                type={'circle'}>
                SMS
              </TextAvatar>
            </View>
            <View style={{ marginLeft: 20 }}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                }}>
                Bulk SMS Members
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`mailto:${email}`);
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{ marginLeft: 15 }}>
              <TextAvatar
                backgroundColor={'#1E7A9D'}
                textColor={'#FFF'}
                size={40}
                type={'circle'}>
                MAIL
              </TextAvatar>
            </View>
            <View style={{ marginLeft: 20 }}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                }}>
                Bulk Email Members
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              _addbookmark();
            }}
            style={{
              height: responsiveScreenHeight(10),
              width: '100%',
              marginTop: 5,
              backgroundColor: 'white', //'#1E7A9D',
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{ marginLeft: 15 }}>
              <TextAvatar
                backgroundColor={'#1E7A9D'}
                textColor={'#FFF'}
                size={40}
                type={'circle'}>
                BM
              </TextAvatar>
            </View>
            <View style={{ marginLeft: 20 }}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2),
                  color: '#3A4041',
                  fontFamily: 'product',
                }}>
                Bookmark Members
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </RBSheet>
    </Heading>
  );
};
export default MemberList;
