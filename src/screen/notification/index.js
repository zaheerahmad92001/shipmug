import React, {useEffect, useState} from 'react';
import {Thumbnail} from 'native-base';
import {Text, View, FlatList, RefreshControl} from 'react-native';
import {
  responsiveFontSize,
  responsiveScreenHeight,
} from 'react-native-responsive-dimensions';

import Heading from '../../component/Heading';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import MemberCompo from '../../component/memberComponent';
import Axios from 'axios';
import {SERVER_URI, LocalStorage} from '../../common';
import TextAvatarComp from '../../component/textAvatarComponent';
const DATA = [
  {
    key: 0,
  },
  {
    key: 1,
  },
];

const Notification = () => {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(true);
  const [dataSource, setdataSource] = useState([]);
  const _memberFetch = async () => {
    setLoading(true);
    Axios.get(
      `${SERVER_URI}/getNotificationList/${await LocalStorage.getUserID()}`,
      {
        user_id: await LocalStorage.getUserID(),
      },
    )
      .then(async res => {
        setLoading(false);

        await setdataSource(res.data);
      })
      .catch(err => {
        setLoading(false);
      });
  };

  useEffect(() => {
    if (loading) {
      _memberFetch();
    }
  });
  useFocusEffect(
    React.useCallback(() => {
      _memberFetch();
    }, []),
  );
  console.log(dataSource);
  return (
    <Heading
      text={'Notifications'}
      lefticon={'ios-menu'}
      Iconmap={[]}
      onMenuPress={() => {
        navigation.openDrawer();
      }}>
      <View
        style={{
          height: responsiveScreenHeight(100),
          width: '100%',
        }}>
        <FlatList
          data={dataSource}
          refreshControl={
            <RefreshControl
              colors={['#9Bd35A', '#689F38']}
              refreshing={loading}
              onRefresh={() => setLoading(true)}
            />
          }
          ListEmptyComponent={
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontFamily: 'product'}}>No Notification Found</Text>
            </View>
          }
          renderItem={({item}) => (
            <TextAvatarComp
              onPress={() => {}}
              name={item.message}
              textavarname={'SHEEP MUG'}
            />
          )}
          keyExtractor={item => item.id}
        />
      </View>
    </Heading>
  );
};
export default Notification;
