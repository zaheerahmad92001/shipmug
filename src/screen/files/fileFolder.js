import React, {useEffect, useState} from 'react';
import {
  FlatList,
  ActivityIndicator,
  View,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import {Text, ListItem, Left, Body, Thumbnail} from 'native-base';
import Header from '../../component/Header';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import {ROUTES} from '../../navigation/routes.constant';
import Axios from 'axios';
import {SERVER_URI, LocalStorage, COLOR, getRandomColor} from '../../common';
import {Capitalize} from '../../common/capatalize';
import TextAvatar from 'react-native-text-thumbnail';
import {_logout} from '../../component/logout';
import Heading from '../../component/Heading';
import {
  responsiveFontSize,
  responsiveScreenHeight,
} from 'react-native-responsive-dimensions';
import TextAvatarComp from '../../component/textAvatarComponent';

const FolderList = props => {
  const navigation = useNavigation();
  const [folderList, setfolderList] = useState([]);
  const [loading, setLoading] = useState(true);
  const _groupfetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/folder`,
      {
        user_id: await LocalStorage.getUserID(),
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        if (res.data.message == 'Invalid token or userid') {
          _logout(navigation);
        } else {
          setLoading(false);
          setfolderList(res.data.data);
        }
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    if (loading) {
      _groupfetch();
    }
  });
  useFocusEffect(
    React.useCallback(() => {
      _groupfetch();
    }, []),
  );

  const _view = async (navigation, folderid, name) => {
    navigation.navigate(ROUTES.FILELIST, {folderid, name});
  };

  return (
    <Heading
      text={'Reports'}
      lefticon={'ios-menu'}
      Iconmap={[
        {
          name: 'search',
          onPress: () => {
            navigation.navigate(ROUTES.FOLDERSEARCH);
          },
        },
        // {
        //   name: 'bell',
        //   onPress: () => {
        //     alert('bellpress');
        //   },
        // },
      ]}
      onMenuPress={() => {
        navigation.openDrawer();
      }}>
      <FlatList
        data={folderList}
        ListEmptyComponent={
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontFamily: 'product'}}>No Result Found</Text>
          </View>
        }
        refreshControl={
          <RefreshControl
            colors={['#9Bd35A', '#689F38']}
            refreshing={loading}
            onRefresh={() => setLoading(true)}
          />
        }
        renderItem={({item}) => (
          <TextAvatarComp
            onPress={() => {
              _view(navigation, item.id, item.name);
            }}
            name={Capitalize(item.name)}
            textavarname={item.name
              .split(' ')
              .slice(0, 2)
              .join('+')
              .toUpperCase()}
            // address={'147 Kotei Road Kotei'}
          />
        )}
        keyExtractor={item => item.id}
      />
    </Heading>
  );
};
export default FolderList;
