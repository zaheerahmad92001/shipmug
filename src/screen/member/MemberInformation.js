import React, {useState, useEffect} from 'react';
import {View, ActivityIndicator, Linking, TouchableOpacity} from 'react-native';
import {
  Content,
  Card,
  CardItem,
  Text,
  Left,
  Right,
  Body,
  Row,
  Col,
} from 'native-base';
import Axios from 'axios';
import {SERVER_URI, LocalStorage, COLOR} from '../../common';
import moment from 'moment';
import {Capitalize} from '../../common/capatalize';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import Attendence from '../../icon/Attendence';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
const RenderItem = ({title1, title2, info1, info2}) => {
  return (
    <Row style={{marginTop: 10}}>
      <Col style={{marginLeft: 25}}>
        <Text
          style={{
            color: COLOR.SUBLABEL,
            fontFamily: 'product',
            fontSize: responsiveFontSize(1.5),
          }}>
          {title1}
        </Text>
        <Text
          style={{
            color: '#3A4041',
            fontFamily: 'product',
            fontSize: responsiveFontSize(2),
            marginTop: 10,
          }}>
          {info1}
        </Text>
      </Col>
      <Col>
        <Text
          style={{
            color: COLOR.SUBLABEL,
            fontFamily: 'product',
            fontSize: responsiveFontSize(1.5),
          }}>
          {title2}
        </Text>
        <Text
          style={{
            color: '#3A4041',
            fontFamily: 'product',
            fontSize: responsiveFontSize(2),
            marginTop: 10,
          }}>
          {info2}
        </Text>
      </Col>
    </Row>
  );
};
const getAge = dateString => {
  var today = new Date();
  var birthDate = new Date(dateString);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
};

const MemberInformation = ({id}) => {
  const [loading, setLoading] = useState(true);
  const [member, setMember] = useState(null);
  const [expand, setExpand] = useState({
    personal: false,
    church: false,
    family: false,
    work: false,
    education: false,
    special: false,
    other: false,
  });
  const _onopencollapse = type => {
    if (type == 'personal') {
      setExpand({
        personal: true,
        church: false,
        family: false,
        work: false,
        education: false,
        special: false,
        other: false,
      });
    } else if (type == 'church') {
      setExpand({
        personal: false,
        church: true,
        family: false,
        work: false,
        education: false,
        special: false,
        other: false,
      });
    } else if (type == 'family') {
      setExpand({
        personal: false,
        church: false,
        family: true,
        work: false,
        education: false,
        special: false,
        other: false,
      });
    } else if (type == 'work') {
      setExpand({
        personal: false,
        church: false,
        family: false,
        work: true,
        education: false,
        special: false,
        other: false,
      });
    } else if (type == 'education') {
      setExpand({
        personal: false,
        church: false,
        family: false,
        work: false,
        education: true,
        special: false,
        other: false,
      });
    } else if (type == 'special') {
      setExpand({
        personal: false,
        church: false,
        family: false,
        work: false,
        education: false,
        special: true,
        other: false,
      });
    } else if (type == 'other') {
      setExpand({
        personal: false,
        church: false,
        family: false,
        work: false,
        education: false,
        special: false,
        other: true,
      });
    } else {
      setExpand({
        personal: false,
        church: false,
        family: false,
        work: false,
        education: false,
        special: false,
        other: false,
      });
    }
  };

  const _memberFetch = async () => {
    setLoading(true);
    Axios.get(
      `${SERVER_URI}/getMemberProfile/${id}`,
      // {
      //   user_id: await LocalStorage.getUserID(),
      //   member_id: id,
      // },
      // {
      //   headers: {
      //     'X-API-TOKEN': await LocalStorage.getToken(),
      //   },
      // },
    )
      .then(async res => {
        setLoading(false);
        setMember(res.data);
      })
      .catch(err => {
        setLoading(false);
      });
  };

  useEffect(() => {
    if (loading) {
      _memberFetch();
    }
  });

  return member == null ? (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
      <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
    </View>
  ) : (
    <Content>
      <TouchableOpacity
        onPress={() => {
          expand.personal ? _onopencollapse(null) : _onopencollapse('personal');
        }}
        style={{
          height: responsiveScreenHeight(8),
          width: '100%',
          borderTopColor: '#00000017',
          borderTopWidth: 2,
          borderBottomColor: '#00000017',
          borderBottomWidth: 2,
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{marginLeft: 50}}>
          {/* <Attendence width={30} height={30} /> */}
          <FontAwesome name={'info'} size={30} color={'#9CA8AA'} />
        </View>
        <View style={{marginLeft: 30}}>
          <Text
            style={{
              fontSize: responsiveFontSize(2),
              color: '#3A4041',
              fontWeight: '800',
              fontFamily: 'product',
            }}>
            Personal Info
          </Text>
        </View>
      </TouchableOpacity>
      {expand.personal && (
        <>
          <Row style={{marginTop: 10}}>
            <Col style={{marginLeft: 25}}>
              <Text
                style={{
                  color: COLOR.SUBLABEL,
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(1.5),
                }}>
                First Name
              </Text>
              <Text
                style={{
                  color: '#3A4041',
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(2),
                  marginTop: 10,
                }}>
                {member['Personal info'].first_name
                  ? Capitalize(member['Personal info'].first_name)
                  : '---'}
              </Text>
            </Col>
            <Col>
              <Text
                style={{
                  color: COLOR.SUBLABEL,
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(1.5),
                }}>
                Date Of Birth
              </Text>
              <Text
                style={{
                  color: '#3A4041',
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(2),
                  marginTop: 10,
                }}>
                {member['Personal info'].dob
                  ? moment(member['Personal info'].dob).format(
                      'dddd, Do MMMM, YY',
                    )
                  : '---'}
              </Text>
            </Col>
          </Row>
          <Row style={{marginTop: 10}}>
            <Col style={{marginLeft: 25}}>
              <Text
                style={{
                  color: COLOR.SUBLABEL,
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(1.5),
                }}>
                Last Name
              </Text>
              <Text
                style={{
                  color: '#3A4041',
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(2),
                  marginTop: 10,
                }}>
                {member['Personal info'].last_name
                  ? Capitalize(member['Personal info'].last_name)
                  : '---'}
              </Text>
            </Col>
            <Col>
              <Text
                style={{
                  color: COLOR.SUBLABEL,
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(1.5),
                }}>
                Age
              </Text>
              <Text
                style={{
                  color: '#3A4041',
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(2),
                  marginTop: 10,
                }}>
                {member['Personal info'].dob
                  ? getAge(member['Personal info'].dob)
                  : '---'}
              </Text>
            </Col>
          </Row>
          <Row style={{marginTop: 10}}>
            <Col style={{marginLeft: 25}}>
              <Text
                style={{
                  color: COLOR.SUBLABEL,
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(1.5),
                }}>
                Gender
              </Text>
              <Text
                style={{
                  color: '#3A4041',
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(2),
                  marginTop: 10,
                }}>
                {member['Personal info'].gender
                  ? Capitalize(member['Personal info'].gender)
                  : '---'}
              </Text>
            </Col>
            <Col>
              <Text
                style={{
                  color: COLOR.SUBLABEL,
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(1.5),
                }}>
                Email
              </Text>
              <Text
                style={{
                  color: '#3A4041',
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(2),
                  marginTop: 10,
                }}>
                {member['Personal info'].email
                  ? member['Personal info'].email
                  : '---'}
              </Text>
            </Col>
          </Row>
          <Row style={{marginTop: 10}}>
            <Col style={{marginLeft: 25}}>
              <Text
                style={{
                  color: COLOR.SUBLABEL,
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(1.5),
                }}>
                Address
              </Text>
              <Text
                style={{
                  color: '#3A4041',
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(2),
                  marginTop: 10,
                }}>
                {member['Personal info'].address
                  ? Capitalize(member['Personal info'].address)
                  : '---'}
              </Text>
            </Col>
            <Col>
              <Text
                style={{
                  color: COLOR.SUBLABEL,
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(1.5),
                }}>
                Phone Number
              </Text>
              <Text
                style={{
                  color: '#3A4041',
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(2),
                  marginTop: 10,
                }}>
                {member['Personal info'].mobile_phone
                  ? member['Personal info'].mobile_phone
                  : '---'}
              </Text>
            </Col>
          </Row>
        </>
      )}

      <TouchableOpacity
        onPress={() => {
          expand.church ? _onopencollapse(null) : _onopencollapse('church');
        }}
        style={{
          height: responsiveScreenHeight(8),
          width: '100%',
          borderTopColor: '#00000017',
          borderTopWidth: 2,
          borderBottomColor: '#00000017',
          borderBottomWidth: 2,
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{marginLeft: 30}}>
          <FontAwesome name={'church'} size={30} color={'#9CA8AA'} />
        </View>
        <View style={{marginLeft: 30}}>
          <Text
            style={{
              fontSize: responsiveFontSize(2),
              color: '#3A4041',
              fontWeight: '800',
              fontFamily: 'product',
            }}>
            Church Details
          </Text>
        </View>
      </TouchableOpacity>
      {expand.church && (
        <>
          <RenderItem
            title1={'Deparment Groups'}
            title2={'Status'}
            info1={
              member['Church info'].department_groups
                ? member['Church info'].department_groups
                : '---'
            }
            info2={
              member['Church info'].status
                ? member['Church info'].status
                : '---'
            }
          />
          <RenderItem
            title1={'Date Of Baptism'}
            title2={'Position'}
            info1={
              member['Church info'].date_of_baptism
                ? moment(member['Church info'].date_of_baptism).format(
                    'dddd, Do MMMM, YY',
                  )
                : '---'
            }
            info2={
              member['Church info'].position
                ? member['Church info'].position
                : '---'
            }
          />
        </>
      )}
      <TouchableOpacity
        onPress={() => {
          expand.family ? _onopencollapse(null) : _onopencollapse('family');
        }}
        style={{
          height: responsiveScreenHeight(8),
          width: '100%',
          borderTopColor: '#00000017',
          borderTopWidth: 2,
          borderBottomColor: '#00000017',
          borderBottomWidth: 2,
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{marginLeft: 30}}>
          <FontAwesome name={'users'} size={30} color={'#9CA8AA'} />
        </View>
        <View style={{marginLeft: 30}}>
          <Text
            style={{
              fontSize: responsiveFontSize(2),
              color: '#3A4041',
              fontWeight: '800',
              fontFamily: 'product',
            }}>
            Family Information
          </Text>
        </View>
      </TouchableOpacity>
      {expand.family && (
        <>
          <RenderItem
            title1={'Spouse Firstname'}
            title2={'Spouse Lastname'}
            info1={
              member['Family info'].spouse_first_name
                ? member['Family info'].spouse_first_name
                : '---'
            }
            info2={
              member['Family info'].spouse_last_name
                ? member['Family info'].spouse_last_name
                : '---'
            }
          />
          <RenderItem
            title1={'Spouse Address'}
            title2={'Marriage Date'}
            info1={
              member['Family info'].spouse_address
                ? member['Family info'].spouse_address
                : '---'
            }
            info2={
              member['Family info'].date_marriage_separated
                ? member['Family info'].date_marriage_separated
                : '---'
            }
          />
          <RenderItem
            title1={'Marriage Status'}
            title2={'Emergency Contact Name'}
            info1={
              member['Family info'].marriage_status
                ? member['Family info'].marriage_status
                : '---'
            }
            info2={
              member['Family info'].emergency_contact_full_name
                ? member['Family info'].emergency_contact_full_name
                : '---'
            }
          />
          <RenderItem
            title1={'Emergency Contact Address'}
            title2={'Emergency Contact Number'}
            info1={
              member['Family info'].emergency_contact_address
                ? member['Family info'].emergency_contact_address
                : '---'
            }
            info2={
              member['Family info'].emergency_contact_number
                ? member['Family info'].emergency_contact_number
                : '---'
            }
          />
        </>
      )}

      <TouchableOpacity
        onPress={() => {
          expand.work ? _onopencollapse(null) : _onopencollapse('work');
        }}
        style={{
          height: responsiveScreenHeight(8),
          width: '100%',
          borderTopColor: '#00000017',
          borderTopWidth: 2,
          borderBottomColor: '#00000017',
          borderBottomWidth: 2,
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{marginLeft: 30}}>
          <MaterialIcons name={'work'} size={30} color={'#9CA8AA'} />
        </View>
        <View style={{marginLeft: 30}}>
          <Text
            style={{
              fontSize: responsiveFontSize(2),
              color: '#3A4041',
              fontWeight: '800',
              fontFamily: 'product',
            }}>
            Work Information
          </Text>
        </View>
      </TouchableOpacity>

      {expand.work && (
        <>
          <RenderItem
            title1={'Work Placename'}
            title2={'Work Position'}
            info1={
              member['Professions Info'].workplace_name
                ? member['Professions Info'].workplace_name
                : '---'
            }
            info2={
              member['Professions Info'].position
                ? member['Professions Info'].position
                : '---'
            }
          />
          <Row style={{marginTop: 10}}>
            <Col style={{marginLeft: 25}}>
              <Text
                style={{
                  color: COLOR.SUBLABEL,
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(1.5),
                }}>
                Address
              </Text>
              <Text
                style={{
                  color: '#3A4041',
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(2),
                  marginTop: 10,
                }}>
                {member['Professions Info'].address
                  ? member['Professions Info'].address
                  : '---'}
              </Text>
            </Col>
          </Row>
        </>
      )}

      <TouchableOpacity
        onPress={() => {
          expand.education
            ? _onopencollapse(null)
            : _onopencollapse('education');
        }}
        style={{
          height: responsiveScreenHeight(8),
          width: '100%',
          borderTopColor: '#00000017',
          borderTopWidth: 2,
          borderBottomColor: '#00000017',
          borderBottomWidth: 2,
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{marginLeft: 30}}>
          <MaterialIcons name={'work'} size={30} color={'#9CA8AA'} />
        </View>
        <View style={{marginLeft: 30}}>
          <Text
            style={{
              fontSize: responsiveFontSize(2),
              color: '#3A4041',
              fontWeight: '800',
              fontFamily: 'product',
            }}>
            Education Background
          </Text>
        </View>
      </TouchableOpacity>

      {expand.education && (
        <>
          <RenderItem
            title1={'School Name'}
            title2={'School Location'}
            info1={
              member['Educational Background'].school_name
                ? member['Educational Background'].school_name
                : '---'
            }
            info2={
              member['Educational Background'].school_location
                ? member['Educational Background'].school_location
                : '---'
            }
          />
          <RenderItem
            title1={'Course Name'}
            title2={'Hostel Hallname'}
            info1={
              member['Educational Background'].course_name
                ? member['Educational Background'].course_name
                : '---'
            }
            info2={
              member['Educational Background'].hostel_hall_name
                ? member['Educational Background'].hostel_hall_name
                : '---'
            }
          />

          <Row style={{marginTop: 10}}>
            <Col style={{marginLeft: 25}}>
              <Text
                style={{
                  color: COLOR.SUBLABEL,
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(1.5),
                }}>
                Room Number
              </Text>
              <Text
                style={{
                  color: '#3A4041',
                  fontFamily: 'product',
                  fontSize: responsiveFontSize(2),
                  marginTop: 10,
                }}>
                {member['Educational Background'].room_number
                  ? member['Educational Background'].room_number
                  : '---'}
              </Text>
            </Col>
          </Row>
        </>
      )}

      <TouchableOpacity
        onPress={() => {
          expand.special ? _onopencollapse(null) : _onopencollapse('special');
        }}
        style={{
          height: responsiveScreenHeight(8),
          width: '100%',
          borderTopColor: '#00000017',
          borderTopWidth: 2,
          borderBottomColor: '#00000017',
          borderBottomWidth: 2,
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{marginLeft: 30}}>
          <FontAwesome name={'star'} size={30} color={'#9CA8AA'} />
        </View>
        <View style={{marginLeft: 30}}>
          <Text
            style={{
              fontSize: responsiveFontSize(2),
              color: '#3A4041',
              fontWeight: '800',
              fontFamily: 'product',
            }}>
            Special Dates
          </Text>
        </View>
      </TouchableOpacity>
      {expand.special && (
        <>
          {member['Special Dates'].length > 0 ? (
            member['Special Dates'].map(info => (
              <RenderItem
                title1={'Title'}
                title2={'Date'}
                info1={info && info.field_name ? info.field_name : '---'}
                info2={
                  info && info.value
                    ? moment(info.value).format('dddd, Do MMMM, YY')
                    : '---'
                }
              />
            ))
          ) : (
            <Text style={{fontFamily: 'product', color: '#5B6466'}}>
              No special data found
            </Text>
          )}
        </>
      )}

      <TouchableOpacity
        onPress={() => {
          expand.other ? _onopencollapse(null) : _onopencollapse('other');
        }}
        style={{
          height: responsiveScreenHeight(8),
          width: '100%',
          borderTopColor: '#00000017',
          borderTopWidth: 2,
          borderBottomColor: '#00000017',
          borderBottomWidth: 2,
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{marginLeft: 30}}>
          <AntDesign name={'logout'} size={30} color={'#9CA8AA'} />
        </View>
        <View style={{marginLeft: 30}}>
          <Text
            style={{
              fontSize: responsiveFontSize(2),
              color: '#3A4041',
              fontWeight: '800',
              fontFamily: 'product',
            }}>
            Others
          </Text>
        </View>
      </TouchableOpacity>
      {expand.other && (
        <>
          {member['Custom Data'].length > 0 ? (
            member['Custom Data'].map(info => (
              <RenderItem
                title1={'Title'}
                title2={'Description'}
                info1={info && info.field_name ? info.field_name : '---'}
                info2={info && info.value ? info.value : '---'}
              />
            ))
          ) : (
            <Text style={{fontFamily: 'product', color: '#5B6466'}}>
              No other data found
            </Text>
          )}
        </>
      )}
    </Content>
  );
};
export default MemberInformation;
{
  /* <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>First Name:</Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <Text style={{fontFamily: 'product'}}>
            {member[0].first_name ? Capitalize(member[0].first_name) : '---'}
          </Text>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>Middle Name:</Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <Text style={{fontFamily: 'product'}}>
            {member[0].middle_name ? Capitalize(member[0].middle_name) : '---'}
          </Text>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>Last Name:</Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <Text style={{fontFamily: 'product'}}>
            {member[0].last_name ? Capitalize(member[0].last_name) : '---'}
          </Text>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>Gender:</Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <Text style={{fontFamily: 'product'}}>
            {member[0].gender ? Capitalize(member[0].gender) : '---'}
          </Text>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>Emial:</Text>
        </Left>

        <View style={{marginLeft: -60}} />
        <Body>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`mailto:${member[0].email}`);
            }}>
            <Text style={{fontFamily: 'product'}}>
              {member[0].email ? member[0].email : '---'}
            </Text>
          </TouchableOpacity>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>Phone:</Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`tel:${member[0].mobile_phone}`);
            }}>
            <Text style={{fontFamily: 'product'}}>
              {member[0].mobile_phone ? member[0].mobile_phone : '---'}
            </Text>
          </TouchableOpacity>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>Home Phone:</Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`tel:${member[0].home_phone}`);
            }}>
            <Text style={{fontFamily: 'product'}}>
              {member[0].home_phone ? member[0].home_phone : '---'}
            </Text>
          </TouchableOpacity>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>Work Phone:</Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`tel:${member[0].work_phone}`);
            }}>
            <Text style={{fontFamily: 'product'}}>
              {member[0].work_phone ? member[0].work_phone : '---'}
            </Text>
          </TouchableOpacity>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>
            Date Of Birth:
          </Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <Text style={{fontFamily: 'product'}}>
            {member[0].dob
              ? moment(member[0].dob).format('dddd, Do MMMM, YY')
              : '---'}
          </Text>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>Age:</Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <Text style={{fontFamily: 'product'}}>
            {member[0].dob ? getAge(member[0].dob) : '---'}
          </Text>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>
            Marriage Status:
          </Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <Text style={{fontFamily: 'product'}}>
            {member[0].marital_status
              ? Capitalize(member[0].marital_status)
              : '---'}
          </Text>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>Notes:</Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <Text style={{fontFamily: 'product'}}>
            {member[0].notes ? Capitalize(member[0].notes) : '---'}
          </Text>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>Groups:</Text>
        </Left>

        <View style={{marginLeft: -60}} />
        <Body>
          {member[0].groupInfo.map((res) => {
            return (
              <Text style={{fontFamily: 'product'}}>{Capitalize(res.name)}</Text>
            );
          })}
        </Body>
      </CardItem>

      <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>
        Other Information
      </Text>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>Address:</Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <Text style={{fontFamily: 'product'}}>
            {member[0].address ? Capitalize(member[0].address) : '---'}
          </Text>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>Status:</Text>
        </Left>
        <View style={{marginLeft: -60}} />
        <Body>
          <Text style={{fontFamily: 'product'}}>
            {member[0].status ? Capitalize(member[0].status) : '---'}
          </Text>
        </Body>

        <Row style={{marginTop: 10}}>
            <Col style={{marginLeft: 25}}>
              <Text style={{color: '#9CA8AA', fontFamily: 'product'}}>STATUS</Text>
              <Text style={{fontFamily: 'product', color: '#5B6466'}}>
                {member['Church info'].status
                  ? Capitalize(member['Church info'].status)
                  : '---'}
              </Text>
            </Col>
            <Col>
              <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>POSITION</Text>
              <Text style={{fontFamily: 'product'}}>
                {member['Church info'].position
                  ? Capitalize(member['Church info'].position)
                  : '---'}
              </Text>
            </Col>
          </Row>
          <Row style={{marginTop: 10}}>
            <Col style={{marginLeft: 25}}>
              <Text style={{color: '#9CA8AA', fontFamily: 'product'}}>
                BAPTISM DATE
              </Text>
              <Text style={{fontFamily: 'product', color: '#5B6466'}}>
                {member['Church info'].date_of_baptism
                  ? moment(member['Church info'].date_of_baptism).format(
                      'dddd, Do MMMM, YY',
                    )
                  : '---'}
              </Text>
            </Col>
            <Col>
              <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>
                DATE JOINED
              </Text>
              <Text style={{fontFamily: 'product'}}>
                {member['Church info'].date_of_joining
                  ? moment(member['Church info'].date_of_joining).format(
                      'dddd, Do MMMM, YY',
                    )
                  : '---'}
              </Text>
            </Col>
          </Row>
          <Row style={{marginTop: 10}}>
            <Col style={{marginLeft: 25}}>
              <Text style={{color: '#9CA8AA', fontFamily: 'product'}}>
                TITHE NUMBER
              </Text>
              <Text style={{fontFamily: 'product', color: '#5B6466'}}>
                {member['Church info'].tithe_number
                  ? member['Church info'].tithe_number
                  : '---'}
              </Text>
            </Col>
            <Col>
              <Text style={{color: '#b8b6bf', fontFamily: 'product'}}>
                DEPARTMENT GROUPS
              </Text>
              <Text style={{fontFamily: 'product'}}>
                {member['Church info'].department_groups
                  ? member['Church info'].department_groups
                  : '---'}
              </Text>
            </Col>
          </Row>
      </CardItem> */
}
