import React, {useState, useEffect} from 'react';
import {View, Dimensions, ActivityIndicator, TextInput} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Header from '../../component/Header';
import {
  Text,
  Picker,
  Item,
  Footer,
  FooterTab,
  Button,
  Container,
} from 'native-base';
import {ROUTES} from '../../navigation/routes.constant';

import {COLOR, LocalStorage, SERVER_URI} from '../../common';
import Axios from 'axios';
import Heading from '../../component/Heading';
import {
  responsiveHeight,
  responsiveFontSize,
  responsiveScreenWidth,
} from 'react-native-responsive-dimensions';
const MemberFilter = () => {
  const navigation = useNavigation();

  const [member, setMember] = useState([]);
  const [loading, setLoading] = useState(true);
  const _groupfetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/groupsnew`,
      {
        user_id: await LocalStorage.getUserID(),
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);
        setMember(res.data.data);
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    if (loading) {
      _groupfetch();
    }
  });

  const [gender, setGender] = useState('null');
  const [month, setMonth] = useState('0');
  const [marital, setMarital] = useState('null');
  const [memberstatus, setmemberStatus] = useState('null');
  // const [age, setAge] = useState(22);
  const [group, setGroup] = useState('null');
  const [start, setStart] = useState();
  const [end, setEnd] = useState();

  const _clearall = () => {
    setGender('null');
    setMarital('null');
    setmemberStatus('null');
    setMonth('0');
    setStart(null);
    setEnd(null);
    // setAge(22);
    setGroup(null);
  };

  const _filterdata = async () => {
    let group_id = group == null ? null : group;

    Axios.post(
      `${SERVER_URI}/memberfilter`,
      {
        user_id: await LocalStorage.getUserID(),
        group_id,
        birthday_month: month,
        marital_status: marital,
        member_status: memberstatus,
        gender: gender,
        age_from: 22,
        age_to: 500,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        if (res.data.status == 'fail') {
          alert('No Results found, Try another Category please.');
        } else {
          navigation.navigate(ROUTES.MEMBERFILTERRESULT, {
            member: res.data.data,
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <Heading
      text={'Filter Members'}
      lefticon={'ios-arrow-round-back'}
      Iconmap={
        [
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('notification screen');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        navigation.goBack();
      }}>
      {loading ? (
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
        </View>
      ) : (
        <Container
          style={{
            backgroundColor: '#0000000D',
            height: responsiveHeight(100),
            width: '100%',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: '97%',
              backgroundColor: 'white',
              height: responsiveHeight(8),
              marginTop: 10,
            }}>
            <Picker
              iosHeader="Select one"
              mode="dropdown"
              selectedValue={group}
              onValueChange={e => {
                setGroup(e);
              }}>
              <Item
                style={{fontFamily: 'product'}}
                label={'Group'}
                value={'null'}
              />
              {member.map(res => {
                return (
                  <Item
                    style={{fontFamily: 'product'}}
                    label={res.name}
                    value={res.id}
                  />
                );
              })}
            </Picker>
          </View>
          <View
            style={{
              width: '97%',
              backgroundColor: 'white',
              height: responsiveHeight(8),
              marginTop: 10,
            }}>
            <Picker
              iosHeader="Select one"
              mode="dropdown"
              selectedValue={memberstatus}
              onValueChange={e => {
                setmemberStatus(e);
              }}>
              <Item
                style={{fontFamily: 'product'}}
                label="Member Status"
                value="null"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="Attender"
                value="attender"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="Visitor"
                value="visitor"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="Inactive"
                value="inactive"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="Unknown"
                value="unknown"
              />
            </Picker>
          </View>
          <View
            style={{
              width: '97%',
              backgroundColor: 'white',
              height: responsiveHeight(8),
              marginTop: 10,
            }}>
            <Picker
              iosHeader="Select one"
              mode="dropdown"
              selectedValue={marital}
              onValueChange={e => {
                setMarital(e);
              }}>
              <Item
                style={{fontFamily: 'product'}}
                label="Marital Status"
                value="null"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="Single"
                value="single"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="Engaged"
                value="engaged"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="Married"
                value="married"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="Divorced"
                value="divorced"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="Widowed"
                value="widowed"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="Separated"
                value="separated"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="Unknown"
                value="unknown"
              />
            </Picker>
          </View>
          <View
            style={{
              width: '97%',
              backgroundColor: 'white',
              height: responsiveHeight(8),
              marginTop: 10,
            }}>
            <Picker
              iosHeader="Select one"
              mode="dropdown"
              selectedValue={month}
              onValueChange={e => {
                setMonth(e);
              }}>
              <Item
                style={{fontFamily: 'product'}}
                label="Birthday Month"
                value="0"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="January"
                value="01"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="February"
                value="02"
              />
              <Item style={{fontFamily: 'product'}} label="March" value="03" />
              <Item style={{fontFamily: 'product'}} label="April" value="04" />
              <Item style={{fontFamily: 'product'}} label="May" value="05" />
              <Item style={{fontFamily: 'product'}} label="June" value="06" />
              <Item style={{fontFamily: 'product'}} label="July" value="07" />
              <Item style={{fontFamily: 'product'}} label="August" value="08" />
              <Item
                style={{fontFamily: 'product'}}
                label="September"
                value="09"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="October"
                value="10"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="November"
                value="11"
              />
              <Item
                style={{fontFamily: 'product'}}
                label="December"
                value="12"
              />
            </Picker>
          </View>
          <View
            style={{
              width: '97%',
              backgroundColor: 'white',
              height: responsiveHeight(8),
              marginTop: 10,
            }}>
            <Picker
              iosHeader="Select one"
              mode="dropdown"
              selectedValue={gender}
              onValueChange={e => {
                setGender(e);
              }}>
              <Item
                style={{fontFamily: 'product'}}
                label="Select Gender"
                value="null"
              />
              <Item style={{fontFamily: 'product'}} label="Male" value="male" />
              <Item
                style={{fontFamily: 'product'}}
                label="Female"
                value="female"
              />
            </Picker>
          </View>

          <View
            style={{
              flexDirection: 'row',
              width: '95%',
              height: responsiveHeight(8),
              marginTop: 10,
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingHorizontal: 1,
            }}>
            <Text style={{fontFamily: 'product', width: '40%'}}>Age Range</Text>
            <View style={{flexDirection: 'row', width: '65%'}}>
              <View
                style={{
                  borderColor: '#00000029',
                  borderWidth: 2,
                  backgroundColor: 'white',
                  marginBottom: 10,
                  width: '45%',
                }}>
                <TextInput
                  placeholder={'From'}
                  style={{fontFamily: 'product'}}
                  onChangeText={text => {
                    setStart(text);
                  }}
                  value={start}
                  keyboardType={'number-pad'}
                />
              </View>
              <View
                style={{
                  borderColor: '#00000029',
                  borderWidth: 2,
                  width: '45%',
                  backgroundColor: 'white',
                  marginBottom: 10,
                  marginLeft: 5,
                }}>
                <TextInput
                  placeholder={'To'}
                  style={{fontFamily: 'product'}}
                  value={end}
                  onChangeText={text => {
                    setEnd(text);
                  }}
                  keyboardType={'number-pad'}
                />
              </View>
            </View>
          </View>
          <View
            style={{
              width: '100%',
              marginBottom: 10,
              justifyContent: 'space-between',
              flexDirection: 'row',
              paddingHorizontal: responsiveScreenWidth(15),
            }}>
            <Button
              onPress={() => {
                _clearall();
              }}
              style={{
                width: responsiveScreenWidth(30),
                marginTop: responsiveHeight(5),
                borderRadius: 8,
                height: 50,
                backgroundColor: '#9CA8AA',
                justifyContent: 'center',
              }}>
              <Text
                uppercase={false}
                style={{
                  fontFamily: 'product',
                  color: 'white',
                  alignSelf: 'center',
                  fontSize: responsiveFontSize(1.5),
                }}>
                Clear
              </Text>
            </Button>

            <Button
              style={{
                width: responsiveScreenWidth(30),
                marginTop: responsiveHeight(5),
                borderRadius: 8,
                height: 50,
                backgroundColor: '#0178f2',
                justifyContent: 'center',
              }}
              onPress={() => {
                _filterdata();
              }}>
              <Text
                uppercase={false}
                style={{
                  fontFamily: 'product',
                  color: 'white',
                  alignSelf: 'center',
                  fontSize: responsiveFontSize(1.5),
                }}>
                Apply
              </Text>
            </Button>
          </View>
        </Container>
      )}
    </Heading>
  );
};
export default MemberFilter;
{
  /* <Container>
<View
  style={{
    backgroundColor: COLOR.LIGHTBLUE,
    height: Dimensions.get('window').height,
    width: '100%',
    alignItems: 'center',
  }}>
  <Text
    style={{
      color: '#b8b6bf',
      fontSize: 20,
      marginTop: 10,
      marginBottom: 10,
      fontFamily: 'sf',
    }}>
    Groups
  </Text>

  <View
    style={{
      width: '90%',
      backgroundColor: 'white',
      marginBottom: 10,
    }}>
    <Picker
      iosHeader="Select one"
      mode="dropdown"
      selectedValue={group}
      onValueChange={e => {
        setGroup(e);
      }}>
      <Item
        style={{fontFamily: 'sf'}}
        label={'Select Group'}
        value={'null'}
      />
      {member.map(res => {
        return (
          <Item
            style={{fontFamily: 'sf'}}
            label={res.name}
            value={res.id}
          />
        );
      })}
    </Picker>
  </View>

  <Text
    style={{
      color: '#b8b6bf',
      fontSize: 20,
      marginTop: 10,
      marginBottom: 10,
      fontFamily: 'sf',
    }}>
    Member Status
  </Text>

  <View
    style={{
      width: '90%',
      backgroundColor: 'white',
      marginBottom: 10,
    }}>
    <Picker
      iosHeader="Select one"
      mode="dropdown"
      selectedValue={memberstatus}
      onValueChange={e => {
        setmemberStatus(e);
      }}>
      <Item
        style={{fontFamily: 'sf'}}
        label="Select status"
        value="null"
      />
      <Item
        style={{fontFamily: 'sf'}}
        label="Attender"
        value="attender"
      />
      <Item
        style={{fontFamily: 'sf'}}
        label="Visitor"
        value="visitor"
      />
      <Item
        style={{fontFamily: 'sf'}}
        label="Inactive"
        value="inactive"
      />
      <Item
        style={{fontFamily: 'sf'}}
        label="Unknown"
        value="unknown"
      />
    </Picker>
  </View>
  <Text
    style={{
      color: '#b8b6bf',
      fontSize: 20,
      marginTop: 10,
      marginBottom: 10,
      fontFamily: 'sf',
    }}>
    Marital Status
  </Text>

  <View
    style={{
      width: '90%',
      backgroundColor: 'white',
      marginBottom: 10,
    }}>
    <Picker
      iosHeader="Select one"
      mode="dropdown"
      selectedValue={marital}
      onValueChange={e => {
        setMarital(e);
      }}>
      <Item
        style={{fontFamily: 'sf'}}
        label="Select Marital status"
        value="null"
      />
      <Item
        style={{fontFamily: 'sf'}}
        label="Single"
        value="single"
      />
      <Item
        style={{fontFamily: 'sf'}}
        label="Engaged"
        value="engaged"
      />
      <Item
        style={{fontFamily: 'sf'}}
        label="Married"
        value="married"
      />
      <Item
        style={{fontFamily: 'sf'}}
        label="Divorced"
        value="divorced"
      />
      <Item
        style={{fontFamily: 'sf'}}
        label="Widowed"
        value="widowed"
      />
      <Item
        style={{fontFamily: 'sf'}}
        label="Separated"
        value="separated"
      />
      <Item
        style={{fontFamily: 'sf'}}
        label="Unknown"
        value="unknown"
      />
    </Picker>
  </View>
  <Text
    style={{
      color: '#b8b6bf',
      fontSize: 20,
      marginTop: 10,
      marginBottom: 10,
      fontFamily: 'sf',
    }}>
    Birthday Month
  </Text>

  <View
    style={{
      width: '90%',
      backgroundColor: 'white',
      marginBottom: 10,
    }}>
    <Picker
      iosHeader="Select one"
      mode="dropdown"
      selectedValue={month}
      onValueChange={e => {
        setMonth(e);
      }}>
      <Item
        style={{fontFamily: 'sf'}}
        label="Select Month"
        value="0"
      />
      <Item style={{fontFamily: 'sf'}} label="January" value="01" />
      <Item style={{fontFamily: 'sf'}} label="February" value="02" />
      <Item style={{fontFamily: 'sf'}} label="March" value="03" />
      <Item style={{fontFamily: 'sf'}} label="April" value="04" />
      <Item style={{fontFamily: 'sf'}} label="May" value="05" />
      <Item style={{fontFamily: 'sf'}} label="June" value="06" />
      <Item style={{fontFamily: 'sf'}} label="July" value="07" />
      <Item style={{fontFamily: 'sf'}} label="August" value="08" />
      <Item style={{fontFamily: 'sf'}} label="September" value="09" />
      <Item style={{fontFamily: 'sf'}} label="October" value="10" />
      <Item style={{fontFamily: 'sf'}} label="November" value="11" />
      <Item style={{fontFamily: 'sf'}} label="December" value="12" />
    </Picker>
  </View>

  <Text
    style={{
      color: '#b8b6bf',
      fontSize: 20,
      marginTop: 10,
      marginBottom: 10,
      fontFamily: 'sf',
    }}>
    Gender
  </Text>
  <View
    style={{
      width: '90%',
      backgroundColor: 'white',
      marginBottom: 10,
    }}>
    <Picker
      iosHeader="Select one"
      mode="dropdown"
      selectedValue={gender}
      onValueChange={e => {
        setGender(e);
      }}>
      <Item
        style={{fontFamily: 'sf'}}
        label="Select Gender"
        value="null"
      />
      <Item style={{fontFamily: 'sf'}} label="Male" value="male" />
      <Item
        style={{fontFamily: 'sf'}}
        label="Female"
        value="female"
      />
    </Picker>
  </View>
  <Text
    style={{
      color: '#b8b6bf',
      fontSize: 20,
      marginTop: 10,
      marginBottom: 10,
      fontFamily: 'sf',
    }}>
    Age Range
  </Text>

  <View
    style={{
      flexDirection: 'row',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 1,
    }}>
    <View
      style={{
        width: '45%',
        backgroundColor: 'white',
        marginBottom: 10,
      }}>
      <TextInput
        placeholder={'From'}
        // value={22}
        // editable={false}
        onChangeText={text => {
          setStart(text);
        }}
        value={start}
        keyboardType={'number-pad'}
      />
    </View>
    <View
      style={{
        width: '45%',
        backgroundColor: 'white',
        marginBottom: 10,
        marginLeft: 5,
      }}>
      <TextInput
        placeholder={'To'}
        value={end}
        onChangeText={text => {
          setEnd(text);
        }}
        keyboardType={'number-pad'}
      />
    </View>
  </View>
  <View
    style={{
      width: '90%',
      backgroundColor: 'white',
      marginBottom: 10,
    }}
  />
</View>
</Container>
</Heading>
<Footer>
<FooterTab>
<Button
  style={{backgroundColor: COLOR.DARKBLUE}}
  onPress={() => {
    _filterdata();
  }}>
  <Text style={{color: 'white', fontSize: 14, fontFamily: 'fs'}}>
    APPLY
  </Text>
</Button>
<Button
  style={{backgroundColor: COLOR.YELLOW}}
  onPress={() => {
    _clearall();
  }}>
  <Text style={{color: 'white', fontSize: 14, fontFamily: 'fs'}}>
    CLEAR ALL
  </Text>
</Button>
</FooterTab>
</Footer> */
}
