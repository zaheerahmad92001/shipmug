export const COLOR = {
  DARKBLUE: '#394C68',
  LIGHTBLUE: '#E4E8EB',
  GOLD: '#C8A169',
  YELLOW: '#fec169',

  //NEW
  TEXT: '#b0afb0',
  LABEL: '#3A4041',
  SUBLABEL: '#9CA8AA',
};
