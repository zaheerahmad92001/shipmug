import {COLOR} from './color';
import {LocalStorage} from './localstorage';
import getRandomColor from './genRandomcolor';
import {SERVER_URI, USER_PROFILE} from './server';
export {COLOR, LocalStorage, SERVER_URI, USER_PROFILE, getRandomColor};
