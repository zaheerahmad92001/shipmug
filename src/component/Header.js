import React from 'react';
import {
  Container,
  Header as Head,
  Title,
  Content,
  Button,
  Left,
  Right,
  Body,
  Icon,
} from 'native-base';
import Icons from 'react-native-vector-icons/FontAwesome';
import {StatusBar} from 'react-native';
import {COLOR} from '../common';
const Header = props => {
  return (
    <Container style={{backgroundColor: COLOR.LIGHTBLUE}}>
      <Head style={{backgroundColor: COLOR.DARKBLUE}}>
        <Left>
          <Button onPress={props.leftClick} transparent>
            <Icon onPress={props.leftClick} name={props.leftIcon} />
          </Button>
        </Left>
        <Body>
          <Title style={{ fontFamily: 'sf',}}>{props.title}</Title>
        </Body>
        <Right>
          {props.rightFirst && (
            <Button onPress={props.rightFirstclick} transparent>
              <Icons
                name={props.rightFirst}
                onPress={props.rightFirstclick}
                size={20}
                style={{color: 'white'}}
              />
            </Button>
          )}
          {props.rightSecond && (
            <Button onPress={props.rightSecondclick} transparent>
              <Icon onPress={props.rightSecondclick} name={props.rightSecond} />
            </Button>
          )}
        </Right>
      </Head>
      <Content refreshControl={props.refreshControl}>
        <StatusBar backgroundColor={COLOR.DARKBLUE} />
        {props.children}
      </Content>
    </Container>
  );
};
export default Header;
