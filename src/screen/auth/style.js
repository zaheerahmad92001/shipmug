import {StyleSheet, Dimensions} from 'react-native';
const window = Dimensions.get('window');

export const IMAGE_HEIGHT = window.width / 2;
export const IMAGE_HEIGHT_SMALL = window.width / 7;

export default StyleSheet.create({
  logo: {
    height: IMAGE_HEIGHT,
    resizeMode: 'contain',
    marginBottom: 20,
    padding: 10,
    marginTop: 20,
  },
});

// <View
// style={{
//   height: responsiveHeight(100),
//   width: '100%',
//   backgroundColor: 'white',
// }}>
// <Grid>
//   <Row
//     size={50}
//     style={{justifyContent: 'center', alignItems: 'center'}}>
//     <Image
//       source={require('../../../assets/login.png')}
//       style={{height: 189, width: 273}}
//     />
//   </Row>
//   <Row
//     size={50}
//     style={{
//       width: responsiveWidth(100),
//       justifyContent: 'center',
//     }}>
//     <View style={{width: responsiveWidth(90), alignItems: 'center'}}>
//       <Item
//         regular
//         style={{
//           marginBottom: 6,
//           padding: 5,
//           borderRadius: 5,
//           borderWidth: 1,
//         }}>
//         <AntIcon name="user" size={25} color="#b0afb0" />
//         <Input
//           style={{fontFamily: 'product', color: '#b0afb0'}}
//           placeholder="Email Address"
//           placeholderTextColor={'#b0afb0'}
//         />
//       </Item>
//       <Item
//         regular
//         style={{
//           marginBottom: 6,
//           padding: 5,
//           borderRadius: 5,
//           borderWidth: 1,
//         }}>
//         <AntIcon active name="lock" size={25} color="#b0afb0" />
//         <Input
//           style={{fontFamily: 'product', color: '#b0afb0'}}
//           placeholder="Password"
//           placeholderTextColor={'#b0afb0'}
//         />
//         <FonAwsmIcon
//           active
//           name="eye-slash"
//           size={20}
//           color="#b0afb0"
//         />
//       </Item>
//       <Button
//         style={{
//           width: 136,
//           marginTop: responsiveHeight(5),
//           borderRadius: 8,
//           height: 50,
//           backgroundColor: '#0178f2',
//           justifyContent: 'center',
//         }}
//         onPress={() => {}}>
//         <Text
//           uppercase={false}
//           style={{
//             fontFamily: 'product',
//             color: 'white',
//             alignSelf: 'center',
//             fontSize: responsiveFontSize(1.5),
//           }}>
//           Login
//         </Text>
//       </Button>
//       <View
//         style={{
//           flexDirection: 'row',
//           marginTop: responsiveHeight(10),
//         }}>
//         <Text
//           style={{
//             fontFamily: 'product',
//             color: 'black',
//             alignSelf: 'center',
//             fontSize: responsiveFontSize(1.5),
//           }}>
//           By Logging in , You hereby agree to
//         </Text>
//         <Text
//           style={{
//             fontFamily: 'product',
//             color: '#00479D',
//             alignSelf: 'center',
//             fontSize: responsiveFontSize(1.5),
//             marginLeft: 10,
//           }}>
//           Our Terms & Conditions
//         </Text>
//       </View>
//     </View>
//   </Row>
// </Grid>
// </View>
