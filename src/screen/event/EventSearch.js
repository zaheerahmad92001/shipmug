import React, {useState} from 'react';
import {FlatList, View, ActivityIndicator, StatusBar} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Content, Text, Item, Header as Head, Input, Icon} from 'native-base';
import moment from 'moment';
import Axios from 'axios';

import {COLOR, SERVER_URI, LocalStorage} from '../../common';
import {ROUTES} from '../../navigation/routes.constant';
import {Capitalize} from '../../common/capatalize';
import Heading from '../../component/Heading';
import TextAvatarComp from '../../component/textAvatarComponent';

const EventSearch = () => {
  const [search, setSearch] = useState('');
  const navigation = useNavigation();
  const [member, setMember] = useState();
  const [loading, setLoading] = useState(false);

  const _memberFetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/eventlist`,
      {
        user_id: await LocalStorage.getUserID(),
        key_word: search,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async (res) => {
        setLoading(false);
        setMember(res.data.data);
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  return (
    <>
      <Heading
        text={'Event Search'}
        lefticon={'ios-arrow-round-back'}
        Iconmap={[
          {
            name: 'bell',
            onPress: () => {
              alert('notification screen');
            },
          },
        ]}
        onMenuPress={() => {
          navigation.goBack();
        }}>
        <Content
          contentContainerStyle={{
            width: '100%',
            backgroundColor: 'white',
          }}>
          <Head searchBar rounded style={{backgroundColor: 'white'}}>
            <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
            <Item
              regular
              style={{
                marginBottom: 6,
                padding: 5,
                borderRadius: 5,
                borderWidth: 1,
              }}>
              <Icon name="ios-people" />
              <Input
                placeholder="Search"
                onChangeText={(e) => {
                  setSearch(e);
                  _memberFetch();
                }}
              />
              <Icon
                name="ios-search"
                onPress={() => {
                  _memberFetch();
                }}
              />
            </Item>
          </Head>
          <FlatList
            data={member}
            ListFooterComponent={
              loading ? (
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
                </View>
              ) : null
            }
            ListEmptyComponent={
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{fontFamily: 'product'}}>Search Result empty</Text>
              </View>
            }
            renderItem={({item}) => (
              <TextAvatarComp
                onPress={() => {
                  navigation.navigate(ROUTES.EVENTPROFILE, {id: item.id});
                }}
                name={Capitalize(item.name)}
                textavarname={item.name
                  .split(' ')
                  .slice(0, 2)
                  .join('+')
                  .toUpperCase()}
                address={moment(item.start_date).format('dddd, Do MMMM, YY')}
              />
            )}
            keyExtractor={(item) => item.id}
          />
        </Content>
      </Heading>
    </>
  );
};
export default EventSearch;
