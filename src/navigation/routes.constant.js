export const ROUTES = {
  LOGIN: 'LOGIN',
  SPLASH: 'SPLASH',

  AUTHSTACK: 'AUTHSTACK',
  APPSTACK: 'APPSTACK',
  DRAWERSTACK: 'DRAWERSTACK',
  MEMBERSTACK: 'MEMBERSTACK',
  GROUPSTACK: 'GROUPSTACK',
  EVENTSTACK: 'EVENTSTACK',
  FILESTACK: 'FILESTACK',
  NOTIFICATIONSTACK: 'NOTIFICATIONSTACK',
  SETTINGSTACK: 'SETTINGSTACK',
  PROFILESTACK: 'PROFILESTACK',
  BLOGSTACK: 'BLOGSTACK',
  BOOKMARKSTACK: 'BOOKMARKSTACK',

  EVENTEATTENDENCE: 'EVENTEATTENDENCE',
  EVENTLIST: 'EVENTLIST',
  EVENTPROFILE: 'EVENTPROFILE',
  EVENTSEARCH: 'EVENTSEARCH',

  MEMBERLIST: 'MEMBERLIST',
  MEMBERPROFILE: 'MEMBERPROFILE',
  MEMBERFILTER: 'MEMBERFILTER',
  MEMBERSEARCH: 'MEMBERSEARCH',
  MEMBERFILTERRESULT: 'MEMBERFILTERRESULT',

  GROUPLIST: 'GROUPLIST',
  SUBGROUPLIST: 'SUBGROUPLIST',
  GROUPMEMBER: 'GROUPMEMBER',
  GROUPSEARCH: 'GROUPSEARCH',
  GROUPPROFILE: 'GROUPPROFILE',

  BLOGLIST: 'BLOGLIST',
  NOTIFICATION: 'NOTIFICATION',
  PROFILE: 'PROFILE',
  SETTING: 'SETTING',

  FOLDERLIST: 'FOLDERLIST',
  FILELIST: 'FILELIST',
  FOLDERSEARCH: 'FOLDERSEARCH',
  FILESEARCH: 'FILESEARCH',

  INTRO: 'INTRO',

  DASHBOARD: 'DASHBOARD',

  BOOKMARK: 'BOOKMARK',

  DASHEVENT: 'DASHEVENT',
  DASHMEMBER: 'DASHMEMBER',
  DASHMEMBERPROFILE: 'DASHMEMBERPROFILE',
  DASHEVENTPROFILE: 'DASHEVENTPROFILE',
  CREATEEVENT:'CREATEEVENT',
  EDITEVENT:'EDITEVENT'
};
