import React, {useState, useEffect} from 'react';
import {
  useNavigation,
  StackActions,
  useFocusEffect,
} from '@react-navigation/native';
import {
  Content,
  Right,
  ListItem,
  Body,
  Card,
  CardItem,
  Left,
  Button,
} from 'native-base';
import Heading from '../../component/Heading';
import {COLOR, SERVER_URI, LocalStorage} from '../../common';
import {ROUTES} from '../../navigation/routes.constant';
import {Capitalize} from '../../common/capatalize';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/EvilIcons';
import {
  Image,
  View,
  Dimensions,
  RefreshControl,
  Text,
  Linking,
  Share,
  TouchableOpacity,
} from 'react-native';
import {
  responsiveFontSize,
  responsiveScreenHeight,
} from 'react-native-responsive-dimensions';
import Person from '../../icon/person';
import Church from '../../icon/Church';
import Logout from '../../icon/logout';
import Mail from '../../icon/mail';
import Lock from '../../icon/lock';
const Settings = () => {
  const navigation = useNavigation();
  const [opens, setOpen] = useState(false);
  const [member, setMember] = useState();
  const [loading, setLoading] = useState(true);

  const [imgload, setImgload] = useState(true);
  const _memberFetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/getusernamedetail`,
      {
        user_id: await LocalStorage.getUserID(),
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);
        setMember(res.data.data);
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    if (loading) {
      _memberFetch();
    }
  });
  const reset = route => {
    return navigation.dispatch(StackActions.pop(2));
  };

  const _logout = async navigation => {
    await LocalStorage.clearCache();
    await LocalStorage.clearToken();
    await LocalStorage.clearUserID();
    await LocalStorage.clearPermission();
    Axios.post(
      `${SERVER_URI}/logoutuser`,
      {
        user_id: await LocalStorage.getUserID(),
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });

    await reset(ROUTES.AUTHSTACK);

    // navigation.dispatch(StackActions.push(ROUTES.AUTHSTACK));
    // await navigation.navigate(ROUTES.AUTHSTACK);
  };
  // useFocusEffect(
  //   React.useCallback(() => {
  //     _memberFetch();
  //   }),
  // );

  useFocusEffect(
    React.useCallback(() => {
      _memberFetch();
    }, []),
  );

  var str = member && member.profilepic;

  var res =
    member &&
    member.profilepic &&
    str.replace(
      'http://app.sheepmug.com/public/uploads/',
      'https://app.sheepmug.com/public/uploads/user/',
    );
  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `kindly download our app   ${'http://www.sheepmug.com/'}`,
        url: 'http://www.sheepmug.com/',
        title: 'Link',
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  return (
    <Heading
      text={'Settings'}
      lefticon={'ios-menu'}
      Iconmap={[]}
      onMenuPress={() => {
        navigation.openDrawer();
      }}>
      <Content
        refreshControl={
          <RefreshControl
            colors={['#9Bd35A', '#689F38']}
            refreshing={loading}
            onRefresh={() => setLoading(true)}
          />
        }
        contentContainerStyle={{
          justifyContent: 'center',
          marginTop: 10,
          alignItems: 'center',
          backgroundColor: 'white',
        }}>
        <View
          style={{
            backgroundColor: 'white',
            height: Dimensions.get('window').height - 150,
            width: '100%',
            alignItems: 'center',
          }}>
          
          <Text
            style={{
              color: '#5B6466',
              fontSize: responsiveFontSize(2.4),
              fontFamily: 'product',
            }}>
            {member && Capitalize(member.username)}
          </Text>

          <TouchableOpacity
            style={{
              height: responsiveScreenHeight(8),
              width: '100%',
              marginTop: 25,
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 30}}>
              <Person width={30} height={30} />
            </View>
            <View style={{marginLeft: 30}}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2.2),
                  color: '#5B6466',
                  fontWeight: 'normal',
                  fontFamily: 'product',
                }}>
                Assigned Members - {member && member.total_member}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              height: responsiveScreenHeight(8),
              width: '100%',
              marginTop: 5,
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 30}}>
              <Church width={30} height={30} />
            </View>
            <View style={{marginLeft: 30}}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2.2),
                  color: '#5B6466',
                  fontWeight: 'normal',
                  fontFamily: 'product',
                }}>
                Assigned Groups - {member && member.total_group}
              </Text>
            </View>
          </TouchableOpacity>

         
         

          <TouchableOpacity
            onPress={() => {
              Linking.openURL(
                'mailto:support@sheepmug.com?subject=Bug Reports Enquiries &body=',
              );
            }}
            style={{
              height: responsiveScreenHeight(8),
              width: '100%',
              marginTop: 5,
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 30}}>
              <Mail width={30} height={30} />
            </View>
            <View style={{marginLeft: 30}}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2.2),
                  color: '#5B6466',
                  fontWeight: 'normal',
                  fontFamily: 'product',
                }}>
                Contact Us - Bug Report, Enquiries
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              _logout(navigation);
            }}
            style={{
              height: responsiveScreenHeight(8),
              width: '100%',
              marginTop: 5,
              borderBottomColor: '#00000017',
              borderBottomWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View style={{marginLeft: 30}}>
              <Logout width={30} height={30} />
            </View>
            <View style={{marginLeft: 30}}>
              <Text
                style={{
                  fontSize: responsiveFontSize(2.2),
                  color: '#5B6466',
                  fontWeight: 'normal',
                  fontFamily: 'product',
                }}>
                Log-out
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        {/* <ListItem style={{height: 80}}>
          <Body>
            <Text>Notifications</Text>
          </Body>
          <Right>
            <Icons name={'ellipsis-v'} size={20} style={{color: 'black'}} />
          </Right>
        </ListItem> */}
        {/* <Card>
          <CardItem>
            <Left>
              <Text style={{color: '#b8b6bf'}}>Member Update</Text>
            </Left>

            <Right>
              <Switch
                trackColor={{false: '#767577', true: COLOR.DARKBLUE}}
                thumbColor={isEnabled ? COLOR.DARKBLUE : COLOR.DARKBLUE}
                ios_backgroundColor="#3e3e3e"
                onValueChange={toggleSwitch}
                value={isEnabled}
              />
            </Right>
          </CardItem>
          <CardItem>
            <Left>
              <Text style={{color: '#b8b6bf'}}>Event Update</Text>
            </Left>

            <Right>
              <Switch
                trackColor={{false: '#767577', true: COLOR.DARKBLUE}}
                thumbColor={isEnabled ? COLOR.DARKBLUE : COLOR.DARKBLUE}
                ios_backgroundColor="#3e3e3e"
                onValueChange={toggleSwitch}
                value={isEnabled}
              />
            </Right>
          </CardItem>
          <CardItem>
            <Left>
              <Text style={{color: '#b8b6bf'}}>Info Dates</Text>
            </Left>

            <Right>
              <Switch
                trackColor={{false: '#767577', true: COLOR.DARKBLUE}}
                thumbColor={isEnabled ? COLOR.DARKBLUE : COLOR.DARKBLUE}
                ios_backgroundColor="#3e3e3e"
                onValueChange={toggleSwitch}
                value={isEnabled}
              />
            </Right>
          </CardItem>
          <CardItem>
            <Left>
              <Text style={{color: '#b8b6bf'}}>Attendence Reminder</Text>
            </Left>

            <Right>
              <Switch
                trackColor={{false: '#767577', true: COLOR.DARKBLUE}}
                thumbColor={isEnabled ? COLOR.DARKBLUE : COLOR.DARKBLUE}
                ios_backgroundColor="#3e3e3e"
                onValueChange={toggleSwitch}
                value={isEnabled}
              />
            </Right>
          </CardItem>
          <CardItem>
            <Left>
              <Text style={{color: '#b8b6bf'}}>Blog Notifications</Text>
            </Left>

            <Right>
              <Switch
                trackColor={{false: '#767577', true: COLOR.DARKBLUE}}
                thumbColor={isEnabled ? COLOR.DARKBLUE : COLOR.DARKBLUE}
                ios_backgroundColor="#3e3e3e"
                onValueChange={toggleSwitch}
                value={isEnabled}
              />
            </Right>
          </CardItem>
          Linking.openURL(`tel:${member[0].mobile_phone}`);
        </Card> */}
        {/* <ListItem style={{height: 80}}>
          <Body>
            <Text>Synchronization</Text>
          </Body>
          <Right>
            <Switch
              trackColor={{false: '#767577', true: COLOR.DARKBLUE}}
              thumbColor={isEnabled ? COLOR.DARKBLUE : COLOR.DARKBLUE}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </Right>
        </ListItem> */}
        {/* <ListItem style={{height: 80}}>
          <Body>
            <Text>Profile Infortmation</Text>
          </Body>
        </ListItem> */}
        {/* <ListItem
          onPress={() => {
            setOpen(!opens);
          }}
          style={{height: 80}}>
          <Body>
            <Text style={{fontFamily: 'sf'}}>About App & Contact Us</Text>
          </Body>
        </ListItem>
        {opens && (
          <ListItem style={{height: 80}}>
            <Body>
              <Text
                style={{fontFamily: 'sf'}}
                onPress={() => {
                  Linking.openURL(`http://sheepmug.com/`);
                }}>
                About App
              </Text>
            </Body>
            <Body>
              <Text
                style={{fontFamily: 'sf'}}
                onPress={() => {
                  Linking.openURL(`tel:+919726491167`);
                }}>
                Contact Us
              </Text>
            </Body>
          </ListItem>
        )}
        <ListItem
          onPress={() => {
            onShare();
          }}
          style={{height: 80}}>
          <Body>
            <Text
              style={{fontFamily: 'sf'}}
              onPress={() => {
                onShare();
              }}>
              Share App
            </Text>
          </Body>
        </ListItem> */}
      </Content>
    </Heading>
  );
};

export default Settings;
