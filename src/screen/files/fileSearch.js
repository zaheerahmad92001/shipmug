// import React, {useState} from 'react';
// import {useNavigation, useRoute} from '@react-navigation/native';
// import {
//   FlatList,
//   View,
//   StatusBar,
//   TouchableOpacity,
//   Linking,
// } from 'react-native';

// import {Content, Text, Item, Header as Head, Input, Icon} from 'native-base';

// import {Capitalize} from '../../common/capatalize';

// import Heading from '../../component/Heading';
// import Doc from '../../icon/Doc';
// import Excel from '../../icon/Excel';
// import Jpg from '../../icon/Jpg';
// import MP from '../../icon/Mp';
// import Pdf from '../../icon/pdf';
// import PowerPoint from '../../icon/powerpoint';
// import {
//   responsiveFontSize,
//   responsiveScreenHeight,
// } from 'react-native-responsive-dimensions';
// const FileSearch = () => {
//   const route = useRoute();
//   const {fileList} = route.params;
//   const getfileicon = (type) => {
//     let Component = Doc;
//     if (type == 'pdf') {
//       Component = Pdf;
//     } else if (type == 'docx' || type == 'doc') {
//       Component = Doc;
//     } else if (type == 'jpg' || type == 'png' || type == 'jpeg') {
//       Component = Jpg;
//     } else if (
//       type == 'xls' ||
//       type == 'xlsb' ||
//       type == 'xlsm' ||
//       type == 'xlsx'
//     ) {
//       Component = Excel;
//     } else if (
//       type == 'mp3' ||
//       type == 'Mp3' ||
//       type == 'ogg' ||
//       type == 'mp4'
//     ) {
//       Component = MP;
//     } else if (type == 'pptx' || type == 'pptm' || type == 'ppt') {
//       Component = PowerPoint;
//     }

//     return <Component height={40} width={40} />;
//   };
//   const [search, setSearch] = useState('!');
//   const [searchData, setSearchData] = useState([]);
//   const navigation = useNavigation();
//   var filters = [{name: search}];

//   const searchss = () =>
//     fileList &&
//     fileList.filter((a) =>
//       filters.every(function (filter) {
//         return Object.keys(filter).every(function (key) {
//           return a[key].includes([filter[key]]);
//         });
//       }),
//     );

//   return (
//     <Heading
//       text={'File Search'}
//       lefticon={'ios-arrow-round-back'}
//       Iconmap={[
//         {
//           name: 'bell',
//           onPress: () => {
//             alert('notification screen');
//           },
//         },
//       ]}
//       onMenuPress={() => {
//         navigation.goBack();
//       }}>
//       <Content
//         contentContainerStyle={{
//           width: '100%',
//           backgroundColor: 'white',
//         }}>
//         <Head searchBar rounded style={{backgroundColor: 'white'}}>
//           <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
//           <Item
//             regular
//             style={{
//               marginBottom: 6,
//               padding: 5,
//               borderRadius: 5,
//               borderWidth: 1,
//             }}>
//             <Icon name="ios-people" />
//             <Input
//               placeholder="Search"
//               style={{fontFamily: 'product', color: '#b0afb0'}}
//               placeholderTextColor={'#b0afb0'}
//               onChangeText={(e) => {
//                 setSearch(e);
//               }}
//             />

//             <Icon
//               name="ios-search"
//               onPress={() => {
//                 setSearchData(searchss());
//               }}
//             />
//           </Item>
//         </Head>
//         <FlatList
//           data={searchData}
//           ListEmptyComponent={
//             <View style={{justifyContent: 'center', alignItems: 'center'}}>
//               <Text style={{fontFamily: 'product'}}>Search Result empty</Text>
//             </View>
//           }
//           renderItem={({item}) => (
//             <TouchableOpacity
//               onPress={() => {
//                 Linking.openURL(
//                   `http://www.sheepmug.com/superadmin/public/uploads/folderFiles/${item.name}`,
//                 );
//               }}
//               style={{
//                 height: responsiveScreenHeight(10),
//                 width: '100%',
//                 marginTop: 5,
//                 backgroundColor: 'white', //'#1E7A9D',
//                 borderBottomColor: '#00000017',
//                 borderBottomWidth: 1,
//                 alignItems: 'center',
//                 flexDirection: 'row',
//               }}>
//               <View style={{marginLeft: 15}}>
//                 {getfileicon(item.name.slice(item.name.lastIndexOf('.') + 1))}
//               </View>
//               <View style={{marginLeft: 20, width: '90%'}}>
//                 <Text
//                   numberOfLines={1}
//                   style={{
//                     fontSize: responsiveFontSize(2),
//                     color: '#3A4041',
//                     fontFamily: 'product',
//                     width: '90%',
//                   }}>
//                   {Capitalize(item.name)}
//                 </Text>
//               </View>
//             </TouchableOpacity>
//           )}
//           keyExtractor={(item) => item}
//         />
//       </Content>
//     </Heading>
//   );
// };
// export default FileSearch;
import React, {useState} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import {
  FlatList,
  View,
  StatusBar,
  TouchableOpacity,
  Linking,
  ActivityIndicator,
} from 'react-native';
import {COLOR, SERVER_URI, LocalStorage} from '../../common';
import {Content, Text, Item, Header as Head, Input, Icon} from 'native-base';
import Axios from 'axios';
import {Capitalize} from '../../common/capatalize';

import Heading from '../../component/Heading';
import Doc from '../../icon/Doc';
import Excel from '../../icon/Excel';
import Jpg from '../../icon/Jpg';
import MP from '../../icon/Mp';
import Pdf from '../../icon/pdf';
import PowerPoint from '../../icon/powerpoint';
import {
  responsiveFontSize,
  responsiveScreenHeight,
} from 'react-native-responsive-dimensions';
const FileSearch = () => {
  const [search, setSearch] = useState('');
  const route = useRoute();
  const {folderid} = route.params;
  const navigation = useNavigation();
  const [folderList, setfolderList] = useState([]);
  const [loading, setLoading] = useState(false);
  const getfileicon = type => {
    let Component = Doc;
    if (type == 'pdf') {
      Component = Pdf;
    } else if (type == 'docx' || type == 'doc') {
      Component = Doc;
    } else if (type == 'jpg' || type == 'png' || type == 'jpeg') {
      Component = Jpg;
    } else if (
      type == 'xls' ||
      type == 'xlsb' ||
      type == 'xlsm' ||
      type == 'xlsx'
    ) {
      Component = Excel;
    } else if (
      type == 'mp3' ||
      type == 'Mp3' ||
      type == 'ogg' ||
      type == 'mp4'
    ) {
      Component = MP;
    } else if (type == 'pptx' || type == 'pptm' || type == 'ppt') {
      Component = PowerPoint;
    }

    return <Component height={40} width={40} />;
  };
  const _memberFetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/files`,
      {
        user_id: await LocalStorage.getUserID(),

        folder_id: folderid,
        key_word: search,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);
        setfolderList(res.data.data);
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  return (
    <Heading
      text={'File Search'}
      lefticon={'ios-arrow-round-back'}
      Iconmap={
        [
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('notification screen');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        navigation.goBack();
      }}>
      <Content
        contentContainerStyle={{
          width: '100%',
          backgroundColor: 'white',
        }}>
        <Head searchBar rounded style={{backgroundColor: 'white'}}>
          <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
          <Item
            regular
            style={{
              marginBottom: 6,
              padding: 5,
              borderRadius: 5,
              borderWidth: 1,
            }}>
            <Icon name="ios-people" />
            <Input
              placeholder="Search"
              style={{fontFamily: 'product', color: '#b0afb0'}}
              placeholderTextColor={'#b0afb0'}
              onChangeText={e => {
                setSearch(e);
                _memberFetch();
              }}
            />

            <Icon
              name="ios-search"
              onPress={() => {
                _memberFetch();
              }}
            />
          </Item>
        </Head>
        <FlatList
          data={folderList}
          ListFooterComponent={
            loading ? (
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
              </View>
            ) : null
          }
          ListEmptyComponent={
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontFamily: 'product'}}>Search Result empty</Text>
            </View>
          }
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => {
                Linking.openURL(
                  `http://www.app.sheepmug.com/public/uploads/folderFiles/${
                    item.name
                  }`,
                );
              }}
              style={{
                height: responsiveScreenHeight(10),
                width: '100%',
                marginTop: 5,
                backgroundColor: 'white', //'#1E7A9D',
                borderBottomColor: '#00000017',
                borderBottomWidth: 1,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <View style={{marginLeft: 15}}>
                {getfileicon(item.name.slice(item.name.lastIndexOf('.') + 1))}
              </View>
              <View style={{marginLeft: 20, width: '90%'}}>
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: responsiveFontSize(2),
                    color: '#3A4041',
                    fontFamily: 'product',
                    width: '90%',
                  }}>
                  {Capitalize(item.name)}
                </Text>
              </View>
            </TouchableOpacity>
          )}
          keyExtractor={item => item.key}
        />
      </Content>
    </Heading>
  );
};
export default FileSearch;
