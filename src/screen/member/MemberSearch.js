import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  FlatList,
  ActivityIndicator,
  View,
  TouchableOpacity,
  StatusBar,
} from 'react-native';

import {
  Content,
  Text,
  Item,
  Header as Head,
  Input,
  Icon,
  Thumbnail,
} from 'native-base';
import {COLOR, SERVER_URI, LocalStorage} from '../../common';
import Axios from 'axios';
import {ROUTES} from '../../navigation/routes.constant';
import {Capitalize} from '../../common/capatalize';
import Heading from '../../component/Heading';
import {
  responsiveScreenHeight,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import MemberCompo from '../../component/memberComponent';
const MemberSearch = () => {
  const [search, setSearch] = useState('');
  const navigation = useNavigation();
  const [member, setMember] = useState();
  const [loading, setLoading] = useState(false);

  const _memberFetch = async () => {
    setLoading(true);
    Axios.post(
      `${SERVER_URI}/memberlist`,
      {
        user_id: await LocalStorage.getUserID(),
        key_word: search,
      },
      {
        headers: {
          'X-API-TOKEN': await LocalStorage.getToken(),
        },
      },
    )
      .then(async res => {
        setLoading(false);
        setMember(res.data.data);
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  return (
    <Heading
      text={'Member Search'}
      lefticon={'ios-arrow-round-back'}
      Iconmap={
        [
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('notification screen');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        navigation.goBack();
      }}>
      <Content
        contentContainerStyle={{
          width: '100%',
          backgroundColor: 'white',
        }}>
        <Head searchBar rounded style={{backgroundColor: 'white'}}>
          <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
          <Item
            regular
            style={{
              marginBottom: 6,
              padding: 5,
              borderRadius: 5,
              borderWidth: 1,
            }}>
            <Icon name="ios-people" />
            <Input
              placeholder="Search"
              style={{fontFamily: 'product', color: '#b0afb0'}}
              placeholderTextColor={'#b0afb0'}
              onChangeText={e => {
                setSearch(e);
                _memberFetch();
              }}
            />

            <Icon
              name="ios-search"
              onPress={() => {
                _memberFetch();
              }}
            />
          </Item>
        </Head>
        <FlatList
          data={member}
          ListFooterComponent={
            loading ? (
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <ActivityIndicator size={'large'} color={COLOR.YELLOW} />
              </View>
            ) : null
          }
          ListEmptyComponent={
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontFamily: 'product'}}>Search Result empty</Text>
            </View>
          }
          renderItem={({item}) => (
            <MemberCompo
              onPress={() => {
                navigation.navigate(ROUTES.MEMBERPROFILE, {
                  type: 'SEARCH_MEMBER',
                  id: item.id,
                  address: item.address,
                  fullname: `${Capitalize(item.first_name)} ${Capitalize(
                    item.last_name,
                  )}`,
                  photo:
                    item.photo == 'http://app.sheepmug.com/public/uploads'
                      ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                      : item.photo,
                });
              }}
              name={`${Capitalize(item.first_name)} ${Capitalize(
                item.last_name,
              )}`}
              photo={
                item.photo == 'http://app.sheepmug.com/public/uploads'
                  ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                  : item.photo
              }
              address={item.address}
            />
            // <TouchableOpacity
            //   onPress={() => {
            //     navigation.navigate(ROUTES.MEMBERPROFILE, {
            //       type: 'SEARCH_MEMBER',
            //       id: item.id,
            //       address: item.address,
            //       fullname: `${Capitalize(item.first_name)} ${Capitalize(
            //         item.last_name,
            //       )}`,
            //       photo:
            //         item.photo ==
            //         'http://app.sheepmug.com/public/uploads'
            //           ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
            //           : item.photo,
            //     });
            //   }}
            //   style={{
            //     height: responsiveScreenHeight(8),
            //     width: '100%',
            //     borderTopColor: '#00000017',
            //     borderTopWidth: 2,
            //     borderBottomColor: '#00000017',
            //     borderBottomWidth: 2,
            //     alignItems: 'center',
            //     flexDirection: 'row',
            //   }}>
            //   <View style={{marginLeft: 15}}>
            //     <Thumbnail
            //       style={{height: 40, width: 40}}
            //       source={{
            //         uri:
            //           item.photo ==
            //           'http://app.sheepmug.com/public/uploads'
            //             ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
            //             : item.photo,
            //       }}
            //     />
            //   </View>
            //   <View style={{marginLeft: 16}}>
            //     <Text
            //       style={{
            //         fontSize: responsiveFontSize(1.5),
            //         color: '#3A4041',
            //         fontFamily: 'product',
            //       }}>
            //       {Capitalize(item.first_name)} {Capitalize(item.last_name)}
            //     </Text>
            //     <Text
            //       style={{color: '#9CA8AA', fontSize: responsiveFontSize(1)}}>
            //       {item.address}
            //     </Text>
            //   </View>
            // </TouchableOpacity>
          )}
          keyExtractor={item => item.key}
        />
      </Content>
    </Heading>
  );
};
export default MemberSearch;
// <TouchableOpacity
//   onPress={() => {
//     navigation.navigate(ROUTES.MEMBERPROFILE, {
//       type: 'SEARCH_MEMBER',
//       id: item.id,
//       address: item.address,
//       fullname: `${Capitalize(item.first_name)} ${Capitalize(
//         item.last_name,
//       )}`,
//       photo:
//         item.photo ==
//         'http://app.sheepmug.com/public/uploads'
//           ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
//           : item.photo,
//     });
//   }}
//   style={{
//     height: 82,
//     width: '100%',
//     alignItems: 'center',
//     flexDirection: 'row',
//     backgroundColor: 'white',
//     marginTop: 3,
//   }}>
//   <View style={{marginLeft: 32}}>
//     <Thumbnail
//       source={{
//         uri:
//           item.photo ==
//           'http://app.sheepmug.com/public/uploads'
//             ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
//             : item.photo,
//       }}
//     />
//   </View>
//   <View style={{marginLeft: 25}}>
//     <Text
//       style={{fontSize: 18, color: '#000000', fontFamily: 'sf'}}>
//       {Capitalize(item.first_name)} {Capitalize(item.last_name)}
//     </Text>
//   </View>
// </TouchableOpacity>
