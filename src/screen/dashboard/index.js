import React, {useEffect, useState} from 'react';
import {Thumbnail} from 'native-base';
import {
  ImageBackground,
  Text,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import * as data from '../../common/data';
import {
  responsiveFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from 'react-native-responsive-dimensions';
import {COLOR} from '../../common/color';
import Heading from '../../component/Heading';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import MemberCompo from '../../component/memberComponent';
import TextAvatarComp from '../../component/textAvatarComponent';
import {LocalStorage, SERVER_URI} from '../../common';
import Axios from 'axios';
import {_logout} from '../../component/logout';
import {useSafeArea} from 'react-native-safe-area-context';
import {Capitalize} from '../../common/capatalize';
import {ROUTES} from '../../navigation/routes.constant';
import moment from 'moment';

const DATA = [
  {
    key: 0,
  },
  {
    key: 1,
  },
];

function shuffle(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
const Dashboard = () => {
  const quotes = shuffle(data.data);
  const navigation = useNavigation();
  const [loading, setLoading] = useState(true);
  const [member, setMember] = useState([]);
  const [eventList, setEventList] = useState();

  const _memberFetch = async () => {
    setLoading(true);

    Promise.all([
      Axios.post(
        `${SERVER_URI}/memberlist`,
        {
          user_id: await LocalStorage.getUserID(),
        },
        {
          headers: {
            'X-API-TOKEN': await LocalStorage.getToken(),
          },
        },
      ),
      Axios.post(
        `${SERVER_URI}/eventlist`,
        {
          user_id: await LocalStorage.getUserID(),
        },
        {
          headers: {
            'X-API-TOKEN': await LocalStorage.getToken(),
          },
        },
      ),
    ])
      .then(([member, event]) => {
        setLoading(false);
        setMember(member.data.data.slice(0, 2));
        setEventList(event.data.data.slice(0, 2));
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    if (loading) {
      _memberFetch();
    }
  });

  return (
    <Heading
      text={'SheepMug'}
      lefticon={'ios-menu'}
      Iconmap={
        [
          // {
          //   name: 'bell',
          //   onPress: () => {
          //     alert('bellpress');
          //   },
          // },
        ]
      }
      onMenuPress={() => {
        navigation.openDrawer();
      }}>
      <ImageBackground
        source={require('../../../assets/biblebg.png')}
        style={{height: responsiveScreenHeight(25), width: '100%'}}>
        <Text
          style={{
            color: '#FFFFFF',
            fontSize: responsiveFontSize(1.3),
            marginLeft: 15,
            marginTop: 10,
          }}>
          Bible Verse for Today
        </Text>
        <View
          style={{
            width: responsiveScreenWidth(12),
            backgroundColor: 'white',
            height: responsiveScreenHeight(0.2),
            marginLeft: 15,
            marginTop: 10,
          }}
        />
        <Text
          style={{
            color: 'white',
            fontSize: responsiveFontSize(1.8),
            width: responsiveScreenWidth(98),
            fontFamily: 'product',
            marginLeft: 15,
            marginTop: 10,
          }}>
          {quotes[0].key}
        </Text>
      </ImageBackground>
      <View
        style={{
          height: responsiveScreenHeight(60),
          width: '100%',
        }}>
        <View style={{height: responsiveScreenHeight(30), width: '100%'}}>
          <View
            style={{
              height: responsiveScreenHeight(5),
              width: '100%',
              justifyContent: 'space-between',
              alignItems: 'center',
              flexDirection: 'row',
              display: 'flex',
              paddingHorizontal: 25,
            }}>
            <Text
              style={{
                color: COLOR.SUBLABEL,
                fontSize: responsiveFontSize(1.5),
              }}>
              Recent Members
            </Text>

            <TouchableOpacity
              onPress={() => {
                navigation.navigate(ROUTES.DASHMEMBER);
              }}
              hitSlop={{top: 8, right: 8, left: 8, bottom: 8}}>
              <Text
                style={{
                  color: COLOR.SUBLABEL,
                  fontSize: responsiveFontSize(1.5),
                }}>
                All Members
              </Text>
            </TouchableOpacity>
          </View>

          <FlatList
            data={member}
            ListEmptyComponent={
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                  width: '100%',
                }}>
                <Text style={{fontFamily: 'product'}}>No member Found</Text>
              </View>
            }
            renderItem={({item}) => (
              <View>
                <MemberCompo
                  key={item.id}
                  onPress={() => {
                    navigation.navigate(ROUTES.DASHMEMBERPROFILE, {
                      type: 'MEMBER',
                      id: item.id,
                      address: item.address,
                      fullname: `${Capitalize(item.first_name)} ${Capitalize(
                        item.last_name,
                      )}`,
                      first_name: item.first_name,
                      last_name: item.last_name,
                      photo:
                        item.photo == 'http://app.sheepmug.com/public/uploads'
                          ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                          : `${item.photo}`,
                      phone: item.mobile_phone,
                      email: item.email,
                    });
                  }}
                  photo={
                    item.photo == 'http://app.sheepmug.com/public/uploads'
                      ? 'https://i.postimg.cc/4xRdwW6w/dummy.png'
                      : `${item.photo}`
                  }
                  name={
                    Capitalize(item.first_name) +
                    ' ' +
                    Capitalize(item.last_name)
                  }
                  address={item.address}
                />
              </View>
            )}
            keyExtractor={item => item.id}
          />
        </View>
        <View style={{height: responsiveScreenHeight(30), width: '100%'}}>
          <View
            style={{
              height: responsiveScreenHeight(5),
              width: '100%',
              justifyContent: 'space-between',
              alignItems: 'center',
              flexDirection: 'row',
              display: 'flex',
              paddingHorizontal: 25,
            }}>
            <Text
              style={{
                color: COLOR.SUBLABEL,
                fontSize: responsiveFontSize(1.5),
              }}>
              Upcoming Events
            </Text>

            <TouchableOpacity
              onPress={() => {
                navigation.navigate(ROUTES.DASHEVENT);
              }}
              hitSlop={{top: 8, right: 8, left: 8, bottom: 8}}>
              <Text
                style={{
                  color: COLOR.SUBLABEL,
                  fontSize: responsiveFontSize(1.5),
                }}>
                All Events
              </Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={eventList}
            ListEmptyComponent={
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                  width: '100%',
                }}>
                <Text style={{fontFamily: 'product'}}>No Event Found</Text>
              </View>
            }
            renderItem={({item}) => (
              <TextAvatarComp
                key={item.id}
                onPress={() => {
                  navigation.navigate(ROUTES.DASHEVENTPROFILE, {
                    id: item.id,
                    name: item.name,
                  });
                }}
                name={Capitalize(item.name)}
                textavarname={item.name
                  .split(' ')
                  .slice(0, 2)
                  .join('+')
                  .toUpperCase()}
                address={moment(item.start_date).format('dddd, Do MMMM, YY')}
              />
            )}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    </Heading>
  );
};
export default Dashboard;
